<?php
return [
    'site_name' => 'Evidence for Learning',
    'currency_default'=>'USD',
    'languages'=>[
            'en_uk'=>'English (UK)',
            'en_us'=>'English (Us)',
    ],
    'language_default'=>'en_uk',
    'contentTypes'=>[
            'page'=>'Page',
            'email'=>'Email',
            'block'=>'Block',
    ],
    'site_admin_url' => 'http://ec2-34-240-185-191.eu-west-1.compute.amazonaws.com/admin/home',
    'order_prefix'=>"efl",
    'password_pattern' => '(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
    'keywords' => 'Evidence for Learning',
    'meta_description'=>'Evidence for Learning',
    'date_format'=>'d/m/Y',
    'site_admin_url' => 'http://ec2-34-240-185-191.eu-west-1.compute.amazonaws.com/admin/home/loginas',
];
?>

<!DOCTYPE html>
<html>
    <head>

        <!-- Mirrored from themesdesign.in/appzia/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 08:59:51 GMT -->
    <head>
        <meta charset="utf-8" />
        <title><?php echo Config('params.site_name'); ?> | @yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link rel="shortcut icon" href="{{ asset('/front/assets/images/favicon.ico') }}">
        <link href="{{ asset('/front/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/front/assets/css/icons.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front/assets/css/stylized.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front/assets/css/style.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front/assets/css/style-extra.css')}}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="accountbg"></div>
        <div class="wrapper-page">
            @yield('content')
        </div>
        <script src="{{ asset('/front/assets/js/jquery.min.js') }}"></script> 
        <script src="{{ asset('/front/assets/js/bootstrap.min.js') }}"></script> 
        <script src="{{ asset('/front/assets/js/modernizr.min.js') }}"></script> 
        <script src="{{ asset('/front/assets/js/detect.js') }}"></script> 
        <script src="{{ asset('/front/assets/js/fastclick.js') }}"></script> 
        <script src="{{ asset('/front/assets/js/jquery.slimscroll.js') }}"></script> 
        <script src="{{ asset('/front/assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('/front/assets/js/waves.js') }}"></script> 
        <script src="{{ asset('/front/assets/js/wow.min.js') }}"></script> 
        <script src="{{ asset('/front/assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('/front/assets/js/jquery.scrollTo.min.js') }}"></script> 
        <script src="{{ asset('/front/assets/plugins/morris/morris.min.js') }}"></script>
        <script src="{{ asset('/front/assets/plugins/raphael/raphael-min.js') }}"></script> 
        <script src="{{ asset('/front/assets/pages/dashborad.js') }}"></script> 
        <script src="{{ asset('/front/assets/js/app.js') }}"></script>
    </body>
</html>
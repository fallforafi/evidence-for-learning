<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body class="email-template">
        <div style="padding:8px; box-sizing:border-box;font-family:sans-serif;">
            <h1 style="display:inline; color:#333; margin-top:40px;margin-bottom:40px; padding:3px 0px; font-weight:300;">New Password!</h1>
            <p>You new password is "{{ $password }}".</p>
        </div>
    </body>
</html>

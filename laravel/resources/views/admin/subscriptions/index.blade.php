@extends('admin/admin_template')
@section('content')

<!-- Main row -->
<div class="row">

    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Subscriptions( Total : {{ count($subscriptions) }} ) </h3>
            </div>
            <div class="box-body">
                <ul class="products-list product-list-in-box">

                    <?php $i = 1; ?>
                    @foreach ($subscriptions as $row)

                    <?php
                    $color = ($i % 2 == 0 ? 'success' : 'info');
                    ?>

                    <li class="item">

                        <div class="product-img">
                            <a class="btn btn-warning" href="subscriptions/edit/<?php echo $row->id ?>">Edit</a>
                        </div>

                        <div class="product-info">
                            <?php echo $row->title; ?>
                                <span class="label label-<?php echo $color; ?> pull-right"><?php echo $row->type; ?></span>                            <span class="product-description">
                                <?php echo $row->description; ?>
                            </span>

                        </div>
                    </li>
                    <!-- /.item -->
                    <?php $i++; ?>
                    @endforeach

                </ul>

            </div>
        </div>
    </div>
</div>
<!-- /.row -->	

@endsection

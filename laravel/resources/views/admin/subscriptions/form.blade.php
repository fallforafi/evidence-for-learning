<div class="form-group">
    {!! Form::label('Title') !!}
    {!! Form::text('title', null , array('class' => 'form-control','required') ) !!}
</div>
<div class="form-group">
    {!! Form::label('description') !!}
    {!! Form::textarea('description', null, ['size' => '105x3','class' => 'form-control required']) !!} 

</div>
<div class="form-group">
    {!! Form::label('Initial Price') !!}$
    {!! Form::text('initialPrice', null , array('class' => 'form-control','required') ) !!}
</div>
<div class="form-group">
    {!! Form::label('Price') !!}$
    {!! Form::text('price', null , array('class' => 'form-control','required') ) !!}
</div>
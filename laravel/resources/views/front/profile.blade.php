<?php
$session = Session::all();
?>
<!DOCTYPE html>
<html class="broken-image-checker">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>

        <title><?php echo Config('params.site_name'); ?> | @yield('title')</title>
        <link rel="shortcut icon" href="{{ asset('front/assets/images/favicon.ico')}}">

        <link href="{{ asset('front/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front/assets/css/icons.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front/assets/css/stylized.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front/assets/css/style.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front/assets/css/style-extra.css')}}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('adminlte/plugins/treegrid/treegrid.css')}}">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <link href="{{ asset('front/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css">
        <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

        <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.css')}}">

        <script src="{{ asset('front/assets/js/jquery.js') }}"></script> 
        <script src="{{ asset('front/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>

    </head>

    <body class="fixed-left widescreen transition   " data-theme="<?php echo $session['colour'] ?>">
        <div id="wrapper">
            @include('front.common.header')
            @include('front.common.left');
            <div class="content-page">

                @yield('content')
                <!-- /.login-box-body -->
                @include('front.common.footer')
            </div>
        </div>
        <script>
function includePlg() {
    $(".plg").prop("checked", true);
    $("#excludePlg").show();
    $("#includePlg").hide();

}

function excludePlg() {
    $(".plg").prop("checked", false);
    $("#includePlg").show();
    $("#excludePlg").hide();
}

$(function () {
    $('.treeviewized li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
    $('.treeviewized li.parent_li > span').on('click', function (e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        jQuery(this).parent("li").toggleClass("is-open");

        e.stopPropagation();
    });
});

        </script>    


        <script src="{{ asset('front/assets/js/bootstrap.min.js') }}"></script> 
        <script src="{{ asset('front/assets/js/kodeized.js') }}"></script>
        <script src="{{ asset('front/assets/js/viewportchecker.js') }}"></script>
        <script src="{{ asset('front/assets/js/app.js') }}"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <!-- Include Date Range Picker -->
        <script src="{{ asset('front/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <script src="{{ asset('front/assets/js/jquery.lazyload.js') }}"></script>
        <script src="{{ asset('front/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>

        <script>

$(document).ready(function () {
    $.ajax({
        url: "<?php echo url("getschoollogo"); ?>",
        type: 'get',
        dataType: 'json',
        cache: true,
        async: true,
        beforeSend: function () {
            //$('#evidences_loader').show();
        },
        complete: function () {
            // $('#evidences_loader').hide();
        },
        success: function (response) {
            $('.schoolLogo').attr('src', response.src);

        }
    });

});

        </script>
        <script>
            jQuery(function ($) {
                $('img.lazy').lazyload();
            });
        </script>
    </body>
</html>

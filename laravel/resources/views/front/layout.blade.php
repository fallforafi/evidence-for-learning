<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--iPhone from zooming form issue-->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
        <title><?php echo Config('params.site_name'); ?> | @yield('title')</title>
        <meta name="description" content="@yield('description')">
        <meta name="keywords" content="@yield('keywords')" />

        <title><?php echo Config('params.site_name'); ?> | @yield('title')</title>
        <link rel="shortcut icon" href="{{ asset('front/assets/images/favicon.ico')}}">
        <link rel="stylesheet" href="{{ asset('front/assets/plugins/morris/morris.css')}}">
        <link href="{{ asset('front/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front/assets/css/icons.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front/assets/css/stylized.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front/assets/css/style.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('front/assets/css/style-extra.css')}}" rel="stylesheet" type="text/css">
    </head>
    <body class="fixed-left widescreen login-page " 
	style="background-image:url({{ asset('front/assets/images/bg-login.jpg') }} )" 
	>
        <div id="wrapper">
            @yield('content')
        </div>
    </body>
</html>

<!-- Stored in resources/views/child.blade.php -->
@extends('front.layout')
@section('content')
<div class="accountbg"></div>
<div class="wrapper-page">


    <div class="panel panel-color panel-primary panel-pages login-box">
        <div class="panel-body">
            <a href="{{url('')}}" class="login__logo mb20 text-center db">
                <img src="{{asset('front/assets/images/logo.png')}}">
            </a>

            <h4 class="text-muted text-center m-t-0"><b>School Login</b></h4>
            @include('front/common/errors')
            <div class="form">
                <form method="POST" class="form" action="{{ url('school/login') }}">
                    <input type="hidden"  name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>
                            <input type="text" class="form-control" name="email" placeholder="Email Address" value="blue" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-lock"></i>
                            </span>
                            <input type="password" class="form-control" placeholder="Password" name="password" value="qwerty"  required  />
                        </div>
                    </div>
                    <div class="form-group0  mb10 row">
                        <div class="col-sm-7"> <a href="{{ url('forgot-password') }}" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a></div>
                        <div class="col-sm-5 text-right"> <a href="{{ url('register') }}" class="text-muted">Create an account</a></div>
                    </div>

                    <div class="note text-muted pb20">
                       <b>Note:</b> 
                       The password must have a strength to be minimum of 8 characters, 1 upper case, 1 numeric and 1 special character.
                    </div>

                    <div class="clearfix"></div>


                    <div class="form-group">
                        <button ype="submit" class="form-control w100 btn-primary" >LOGIN</button>
                    </div>
                </form>
            </div>


        </div>
    </div>



    <div class="cr-area text-center">
        <p>©2018 Copyrights theTeacherCloud Ltd . All Rights Reserved.</p>

        <div class="listdvr">
            <ul>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Learn more</a></li>
            </ul>
        </div>

    </div>


</div>
@endsection
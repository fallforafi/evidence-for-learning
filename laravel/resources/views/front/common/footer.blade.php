<footer class="footer"> © 2018 EFL - All Rights Reserved. </footer>


<div id="themeChanger">
    <label><input value="red" type="radio" name="themeChanger" /> Red</label>
    <label><input value="blue" type="radio" name="themeChanger" />Blue</label>
    <label><input value="orange" type="radio" name="themeChanger" />Orange</label>
    <label><input value="green" type="radio" name="themeChanger" />Green</label>
    <label><input value="black" type="radio" name="themeChanger" />Black</label>
</div>

<script>

$('[name="themeChanger"]').on("change", function () {
    var themeColor = $(this).val();
   $("body").attr("data-theme", themeColor);
});


</script>



<style>
div#themeChanger {
    position: fixed;
    bottom: 0;
    z-index: 99999;
    background-color: #f7f7f7;
    padding: 10px;
    width: 120px;
    border-left: 20px solid #eee;
    right: -109px;
}

div#themeChanger:before {
    content: "<";
    position: absolute;
    top: 34%;
    left: -16px;
    font-size: 30px;
    color: #fff;
}

div#themeChanger:hover {
    right: 0;
}

div#themeChanger>label>input {
    /* display: none; */
    float: left;
    margin: 6px 4px 0px;
}

div#themeChanger>label {
    background-color: #666;
    display: block;
    padding: 0px;
    color: #fff; 
    font-size: 14px;
}

</style>


@if (count($errors) > 0)
<div class="">
    <div class="alert alert-danger alert-dismissable alert-sticky">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <div class="cont">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            <div class="alert__icon"><span></span></div>
        </div>
    </div>
</div>
@endif
@if (Session::has('success'))
<div class="">
    <div class="alert alert-success alert-dismissable  alert-sticky">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

        <div class="cont">
            <ul>
                <li><i class="icon fa fa-check"></i> &nbsp  {!! session('success') !!}</li>
            </ul>

            <div class="alert__icon"><span></span></div>
        </div>
    </div>
</div>
@endif
@if (Session::has('danger'))
<div class="">
    <div class="alert alert-danger alert-dismissable  alert-sticky">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <div class="cont">
            <ul>
                <li><strong>Whoops!</strong> There were some problems with your input.</li>
                <li><h5>{!! session('danger') !!}</h5></li>
            </ul>
            <div class="alert__icon"><span></span></div>
        </div>
    </div>
</div>
@endif

<div id="errors"></div>

<script>
    function showErrors(div, response) {
        var errorsHtml = '<div class="alert alert-danger alert-sticky"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="cont"><ul>';
        $.each(response.errors, function (key, value) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });

        errorsHtml += '</ul><div class="alert__icon"><span></span></div></div></div>';
        $('#' + div).html(errorsHtml).show();
    }
</script>
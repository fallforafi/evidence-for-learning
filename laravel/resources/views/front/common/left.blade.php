<div class="left side-menu">
    <div class="slimScrollDiv">
        <div class="sidebar-inner slimscrollleft" >
            <div id="sidebar-menu">
                <ul>
                    
                    <li class="menu-title bg-cvr" style="background-image:url('{{asset('')}}/front/assets/images/profile-bg.jpg')">
                        <span class="profile__logo">
                            <img class="schoolLogo" src="" alt="loading..." />
                        </span>
                        <?php
                        if ($session['loginType'] == 'school') {
                            ?>
                            <span class="profile__label">
                                <i class="icon"><img src="{{asset('')}}/front/assets/images/package-icon.png" alt="" /></i>	<?php echo $session['plan']; ?>
                            </span>
                        <?php } ?>
                    </li>
                    
                    <li> <a href="{{ url('dashboard')}}" class="waves-effect"><i class="ion-ios7-home"></i><span> Evidence</span></a></li>
                    <li> <a href="{{ url('learners')}}" class="waves-effect"><i class="ion-ios-book"></i><span> Learners</span></a></li>
                    <?php
                    if ($session['loginType'] == 'school') {
                        ?>
                        <li class="mt30"> <a href="{{ url('users')}}" class="waves-effect"><i class="ion-person"></i><span> User Manager</span></a></li>
                    <?php } ?>
                        <?php
                    /*if ($session['loginType'] == 'user') {
                        ?>
                        <li> <a href="{{ url('profile')}}" class="waves-effect"><i class="ion-person"></i><span> Profile</span></a></li>
                        <li> <a href="{{ url('change-password')}}" class="waves-effect"><i class="ion-person"></i><span> Change Password</span></a></li>
                    <?php } */ ?>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>

    </div>
</div> 








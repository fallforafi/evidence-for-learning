<?php

?><div class="topbar">
    <div class="topbar-left">
        <div class="text-center"> 
            <a href="{{url('/')}}" class="logo">
                <img src="{{asset('')}}/front/assets/images/logo2.png"></a> 
            <a href="{{url('/')}}" class="logo-sm"><img  src="{{asset('')}}/front/assets/images/logo_sm.png"></a>
        </div>
    </div>
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="">
                <div class="pull-left"> 
                    <button type="button" class="button-menu-mobile open-left waves-effect waves-light"> <i class="ion-navicon"></i> </button> <span class="clearfix"></span>
                </div>


                <ul class="nav navbar-nav navbar-right pull-right navbar-box">


                    <li class="icon-logout0 bg-pink">
                        <a href="{{ url('logout')}}" ><i class="ion-log-out"></i></a>
                    </li>
                    <!--<li class="icon-bell bg-green">
                        <a href="#"><i class="ion-ios-bell-outline"></i></a>
                    </li>
                    <li class="icon-info  bg-blue">
                        <a href="#"><i class="ion-ios-information-outline"></i></a>
                    </li>-->
                </ul>




                <ul class="nav navbar-nav navbar-link navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu </a>
                        <ul class="dropdown-menu">
                                
                            <?php
                        if ($session['loginType'] == 'school') {
                            ?>
                            <li><a href="{{ url('users')}}" class="waves-effect"><i class="ion-person"></i><span> User Manager</span></a></li>
                            <li><a href="<?php echo Config('params.site_admin_url').'/'.$session['schoolId']; ?>" class="waves-effect" target="_blank"><i class="ion-person"></i><span>Admin Home</span>  </a></li>
                        <?php } ?>
                                <li class="divider"></li>
                                <li><a href="{{ url('learners')}}" class="waves-effect"><i class="ion-person"></i><span> User Manager</span></a></li>
                                <li> <a href="{{ url('userfilters')}}" class="waves-effect"><i class="ion-funnel"></i><span> My User Filters</span></a></li>
                                <li class="divider"></li>
                                <li><a href="#aa">Logout</a></li>
<!--
                            <li> <a href="{{ url('dashboard')}}" class="waves-effect"><i class="ion-ios7-home"></i><span> Evidence</span></a></li>
                            <li> <a href="{{ url('learners')}}" class="waves-effect"><i class="ion-ios-book"></i><span> Learners</span></a></li>
                            <?php
                            if ($session['loginType'] == 'school') {
                                
                                ?>
                                <li class=""> <a href="{{ url('users')}}" class="waves-effect"><i class="ion-person"></i><span> User Manager</span></a></li>
                                <li class=""> <a href="<?php echo Config('params.site_admin_url').'/'.$session['schoolId']; ?>" class="waves-effect" target="_blank"><i class="ion-person"></i><span> Admin Home</span></a></li>
                            <?php } ?>
                            <?php
                            if ($session['loginType'] == 'user') {
                                ?>
                                <li> <a href="{{ url('profile')}}" class="waves-effect"><i class="ion-person"></i><span> Profile</span></a></li>
                                <li> <a href="{{ url('change-password')}}" class="waves-effect"><i class="ion-person"></i><span> Change Password</span></a></li>
                            <?php } ?>
-->
                            

                        </ul>
                    </li>
                </ul>

            </div>
        </div>
    </div>
</div>

<header>
    <section class="hdr-area hdr-nav hdr--sticky blur-nav--hover0 cross-toggle fixed" >
        <div class="container">
            <nav class="navbar navbar-default" role="navigation" id="slide-nav">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="{{url('/')}}" class="navbar-brand" >
                            <img src="{{ asset('front/images/logo.png') }}" alt="Logo" />
                        </a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div id="slidemenu">
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                            <ul class="nav navbar-nav navbar-main">

                               <!-- <li><a href="{{url('blog')}}">BLOG</a></li> -->
                                <?php
                                if (isset($_SESSION['user'])) {
                                    ?>
                                    <li class=""><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account<span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                          
                                            <li><a href="{{ url('dashboard')}}" class="pagelinkcolor">Dashboard</a></li>
                                            <!--
                                            <li><a href="{{ url('changepassword')}}" class="pagelinkcolor">Change Password</a></li>
                                            <li><a href="{{ url('profile')}}" class="pagelinkcolor">Profile</a></li>
                                            -->
                                            <li><a href="{{ url('logout')}}" class="pagelinkcolor">Log Out</a></li>
                                        </ul>
                                    </li>


                                    <?php
                                } else {
                                    ?>
                                    <li class="login-link"><a href="{{url('login')}}">Login</a></li>

                                    <?php
                                }
                                ?>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div>
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </section>
</header>
@extends('front.profile')
@section('content')
<div class="content">
    <div class="">
        <div class="page-header-title">
            <h4 class="page-title">Edit Profile</h4>
        </div>
    </div>
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="row0">
                                <div class="col-xs-12 inr-box" id="listing">
                                    {!! Form::model($model, ['class' => 'form','url' => ['updateprofile'], 'method' => 'post']) !!}
                                    <?php
                                    $required = "required";
                                    ?>
                                    @include('admin/commons/errors')

                                    <div class="form-group col-sm-4">
                                        {!! Form::label('First Name') !!}
                                        {!! Form::text('firstName', null , array('class' => 'form-control',$required) ) !!}
                                    </div>
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('Last Name') !!}
                                        {!! Form::text('lastName', null , array('class' => 'form-control',$required) ) !!} 
                                    </div>
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('Email') !!}
                                        {!! Form::text('email', null , array('class' => 'form-control',$required) ) !!} 
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <button type="submit" class="btn btn-primary btn-flat">SAVE</button>                                                  </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
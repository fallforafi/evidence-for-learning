<?php
$required = "required";
//d($model,1);
?>
@include('front/common/errors')
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {display:none;}

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #4CAF50!important;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<h3>User Details</h3>
<div class="form-group col-sm-12">
    {!! Form::label('First Name') !!}
    {!! Form::text('firstName', null , array('class' => 'form-control',$required) ) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('Last Name') !!}
    {!! Form::text('lastName', null , array('class' => 'form-control',$required) ) !!} 
</div>

<div class="form-group col-sm-12">
    {!! Form::label('Role') !!}
    {!! Form::text('role', null , array('class' => 'form-control','placeholder'=>'(optional)') ) !!} 
</div>

<div class="form-group col-sm-12">
    {!! Form::label('Email') !!}
    {!! Form::text('email', null , array('class' => 'form-control',$required) ) !!} 
    Access instructions will be sent to this email address.
</div>

<h3>Login</h3>
<div class="form-group col-sm-12">
    {!! Form::label('username') !!}
    {!! Form::text('username', null , array('class' => 'form-control',$required) ) !!}
</div>
@if(isset($model) && $model->_id != '')

@if(isset($model) && $model->status == 1)
<div class="form-group col-sm-12" id="pwd">
    <label for="pwd">Password</label>
    <a href="{{ url('reset/user/password/'.$model->_id) }}">Reset Password</a>
<!--    <input type="password" class="form-control" name="password" id="password" placeholder="Password *" >-->
</div>
@endif
<!--<div class="form-group col-sm-4">
    <label for="pwd">Confirm Password</label>
    <input type="password" class="form-control" data-match-error="Whoops, these don't match" data-match="#password" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password *" >
</div>-->


<div class="form-group col-sm-4">
    <label>Status</label>
    <label class="switch">
        {!! Form::checkbox('status', 1, null, ['class' => '','id'=>'statusChecked']) !!} 
        <span class="slider round"></span>
    </label>
    <label id="statusCheckedMsg"></label>
</div>
@endif
<!--<div class="form-group col-sm-2">
    {!! Form::label('External ID') !!}
    {!! Form::text('externalId', null , array('class' => 'form-control',$required) ) !!} 
</div>-->


<div class="clearfix"></div>

<div class="form-group col-sm-4">
        <label class="switch">
                {!! Form::checkbox('manager', 1, null, ['class' => '']) !!} 
                <span class="slider round"></span>
            </label>
            
    <label>Assessment Manager</label>


</div>
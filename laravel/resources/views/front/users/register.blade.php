@extends('front')

@section('content')
<?php
$passwordPattern = Config::get('params.password_pattern');
$required = 'required';
//$required = '';
?>


<section class="inr-intro-area pt30">
    <div class="container">

        @if (count($errors->register) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->register->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if (Session::has('success'))
        <div class="alert alert-success">
            <h4><i class="icon fa fa-check"></i> &nbsp  {!! session('success') !!}</h4>
        </div>
        @endif
    </div>
</section>

{!! Form::open(array( 'class' => 'form','url' => 'signUpPost', 'name' => 'register')) !!}

<div class="accountbg"></div>
  <div class="wrapper-page">
    <div class="panel panel-color panel-primary panel-pages">
      <div class="panel-body">
       <h3 class="text-center m-t-0 m-b-15"> <a href="{{url('')}}" class="logo"><img src="{{asset('')}}/front/assets/images/logo.png"></a></h3>
        <h4 class="text-muted text-center m-t-0"><b>Sign Up</b></h4>
        <div class="fom fom-shad form-horizontal m-t-20">

            <div class="form-group">  
                {!! Form::text('firstName', Request::input('firstName') , array('placeholder'=>"First Name *",'class' => 'form-control',$required) ) !!}
            </div>
              <div class="form-group">   
                {!! Form::text('lastName', null , array('placeholder'=>"Last Name *",'class' => 'form-control',$required) ) !!}
            </div>

             <div class="form-group">  
                {!! Form::text('email', null , array('placeholder'=>"Email *",'class' => 'form-control',$required) ) !!}
            </div>

            <div class="form-group">  
                {!! Form::password('password', array('placeholder'=>"Password *",'class' => 'form-control','pattern' => $passwordPattern,$required) ) !!}

            </div>

              <div class="form-group">  
                {!! Form::password('password_confirmation', array(

                'data-match-error'=>"Whoops, these don't match",
                'placeholder'=>"Confirm Password *",
                "data-match"=>"#password",
                'class' => 'form-control',$required))
                !!}
            </div>
           <div class="form-group">  
                <div class="alert alert-info">
                    Your password must contain minimum 8 characters, at least 1 uppercase alphabet, 1 lowercase alphabet, 1 number and 1 special character.
                </div> 
            </div>

              <div class="form-group clrhm">  
                <h5>{!! Form::label('gender', 'Gender *') !!}</h5>
                <div class="inline-form">

                    {!! Form::radio('gender', 'm', true) !!}
                    {!! Form::label('Male', 'Male') !!}
                    {!! Form::radio('gender', 'f') !!}
                    {!! Form::label('Female', 'Female') !!}
                </div>
            </div>

            <div class="form-group clrhm">  
                <label>{!! Form::label('gender', 'Date of birth*') !!}</label>
                <br>
                <div style="margin-right:0; margin-left:0;" class="form-group col-sm-4">
                    {!! Form::selectRange('date',1,31,null,['class' => 'form-control',$required]) !!}
                </div>

                <div style="margin-right:0; margin-left:0;" class="form-group col-sm-4">
                    {!! Form::selectMonth('month',null, ['class' => 'form-control',$required]) !!}
                </div>
                <div style="margin-right:0; margin-left:0;" class="form-group col-sm-4">
                    {!! Form::selectRange('year',2016,1930,null,['class' => 'form-control',$required])!!}
                </div>

            </div>

            <div class="form-group">
                {!! Form::text('country', null , array('placeholder'=>"Country United States (US)",'class' => 'form-control','readonly' => 'readonly',$required) ) !!}
            </div>

            <div class="form-group">
                <select name="state" id="state" <?php echo $required; ?> class="form-control">
                    <option >State *</option>
                    @foreach ($states as $state)
                    <option value="{{ $state->code }}">{{ $state->title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                {!! Form::text('city', null , array('placeholder'=>"City *",'class' => 'form-control',$required) ) !!}
            </div>


            <div class="form-group">
                {!! Form::text('address', null , array('placeholder'=>"Address *",'class' => 'form-control',$required) ) !!}
            </div>


            <div class="form-group">
                {!! Form::text('zip', null , array('placeholder'=>"Postal Code / Zipcode *",'class' => 'form-control',$required) ) !!}
            </div>

            <div class="form-group">
                {!! Form::text('phone', null , array('placeholder'=>"Phone *",'class' => 'form-control',$required) ) !!}
            </div>
			<div class="form-group">
				<div class="text-right"> <a href="{{url('login')}}" class="text-muted">Already have account?</a></div>
			 </div>
            <div class="form-group">
                <button type="submit" class="form-control w100 btn-primary" >SIGNUP</button>
            </div>
            <div class="form-group">
                <small>We do not support orders from the following states:<br>
                    <strong>NY, NJ RI, MD, HI</strong></small>
            </div>	
            {!! Form::hidden('role_id',2) !!}
        </div>
    
	  </div>
      </div>
    </div>
  </div>
	
	
	
{!! Form::close() !!}

@endsection

















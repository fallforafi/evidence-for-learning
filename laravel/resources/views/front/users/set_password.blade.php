@extends('front.layout')

@section('content')
<div class="accountbg"></div>
<div class="wrapper-page">


    <div class="panel panel-color panel-primary panel-pages login-box">
        <div class="panel-body">
            <a href="{{url('')}}" class="login__logo mb20 text-center db">
                <img src="{{asset('front/assets/images/logo.png')}}">
            </a>

            <h4 class="text-muted text-center m-t-0"><b>Set Your Password</b></h4>
            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            @if (Session::has('danger'))
            <div class="alert alert-danger alert-dismissible">
                {!! session('danger') !!}
            </div>
            @endif

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="form">
                <form method="POST" class="form" action="{{ url('postsetpassword') }}">
                    <input type="hidden"  name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_id" value="{{ $user->_id }}">

                    <div class="form-group col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-lock"></i>
                            </span>
                            <input type="password" class="form-control" name="password" placeholder="Password *" value="{{ old('password') }}" required="required"/>
                        </div>  
                    </div>

                    <div class="form-group col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-lock"></i>
                            </span>
                            <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password *" required="required"/>
                        </div>
                    </div>




                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>


        </div>
    </div>

</div>

@endsection
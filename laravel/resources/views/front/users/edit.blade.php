<!-- Stored in resources/views/child.blade.php -->
@extends('front.profile')
@section('content')
<div class="content">
    <div class="">
        <div class="page-header-title">
            <h4 class="page-title">Edit User</h4>
        </div>
    </div>
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="row0">
                                <div class="col-xs-12 inr-box" id="listing">
                                    {!! Form::model($model, ['class' => 'form','url' => ['users/update', $id], 'method' => 'post']) !!}
                                    @include('front.users.form')
                                    <div class="form-group col-sm-12">
                                        <button type="submit" class="btn btn-primary btn-flat">SAVE</button>                                  </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#statusChecked').on('change', function () {
        if (this.checked) {
            // alert('checked');
            $('#statusCheckedMsg').html('Login Active');
            $('#pwd').show();
        } else {
            // alert('Not checked');
             $('#pwd').hide();
            $('#statusCheckedMsg').html('Login Suspended');
        }
    });
</script>
@endsection
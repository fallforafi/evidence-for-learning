<!-- Stored in resources/views/child.blade.php -->
@extends('front.profile')
@section('content')
<div class="content">
    <div class="">
        <div class="page-header-title ">
            <h4 class="page-title">Import Users</h4>
        </div>
    </div>

    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div  id="listing">
                    @include('front/common/errors')
                    {!! Form::open(array( 'class' => 'form','url' => 'users/import','files' => true)) !!}
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!! Form::file('file', null,array('class'=>'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <button type="submit" class="btn btn-primary  btn-flat">SAVE</button>                                      </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
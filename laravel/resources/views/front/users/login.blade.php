<?php
$required = 'required';
?>
@include('front.common.errors')
<form method="POST" class="form" action="{{ url('postLogin') }}">
    <input type="hidden"  name="_token" value="{{ csrf_token() }}">
    <div class="form-group bdr">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="fa fa-user"></i>
            </span>
            <input type="text" class="form-control" name="email" placeholder="Email Address" value="alanw" required />
        </div>
    </div>

    <div class="form-group bdr">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="fa fa-lock"></i>
            </span>
            <input type="password" class="form-control" placeholder="Password" name="password" value="qwerty"  required  />
        </div>
    </div>

    <div class="form-group mb10 overload">
        <div class="checkbox checkbox-primary"> 
            <input id="checkbox-signup" type="checkbox" checked> <label for="checkbox-signup"> Remember me </label></div>
        <!--
                <a href="{{ url('forgot') }}" class="link pul-rgt">
                    <i class="fa fa-support"></i>Lost your passward?
                </a>
        -->
    </div>

    <div class="form-group">
        <button ype="submit" class="form-control w100 btn-primary" >LOGIN</button>
    </div>
</form>
<!-- Stored in resources/views/child.blade.php -->
@extends('front.profile')
@section('content')
<div class="content">
    <div class="">
        <div class="page-header-title">
            <h4 class="page-title">Users 
                <a class="btn btn-info fr mr5" href="{{url('users/export')}}">Export</a>
                <a class="btn btn-warning fr mr5" href="{{url('users/import')}}">Import</a>
                <a class="btn btn-primary fr mr5" href="{{url('users/create')}}">Add New</a>
            </h4>
        </div>
    </div>
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="row">
                                @include("front.common.errors")
                                <div class="col-xs-12" id="listing">

                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Username</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($model as $user)
                                            <tr>
                                                <td><?php echo $user->username; ?></td>
                                                <td><?php echo $user->firstName . ' ' . $user->lastName; ?></td>
                                                <td><?php echo $user->email; ?></td>
                                                <td>
                                                    <a class="btn btn-xs btn-success" href="{{url('users/edit')}}/{{$user->_id}}">Edit</a>                                 <a data-id="{{$user->id}}" href="javascript:void(0);"  class="btn btn-xs btn-danger confirm-delete" data-toggle="modal" data-target="#confirm-delete">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach   
                                        </tbody>
                                    </table>
                                    <div class="col-sm-12">
                                        <?php echo $model->appends(Input::query())->render(); ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" aria-hidden="true" class="close">×</a>
                <h4>You are about to delete.</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to proceed?</p>
            </div>
            <div class="modal-footer">
                <a href="#" id="btnYes" class="btn btn-danger">Yes</a>
                <a href="#" data-dismiss="modal" aria-hidden="true" class="btn btn-info">No</a>
            </div>
        </div>
    </div>
</div>
<script>
    var id = "";
    $('.confirm-delete').on('click', function (e) {
        id = $(this).data('id');
        $('#delete-modal').data('id', id).modal('show');
    });

    $('#btnYes').click(function () {
        window.top.location = "<?php echo url("users/delete") ?>/" + id;
        $('#myModal').modal('hide');
    });

</script>
@endsection

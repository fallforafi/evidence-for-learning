@extends('front.profile')

@section('content')
<?php
$required = "required";
?>
<div class="content">
    <div class="content">

        <div class="page-header-title">
            <h4 class="page-title">Change Password</h4>
        </div>
    </div>	
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">

                @include('front.common.errors')


                {!! Form::open(array( 'class' => 'form','url' => 'postchangepassword', 'method' => 'post')) !!}
                <div class="form-group col-md-12">
                    {!! Form::password('old_password', ['class'=>'form-control','placeholder'=>'Current Password',$required]) !!}
                </div>
                <div class="form-group col-md-12">
                    {!! Form::password('password', ['class'=>'form-control','placeholder'=>'New Password',$required]) !!}
                </div>
                <div class="form-group col-md-12">
                    {!! Form::password('password_confirmation', ['class'=>'form-control','placeholder'=>'Confirm Password',$required]) !!}
                </div>                        
                <div class="form-group col-md-12">
                    <input type="submit" name="submit" class="btn btn-primary pull-right" value="Submit">
                </div>
                {!! Form::close() !!} 

            </div> 
        </div> 
    </div>

</div>    			
@endsection
@extends('front.layout')

@section('content')
<div class="accountbg"></div>
<div class="wrapper-page">


    <div class="panel panel-color panel-primary panel-pages login-box">
        <div class="panel-body">
            <a href="{{url('')}}" class="login__logo mb20 text-center db">
                <img src="{{asset('front/assets/images/logo.png')}}">
            </a>

            <h4 class="text-muted text-center m-t-0"><b>Forgot Password</b></h4>
            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            @if (Session::has('danger'))
            <div class="alert alert-danger alert-dismissible">
                {!! session('danger') !!}
            </div>
            @endif

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="form">
                <form method="POST" class="form" action="{{ url('postforgotpassword') }}">
                    <input type="hidden"  name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-envelope"></i>
                            </span>
                            <input type="text" class="form-control" name="email" placeholder="Email Address" value="{{ old('email') }}" required="required"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <button ype="submit" class="form-control w100 btn-primary" >Reset My Password</button>
                    </div>
                    <div class="form-group0  mb10 row">
                        <div class="col-sm-12 text-right"> <a href="{{ url('/') }}" class="text-muted">Back to login</a></div>
                    </div>
                </form>
            </div>


        </div>
    </div>

</div>

@endsection
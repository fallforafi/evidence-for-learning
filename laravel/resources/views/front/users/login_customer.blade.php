@extends('login_signup')

@section('content')

  <div class="accountbg"></div>
  <div class="wrapper-page">
    <div class="panel panel-color panel-primary panel-pages">
      <div class="panel-body">
        <h3 class="text-center m-t-0 m-b-15"> <a href="{{url('')}}" class="login__logo"><img src="{{asset('')}}/front/assets/images/logo.png"></a></h3>
        
          <div class="form-horizontal m-t-20">
		  
            @include('front.customers.login')
                  
		   <div class="form-group m-b-0">
            <div class="col-sm-7"> <a href="{{ url('forgot') }}" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a></div>
            <div class="col-sm-5 text-right"> <a href="{{ url('register') }}" class="text-muted">Create an account</a></div>

            
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
<!-- Stored in resources/views/child.blade.php -->
@extends('front.profile')
@section('content')
<div class="content">
    <div class="">
        <div class="page-header-title">
            <h4 class="page-title">Filters 
            </h4>
        </div>
    </div>
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="row">
                                <b>Note: </b>New filters can be created when filtering the learners.
                            </div>
                            <div class="row">
                                @include("front.common.errors")
                                <div class="col-xs-12" id="listing">



                                    <?php
                                    if (count($model) > 0) {
                                        ?>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($model as $user)
                                                <tr>
                                                    <td><?php echo $user->name; ?></td>
                                                    <td><a data-id="{{$user->id}}" href="javascript:void(0);"  class="btn btn-danger confirm-delete" data-toggle="modal" data-target="#confirm-delete">Delete</a>
                                                    </td>
                                                </tr>
                                                @endforeach   
                                            </tbody>
                                        </table>
                                        <div class="col-sm-12">
                                            <?php echo $model->appends(Input::query())->render(); ?>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-sm-12">
                                            No record found.
                                        </div>
                                    <?php } ?>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" aria-hidden="true" class="close">×</a>
                <h4>You are about to delete.</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to proceed?</p>
            </div>
            <div class="modal-footer">
                <a href="#" id="btnYes" class="btn btn-danger">Yes</a>
                <a href="#" data-dismiss="modal" aria-hidden="true" class="btn btn-info">No</a>
            </div>
        </div>
    </div>
</div>
<script>
    var id = "";
    $('.confirm-delete').on('click', function (e) {
        id = $(this).data('id');
        $('#delete-modal').data('id', id).modal('show');
    });

    $('#btnYes').click(function () {
        window.top.location = "<?php echo url("userfilters/delete") ?>/" + id;
        $('#myModal').modal('hide');
    });

</script>
@endsection

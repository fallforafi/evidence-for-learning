<!-- Stored in resources/views/child.blade.php -->
@extends('front.profile')
@section('content')
<div class="content">
    <div class="">
        <div class="page-header-title">
            <h4 class="page-title">Tags</h4>
        </div>
    </div>
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="row">
                                @include("front.common.errors")
                                <div class="col-xs-12" id="listing">

                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>ID</th>
                                                <th>Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($model as $tag)
                                            <tr>
                                                <td><a class="btn btn-success" href="{{url('tags/edit')}}/{{$tag->_id}}">Edit</a> | <a href="{{url('tags/delete')}}/{{$tag->_id}}" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete">Delete</a>
                                                </td>
                                                <td><?php echo $tag->_id; ?></td>
                                                <td><?php echo $tag->tagName; ?></td>
                                            </tr>
                                            @endforeach   
                                        </tbody>
                                    </table>
                                    <div class="col-sm-12">
                                        <?php echo $model->appends(Input::query())->render(); ?>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
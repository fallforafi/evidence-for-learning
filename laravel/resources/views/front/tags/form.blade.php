<?php
$required = "required";
?>
@include('admin/commons/errors')
<div class="form-group">
    {!! Form::checkbox('internalUse', 1, null, ['class' => '']) !!} Internal Use
</div>

<div class="form-group">
    {!! Form::label('Tag Name') !!}
    {!! Form::text('tagName', null , array('class' => 'form-control',$required) ) !!}
</div>
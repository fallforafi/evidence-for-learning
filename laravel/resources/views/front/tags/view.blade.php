<!-- Stored in resources/views/child.blade.php -->
@extends('front.profile')

<!--
  @section('title', 'Page Title')
@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection
-->
@section('content')


<div class="content">
    <div class="">
        <div class="page-header-title">
            <h4 class="page-title">Learner (<?php echo $model->firstName . " " . $model->lastName; ?>)</h4>
        </div>
    </div>

    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="tabs-vertical-env">
                        <ul class="nav tabs-vertical">
                            <li class="active">
                                <a href="#v-learner" data-toggle="tab" aria-expanded="true">Learner Profile</a>
                            </li>
                            <li class="">
                                <a href="#v-profile" data-toggle="tab" aria-expanded="false">Evidence({{count($evidences)}})</a>
                            </li>
                            <li class="">
                                <a href="#v-messages" data-toggle="tab" aria-expanded="false">Documents({{count($documents)}})</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="v-learner">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <?php //echo json_encode($model); ?>
                                                    <table class="table table-hover">
                                                        <tbody>
                                                            <tr>
                                                                <td><strong>Name</strong></td>
                                                                <td><?php echo $model->firstName . " " . $model->lastName; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Email</strong></td>
                                                                <td><?php echo $model->email; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>External Id:</strong></td>
                                                                <td><?php echo $model->uniqueRef; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Gender:</strong></td>
                                                                <td><?php echo $model->gender; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Date Of Birth:</strong></td>
                                                                <td><?php
                                                                    if (isset($model->dateOfBirth)) {
                                                                        $datetime = $model->dateOfBirth->toDateTime();
                                                                        $mydate = $datetime->format('m/d/Y');
                                                                    } else {
                                                                        $mydate = "-";
                                                                    }
                                                                    echo $mydate;
                                                                    ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Parent 1 Name:</strong></td>
                                                                <td><?php
                                                                    echo $model->parent1FirstName . " " . $model->parent1LastName;
                                                                    ;
                                                                    ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Parent 1 Email</strong></td>
                                                                <td><?php echo $model->parent1Email; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Parent 2 Name:</strong></td>
                                                                <td><?php
                                                                    echo $model->parent2FirstName . " " . $model->parent2LastName;
                                                                    ;
                                                                    ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Parent 2 Email</strong></td>
                                                                <td><?php echo $model->parent2Email; ?></td>
                                                            </tr>
                                                            <?php foreach ($attributes as $key => $attribute) { ?>
                                                                <tr>
                                                                    <td><?php echo $attribute ?></td>
                                                                    <td><?php
                                                                        $attributeName = "attribute" . $key;
                                                                        if ($model->$attributeName == "1")
                                                                            echo '<i class="fa fa-check" aria-hidden="true"></i>';
                                                                        ?></td>
                                                                </tr>
                                                            <?php } ?>


                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                            <div class="tab-pane" id="v-profile">
                                <?php
                                foreach ($evidences as $evidence) {

                                    $datetime = $evidence->evidenceDate->toDateTime();
                                    $mydate = $datetime->format('m/d/Y H:i a');
                                    ?>
                                    <div class="item  col-xs-4 col-lg-4">
                                        <div class="thumbnail">
                                            <img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
                                            <div class="caption">
                                                <h4 class="group inner list-group-item-heading">

                                                    {{$evidence->subscriptionDeviceID}}
                                                </h4>
                                                <p class="group inner list-group-item-text">


                                                </p>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-6">
                                                        <p class="lead">
                                                            <?php echo $mydate ?></p>
                                                    </div>
                                                    <br clear="all"/>
                                                    <div class="col-xs-12 col-md-6">
                                                        <a class="btn btn-success" href="{{url('evidence/')}}/{{$evidence->_id}}">View Evidence</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="tab-pane" id="v-messages">
                                <?php
                                ?>
                                @if(count($documents)>1)
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>View By Parent</th>
                                            <th>File Size</th>
                                            <th>Created Dated</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>   
                                        <?php
                                        foreach ($documents as $document) {

                                            $datetime = $document->dateCreated->toDateTime();
                                            $mydate = $datetime->format('m/d/Y H:i a');
                                            ?>
                                            <tr>
                                                <td><?php echo $document->docTitle ?></td>
                                                <td><?php echo $document->parentView ? "Yes" : "No"; ?></td>
                                                <td><?php echo $document->fileSize ?> Bytes</td>
                                                <td><?php echo $mydate; ?></td>
                                                <td><a class="btn btn-info" href="<?php echo $document->url; ?>" >View Documents</a></td></tr>
                                            <?php } ?>
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<?php
$image = env("S3_BUCKET_URI") . $id;
if ($ext == "mp4") {

    
?>
    <video controls>
        <source src="<?php echo data_uri($image, "video/mp4") ?>" type="video/mp4">
        Your browser does not support HTML5 video.
    </video>
<?php
} else {
?>
    <img class="group list-group-image" src="<?php echo data_uri($image, "image/jpeg") ?>" alt="" />
<?php 
} 
?>


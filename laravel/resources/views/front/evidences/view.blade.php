<!-- Stored in resources/views/child.blade.php -->
@extends('front.profile')

<!--
  @section('title', 'Page Title')
@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection
-->
@section('content')
<?php
$dateFormat = Config("params.date_format");
?>

<div class="content">
    <div class="">
        <div class="page-header-title">

            @include("front.evidences.delete")


            <a data-id="{{$evidence->id}}" href="javascript:void(0);"  class="btn btn-danger fr mt15 ml5 view-image" data-toggle="modal" data-target="#view-image">
                <i class="fa fa-trash"></i>
            </a>
            <button class="btn btn-default fr mt15" onclick="window.location.href = '<?php echo url('evidences/edit/' . $evidence->_id); ?>'">Edit</button>
            <h4 class="page-title">Evidence</h4>

        </div>

        <div class="actions fr">

        </div>
    </div>

    <div class="page-content-wrapper ">
        <div class="container">

            <div class="inr-box w100">

                <div class="dtline list-icon-arrow">
                    <!--h4>
                        <i class="icon i32x32 hidden">
                            <img src="{{asset('')}}/front/assets/images/capture_parents@3x.png" alt="" />      
                        </i>
                    </h4-->
                    @if($evidence->parentView == "0")
                    <span class="icon icon-shared icon i32x32">  <img src="{{asset('')}}/front/assets/images/icon-parents-no.png" alt="" /></span>
                    <span>  <b>NOT SHARED with parents</b> </span>
                    @elseif($evidence->parentView == "1")
                    <span class="icon icon-shared icon i32x32">  <img src="{{asset('')}}/front/assets/images/icon-parents.png" alt="" /></span>
                    <span> <b>SHARED with parents</b> Photos/Videos Only<br>

                    </span>
                    @elseif($evidence->parentView == "2")
                    <span class="icon icon-shared icon i32x32">  <img src="{{asset('')}}/front/assets/images/icon-parents.png" alt="" /></span>
                    <span> <b>SHARED with parents</b>  Photos/Videos, Comments and Tags <br>

                    </span>
                    @else

                    @endif

                </div>
                <div class="dtline list-icon-arrow">
                    <h4>
                        <i class="icon i32x32">
                            <img src="{{asset('')}}/front/assets/images/capture_eye@3x.png" alt="" />      
                        </i> Evidence Status
                    </h4>
                    <span> <?php echo $evidence->status == 1 ? "Published" : "Draft"; ?> </span>
                </div>


                <div class="dtline list-icon-arrow">
                    <h4>
                        <i class="icon i32x32">
                            <img src="{{asset('')}}/front/assets/images/capture_learner@3x.png" alt="" />      
                        </i> Learner
                    </h4>
                    <span> <?php echo $model->firstName . " " . $model->lastName; ?> </span>
                </div>
                <div class="dtline list-icon-arrow">
                    <h4><i class="icon i32x32">
                            <img src="{{asset('')}}/front/assets/images/capture_date2@3x.png" alt="" />      
                        </i> Date</h4>
                    <span>
                        <?php
                        if (isset($evidence->evidenceDate)) {
                            $datetime = $evidence->evidenceDate->toDateTime();
                            $date = $datetime->format($dateFormat);
                            $createdDate = date_create($date);

                            if (is_object($createdDate)) {
                                $mydate = $createdDate;
                                $mydate = date_format($createdDate, 'D jS M Y');
                            } else {
                                $mydate = "-";
                            }
                        }
                            echo $mydate;
                            ?>
                        </span>
                    </div>



                    <div class="dtline">
                        <h4><i class="icon i32x32">
                                <img src="{{asset('')}}/front/assets/images/capture_photolibrary@3x.png" alt="" />      
                            </i> Photo(s) / Video(s)</h4>

                        <div class="row">
                            <div class="evibox col-sm-2">
                                <div class="evibox__inr">



                                    <div class="evibox__img">
                                        <a data-id="{{$evidence->image1}}" href="javascript:void(0);"  class="__imgholder view-image" data-toggle="modal" data-target="#view-image">
                                            <div class="__img">
                                                <?php
                                                $image1Tn = env("S3_BUCKET_URI") . $evidence->image1Tn;
                                                ?>
                                                <img class="group list-group-image lazy" src="<?php echo data_uri($image1Tn, "image/jpeg") ?>" alt="" />
                                            </div>
                                        </a>
                                    </div>


                                </div>

                            </div>
                            <?php
                            if ($evidence->image2Tn != "" && isset($evidence->image2Tn)) {
                                $image2Tn = env("S3_BUCKET_URI") . $evidence->image2Tn;
                                ?>
                                <?php // echo env("S3_BUCKET_URI");   ?>
                                <div class="evibox col-sm-2">
                                    <div class="evibox__inr">
                                        <div class="evibox__img">
                                            <a data-id="{{$evidence->image2}}" href="javascript:void(0);"  class="__imgholder view-image" data-toggle="modal" data-target="#view-image">
                                                <div class="__img">
                                                    <img class="group list-group-image lazy" src="<?php echo data_uri($image2Tn, "image/jpeg") ?>" alt="" />
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php
                            if ($evidence->image3Tn != "" && isset($evidence->image3Tn)) {
                                $image3Tn = env("S3_BUCKET_URI") . $evidence->image3Tn;
                                ?>
                                <div class="evibox col-sm-2">
                                    <div class="evibox__inr">
                                        <div class="evibox__img">
                                            <a data-id="{{$evidence->image3}}" href="javascript:void(0);"  class="__imgholder view-image" data-toggle="modal" data-target="#view-image">
                                                <div class="__img">
                                                    <img class="group list-group-image lazy" src="<?php echo data_uri($image3Tn, "image/jpeg") ?>" alt="" />
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>


                    <div class="dtline list-icon-arrow">
                        <h4><i class="icon i32x32">
                                <img src="{{asset('')}}/front/assets/images/capture_comment@3x.png" alt="" />      
                            </i> Comments</h4>
                        <span> <?php echo $evidence->comment; ?> </span>
                    </div>


                    <div class="dtline list-icon-arrow">
                        <h4><i class="icon i32x32">
                                <img src="{{asset('')}}/front/assets/images/capture_framework@3x.png" alt="" />      
                            </i> Frameworks</h4>
                        <span>
                            <ul>
                                <?php foreach ($frameworkItems as $key => $frameworkItem) { ?>
                                    <li><?php echo $frameworkItem->frameworkName ?> > <?php echo $frameworkItem->path ?> > <?php echo $frameworkItem->itemText ?></li>
                                <?php } ?>
                            </ul>
                        </span>
                    </div>



                    <div class="dtline list-icon-arrow">
                        <h4><i class="icon i32x32">
                                <img src="{{asset('')}}/front/assets/images/capture_tag@3x.png" alt="" />      
                            </i> Tags</h4>
                        <span> <?php foreach ($tags as $key => $tag) { ?>
                                <?php echo $tag->tagName . ' ' ?>
                            <?php } ?> </span>
                    </div>

                    <!--
                                    <div class="dtline list-icon-arrow">
                                        <h4><i class="icon i32x32">
                                                <img src="{{asset('')}}/front/assets/images/capture_comment@3x.png" alt="" />      
                                            </i> Parent Comment:</h4>
                                        <span>
                                            Static comment for now..
                                        </span>
                                    </div>
                    -->



                </div>
                <!--HIDE AREA-->
            </div>

            <div class="clearfix"></div>

        </div>
    </div>
    <div class="modal fade" id="view-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" data-dismiss="modal" aria-hidden="true" class="close">×</a>
                    <h4>Image/Video</h4>
                </div>
                <div class="modal-body" id="image_video">
                    Loading..
                </div>
            </div>
        </div>
    </div>
    <script>
        var id = "";
        $('.view-image').on('click', function (e) {
            id = $(this).data('id');
            $('#image_video').html("");
            $.ajax({
                url: "<?php echo url("evidences/file"); ?>?id=" + id,
            type: 'get',
            dataType: 'html',
            cache: true,
            async: true,
            beforeSend: function () {
                // $('#evidences_loader').show();
            },
            complete: function () {
                // $('#evidences_loader').hide();
            },
            success: function (response) {
                $('#image_video').html(response);
            }
        });

    });


</script>
@endsection

<?php
$dateFormat = Session::get("dateFormat");
?><div class="table-area table-responsive">
<table class="table table-hover">
    <thead>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Student Name</th>
            <th>Subscription Device ID</th>
            <th>Evidence Date</th>
        </tr>
    </thead>
    <tbody>
    @foreach($evidences as $key=>$evidence)
    
    <?php
    
    if(isset($evidence->evidenceDate->toDateTime())){
        $datetime = $evidence->evidenceDate->toDateTime();
        $mydate = $datetime->format($dateFormat);
    }
    
    ?>
    
        <tr>
            <td><a href="{{url('evidences/edit/')}}/{{$evidence->_id}}" class="btn btn-success">Edit</a></td>
            <td><a href="{{url('evidence/')}}/{{$evidence->_id}}">{{$evidence->_id}}</a></td>
            <td><?php
                if (isset($learners[$evidence->learnerCloudId])) {
                    echo $learners[$evidence->learnerCloudId];
                }
                ?></td>
            <td>{{$evidence->subscriptionDeviceID}}</td>
            <td><?php echo $mydate; ?></td>

        </tr>
        @endforeach   
    </tbody>
</table>
            </div>

<div class="col-sm-12">
    <?php echo $model->appends(Input::query())->render(); ?>
</div>

<script>

    $('.pagination li a').click(function (e) {

        $.ajax({
            url: this.href,
            type: 'get',
            dataType: 'html',
            cache: false,
            async: true,
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {
                $('#listing').html(response);
            }
        });
        return false;
    });
</script>


<?php
$session = Session::all();
$datepickerFormat = Session::get("datepickerFormat");
$countryCode = Session::get("countryCode");
?>

@include('front.common.errors')
<div class="panel-body">
    <form id="form-filter-evidences" class="form form-inline0" name="form-filter-evidences" novalidate>
        <div class="form-group col-sm-6">
            <label>Frameworks</label>
            <div class="input-group">
                <a class="waves-effect waves-light  form-control form-control--ui" data-toggle="modal" data-target="#frameworkModal">Click Here to Select Framework</a>
                <span class="input-group-addon icon">
                    <img src="{{asset('')}}/front/assets/images/icon-search.png" alt="" />
                </span>
            </div>
            <span class="dtline list-icon-arrow dn">
                <ul>
                    <?php foreach ($frameworkItems as $key => $frameworkItem) { ?>
                        <li><?php echo $frameworkItem->frameworkName ?> > <?php echo $frameworkItem->path ?> > <?php echo $frameworkItem->itemText ?></li>
                    <?php } ?>
                </ul>
            </span>



            <div class="modal fade " id="frameworkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog w80">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Select Framework 

                            </h4>
                            <div id="checkedCount"></div>
                        </div>

                        <div class="modal-body" id="frameworks-body" >

                            <div class="form-group treeviewized levelized">

                                <div class="panel-group panel-select-framework" id="accordion">



                                    <?php
                                    $i = 0;
                                    foreach ($frameworks as $framework) {
                                        ?>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="frameworktitle" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $i; ?>">
                                                        <?php echo $framework['title']; ?> 
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse_<?php echo $i; ?>" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul>
                                                        <?php echo $framework['body']; ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                        $i++;
                                    }
                                    ?>


                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <div class="fl ml-30">    
                                <a class="btn btn-default ml10" id="includePlg" onclick="includePlg()">Include PLG</a>
                                <a style="display:none" class="btn btn-danger ml10" onclick="excludePlg()" id="excludePlg" >Exclude PLG</a>
                            </div>
                            <button type="button" class="btn btn-default mr15" id="reset-button">CLEAR ALL</button>    
                            <button type="button" class="btn btn-default mr-15" data-dismiss="modal">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>


        </div>


        <div class="form-group col-sm-6">
            <label>Tags</label>
            <div class="input-group">
                <select class="form-control select2" name="tags[]"  id="tags"  multiple="multiple">
                    <?php foreach ($tags as $tag) { ?>
                        <option value="<?php echo $tag->_id; ?>"><?php echo $tag->tagName; ?></option>
                    <?php } ?>
                </select>
                <span class="input-group-addon icon">
                    <img src="{{asset('')}}/front/assets/images/icon-search.png" alt="" />
                </span>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="form-group col-sm-2">
            <label>Status</label>
            <div class="input-group">
                <select class="form-control select0002 form-control--ui" name="status"  id="status">
                    <option value="all">All</option>
                    <option value=0>Draft only</option>
                    <option value=1>Published only</option>
                </select>
                <span class="input-group-addon icon">
                    <img src="{{asset('')}}/front/assets/images/icon-search.png" alt="" />
                </span>
            </div>
        </div>
        <div class="form-group col-sm-3">
            <label>Date Range</label>
            <div class="input-group" id="range">

                <input id="reportrange" class="pull-right form-control form-control--ui"  class="form-control" class="pull-right" type="text" name="daterange" value="">

                <span class="input-group-addon">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                </span>
            </div>
        </div>
        <div class="form-group col-sm-2_5">
            <label for="parentCapture">Submitted By Parents</label>
            <div class="input-group">
                <select class="form-control select0002 form-control--ui" name="parentCapture"  id="parentCapture"  >
                    <option value="all">All</option>
                    <option value=0>No</option>
                    <option value=1>Yes</option>
                </select>
                <span class="input-group-addon icon">
                    <img src="{{asset('')}}/front/assets/images/icon-search.png" alt="" />
                </span>
            </div>
        </div>

        <div class="form-group col-sm-2_5">
            <label for="parentView">Shared With Parents</label>
            <div class="input-group">
                <select class="form-control select0002 form-control--ui" name="parentView"  id="parentView"  >                         <option value="all">All</option>
                    <option value="0">Not shared</option>
                    <option value="1">Shared photos/videos</option>
                    <option value="2">Shared photos/videos comments & tags</option>
                </select>
                <span class="input-group-addon icon">
                    <img src="{{asset('')}}/front/assets/images/icon-search.png" alt="" />
                </span>
            </div>
        </div>


        <div class="form-group col-sm-2">
            <label> &nbsp; </label>
            <button type="button" class="btn btn-block btn-primary waves-effect waves-light" onclick="listingEvidences();">APPLY</button>
        </div> 



    </form>
</div>
@include('front.common.js')
<script>
    var cookie = readCookie('esort');
    if (cookie !== null) {
        $('#esort').val(cookie);
    }

    jQuery("#esort").on("change", function () {
        createCookie('esort', $(this).val(), 5);

    });
    $("#reset-button").click(function (e) {
        //$("form").trigger('reset');
        $('.sub_cat_1').removeAttr('checked');
    });


    function getFrameworks(id) {

        $.ajax({
            url: "<?php echo url("frameworks/get"); ?>/" + id,
            type: 'get',
            dataType: 'html',
            beforeSend: function () {
                $('#load-loader').show();
            },
            complete: function (response) {
                $('#load-loader').hide();
            },
            success: function (response) {
                $("#child-frameworks").html(response);
            }
        });
    }

    $(document).ready(function () {
        $('#tags').select2();
        $('#groups').select2();
        $('#frameworks').select2();
        //  $('#status').select2();
    });
</script>

<?php
//$dateRanges="'Test':['02/01/2017','02/12/2017']";
?>
<script type="text/javascript">
    $(function () {
        //moment().subtract(29, 'days');
        var start = "<?php echo $session['subscriptionDate'] ?>";
        var end = moment();

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            locale: {
                format: '<?php echo strtoupper($datepickerFormat); ?>YY'
            },
            ranges: {
                'All': [start, end],
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'This Year': [moment().startOf('year'), moment().endOf('year')],
                'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],<?php echo $dateRanges; ?>
            }
        });

        //cb(start, end);

    });

    $(function () {
        $('.treeviewized li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
        $('.treeviewized li.parent_li > span').on('click', function (e) {
            var children = $(this).parent('li.parent_li').find(' > ul > li');
            jQuery(this).parent("li").toggleClass("is-open");

            e.stopPropagation();
        });
    });
</script>

<style>
    .select2-container {
        width: auto !important;
        display: block;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        if (location.hash != null && location.hash != "") {
            $('.collapse').removeClass('in');
            $(location.hash + '.collapse').collapse('show');
        }
    });

    $(document).ready(function () {
        $("#filterEvidences").click(function () {
            var filterEvidenceDataHtml = '';
            var frameworksCount = $('.sub_cat_1:checked').length;
            if (frameworksCount !== 0) {
                filterEvidenceDataHtml += "<li><b>Frameworks Items: </b>" + frameworksCount + "</li>";
            }
            var tags = $("select[name='tags[]']").serializeArray();
            if (tags.length !== 0) {
                filterEvidenceDataHtml += "<li><b>Tags: </b>" + $("select[name='tags[]'] option:selected").text() + "</li>";
            }
            var status = $("select[name='status'] option:selected");
            if (status.length !== 0) {
                filterEvidenceDataHtml += "<li><b>" + status.text() + "</b></li>";
            }
            var dateRange = $("input[name='daterange']");
            if (dateRange["0"].value !== "") {
                filterEvidenceDataHtml += "<li><b>Date Range: </b>" + dateRange["0"].value + "</li>";
            }
            $('#filterEvidenceDataHtml').html(filterEvidenceDataHtml);
        });
    });
    $('.frameworktitle').on('click', function () {
        var filterEvidenceDataHtml = '';
        var frameworksCount = $('.sub_cat_1:checked').length;
        if (frameworksCount !== 0) {
            filterEvidenceDataHtml += "<b>Selected Frameworks Items: </b>" + frameworksCount;
        }
        $('#checkedCount').html(filterEvidenceDataHtml);
    });
</script>

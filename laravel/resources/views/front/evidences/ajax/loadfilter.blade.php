<div >
    <div class="form-group col-sm-12">
        <label>Filters</label>
        <select class="form-control" name="userfilters[]"  id="userfilters" >
            <?php foreach ($model as $filter) {
                ?>
                <option value='<?php echo json_encode($filter['filters']); ?>'><?php echo $filter['name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<style>
    .select2-container {
        width: auto !important;
        display: block;
    }
</style>

<?php
$required = "required";
$imageExt = array('gif', 'png', 'jpg', 'jpeg');
$videoExt = array('mp4', 'mpeg');

$dateFormat = Session::get("dateFormat");
$datepickerFormat = Session::get("datepickerFormat");
$countryCode = Session::get("countryCode");
?>
@include('front/common/errors')


<div class="row">

    <div class="form-group col-sm-4">
        <label>Evidence Status</label> <br/>
        
        <span class=" checker-area is-active">
        
            <span class="fnc__checkbox">
                {!! Form::checkbox('status', 1, null, ['class' => '']) !!} 
            </span><label>Publish</label> </span>
    </div> 

    
    
    <div class="form-group col-sm-12">
        <!--<i class="icon i32x32">
            <img src="{{asset('')}}/front/assets/images/icon-green-parents.png" alt="" />      
        </i>
        
        <label> Parent Status</label>-->
        @if($model->parentView == "0")
        <span class="icon icon-shared icon i32x32">  <img src="{{asset('')}}/front/assets/images/icon-parents-no.png" alt="" /></span>
        <span>  <b>NOT SHARED with parents</b> </span>
        @elseif($model->parentView == "1")
        <span class="icon icon-shared icon i32x32">  <img src="{{asset('')}}/front/assets/images/icon-parents.png" alt="" /></span>
        <span> <b>SHARED with parents</b> Photos/Videos Only <br>
            
        </span>
        @elseif($model->parentView == "2")
        <span class="icon icon-shared icon i32x32">  <img src="{{asset('')}}/front/assets/images/icon-parents.png" alt="" /></span>
        <span> <b>SHARED with parents</b> Photos/Videos, Comments and Tags <br>

        </span>
        @else
        @endif
    </div>


    <div class="clearfix"></div>

    <div class="form-group col-sm-12">
        <i class="icon i32x32">
            <img src="{{asset('')}}/front/assets/images/capture_eye@3x.png" alt="" />      
        </i>
        <label>Parent View</label>
        <div class="input-group">
            <select class="form-control select0002 form-control--ui" name="parentView"  id="parentView" required="required">
                <option value="0" <?php if ($model->parentView == "0") { ?> selected="selected" <?php } ?>>Not shared</option>
                <option value="1" <?php if ($model->parentView == "1") { ?> selected="selected" <?php } ?>>Shared photos/videos</option>
                <option value="2" <?php if ($model->parentView == "2") { ?> selected="selected" <?php } ?>>Shared photos/videos comments & tags</option>
            </select>
        </div>
    </div>






    <div class="form-group col-sm-12">
        <i class="icon i32x32">
            <img src="{{asset('')}}/front/assets/images/capture_learner@3x.png" alt="" />      
        </i>
        {!! Form::label('Learner Name') !!} 
        {!! Form::text('learners', $learner->firstName." ".$learner->lastName , array('readonly' => 'readonly','class' => 'form-control') ) !!} 
    </div>


    {!! Form::hidden('learner_id', $learner->_id) !!} 

    <!--
    <?php /* ?>
      <div class="form-group">
      <?php

      ?>

      <label>Learner</label>
      <select <?php echo $required; ?> class="form-control select2" name="learners[]"  id="learners"  multiple="multiple">
      <?php foreach ($learners as $learner) { ?>
      <option value="<?php echo $learner->_id; ?>"><?php echo $learner->firstName . ' ' . $learner->lastName ?></option>
      <?php } ?>
      </select>

      <?php
      } else {


      ?>
      {!! Form::label('Learner Name') !!}
      {!! Form::text('learners', $student , array('disabled' => 'disabled','class' => 'form-control') ) !!}

      <?php }
      ?>
      </div>
      <?php */ ?>
    -->

    <?php
    if (!isset($id)) {
        $evidenceDate = date($dateFormat);
    } else {
        $evidenceDate = $model->evidenceDate;
        //$evidenceDate = explode('/', $evidenceDate);
    }
    ?>

    <div class="form-group  col-sm-12">
        <i class="icon i32x32">
            <img src="{{asset('')}}/front/assets/images/capture_date2@3x.png" alt="" />      
        </i>
        {!! Form::label('Date') !!}
        {!! Form::text('evidenceDate', $evidenceDate , array('class' => 'form-control datepicker') ) !!} 
    </div>


</div>

<div class="form-group">
    <div class="hed">
        <i class="icon i32x32">
            <img src="{{asset('')}}/front/assets/images/capture_photolibrary@3x.png" alt="" />      
        </i>
        <label>
            Photo(s) / Video(s)
        </label>
    </div>

    <div class="row">
        <div class="col-sm-2 evibox-upload  <?php
        if (isset($model) && $model->image1Tn == "") {
            echo "no-img";
        }
        ?>">
            <div class="evibox__inr">
                <div class="evibox__img  ">
                    <?php
                    if (isset($model) && $model->image1Tn != "") {
                        $image1Tn = env("S3_BUCKET_URI") . $model->image1Tn;
                        $ext = pathinfo($model->image1Tn, PATHINFO_EXTENSION);
                        if (in_array($ext, $imageExt)) {
                            ?>
                            <img class="lazy" src="<?php echo data_uri($image1Tn, "image/jpeg") ?>"   />
                            <?php
                        } if (in_array($ext, $videoExt)) {
                            ?>
                            <h3>Video Name:</h3>
                            <p>{{$model->image1Tn}}</p>
                            <?php
                        } if (!in_array($ext, $videoExt) && !in_array($ext, $imageExt)) {
                            echo 'Invalid  File Extension!';
                        }
                        ?>

                    <?php } if (isset($model) && $model->image1Tn == "") { ?>
                        <div class="no-image ">No Image Found</div>
                    <?php } ?>
                </div>

                <div class="upload-preview__img">
                    <img class="image" src="" alt="" />
                </div>

                <div class="evibox-upload__btns w100">
                    <span class="btn btn-default btn-resetupload">Cancel</span>
                    <button type="button" class="btn btn-danger  btn-delete" onclick="deleteImage('<?php echo $model->_id ?>', 'image1');">Delete</button> 
                    <span class="form-upload__icon btn btn-primary btn-uploader">
                        <i class="fa fa-file"></i> Upload
                        {!! Form::file('file1', null,array('class'=>'form-control')) !!}
                    </span>
                </div>

            </div>
        </div>


        <div class="col-sm-2 evibox-upload  <?php
        if (isset($model) && $model->image2Tn == "") {
            echo "no-img";
        }
        ?>">
            <div class="evibox__inr">
                <div class="evibox__img">
                    <?php
                    if (isset($model) && $model->image2Tn != "") {
                        $image2Tn = env("S3_BUCKET_URI") . $model->image2Tn;
                        $ext = pathinfo($model->image2Tn, PATHINFO_EXTENSION);
                        if (in_array($ext, $imageExt)) {
                            ?>
                            <img class="lazy" src="<?php echo data_uri($image2Tn, "image/jpeg") ?>"   />
                            <?php
                        }
                        if (in_array($ext, $videoExt)) {
                            ?>
                            <h3>Video Name:</h3>
                            <p>{{$model->image2Tn}}</p>
                            <?php
                        } if (!in_array($ext, $videoExt) && !in_array($ext, $imageExt)) {
                            echo 'Invalid  File Extension!';
                        }
                        ?>

                    <?php } if (isset($model) && $model->image2Tn == "") { ?>
                        <div class="no-image">No Image Found</div>
                    <?php } ?>
                </div>


                <div class="upload-preview__img">
                    <img class="image" src="" alt="" />
                </div>

                <div class="evibox-upload__btns w100">
                    <span class="btn btn-default btn-resetupload">Cancel</span>
                    <button type="button" class="btn btn-danger btn-delete" onclick="deleteImage('<?php echo $model->_id ?>', 'image2');">Delete</button> 
                    <span class="form-upload__icon btn btn-primary btn-uploader">
                        <i class="fa fa-file"></i> Upload
                        {!! Form::file('file2', null,array('class'=>'form-control')) !!}
                    </span>
                </div>

            </div>
        </div>

        <div class="col-sm-2  evibox-upload   <?php
        if (isset($model) && $model->image3Tn == "") {
            echo "no-img";
        }
        ?>">
            <div class="evibox__inr">
                <div class="evibox__img">
                    <?php
                    if (isset($model) && $model->image3Tn != "") {
                        $image3Tn = env("S3_BUCKET_URI") . $model->image3Tn;
                        $ext = pathinfo($model->image3Tn, PATHINFO_EXTENSION);
                        if (in_array($ext, $imageExt)) {
                            ?>
                            <img class="lazy" src="<?php echo data_uri($image3Tn, "image/jpeg") ?>"   />
                            <?php
                        }
                        if (in_array($ext, $videoExt)) {
                            ?>
                            <h3>Video Name:</h3>
                            <p>{{$model->image3Tn}}</p>
                            <?php
                        } if (!in_array($ext, $videoExt) && !in_array($ext, $imageExt)) {
                            echo 'Invalid  File Extension!';
                        }
                        ?>

                    <?php } if (isset($model) && $model->image3Tn == "") { ?>
                        <div class="no-image">No Image Found</div>
                    <?php } ?>
                </div>

                <div class="upload-preview__img">
                    <img class="image" src="" alt="" />
                </div>

                <div class="evibox-upload__btns w100">
                    <span class="btn btn-default btn-resetupload">Cancel</span>
                    <button type="button" class="btn btn-danger  btn-delete" onclick="deleteImage('<?php echo $model->_id ?>', 'image3');">Delete</button> 
                    <span class="form-upload__icon btn btn-primary btn-uploader">
                        <i class="fa fa-file"></i> Upload
                        {!! Form::file('file3', null,array('class'=>'form-control')) !!}
                    </span>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="col-sm-12">

    <div class="modal fade" id="commentTemplateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog w50">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Comments Template</h4>
                </div>
                <div class="modal-body" id="templates-body">
                    
                    <div id="commentsTemps" class="clrlist listview">
                        <ul>
                            
                        @foreach($commentTemplates as $commentTemp)
                        <li> <label for="commentTemplate">
                                <input name="commentTemplate" type="radio" value="{{$commentTemp->commentTemplate}}">
                                {{ $commentTemp->commentTemplate }}
                            </label>
                            </li>
                        @endforeach
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default mr-15" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <i class="icon i32x32">
        <img src="{{asset('')}}/front/assets/images/capture_comment@3x.png" alt="" />      
    </i>
    
    {!! Form::label('comment') !!} <a class="btn btn-sm btn-default" data-toggle="modal" data-target="#commentTemplateModal">Comment Template</a>
    {!! Form::textarea('comment',null,['class'=>'form-control', 'rows' => 3, 'cols' => 40,'id'=>'commentTextarea']) !!}
</div>

<div class="form-group">
    <i class="icon i32x32">
        <img src="{{asset('')}}/front/assets/images/capture_framework@3x.png" alt="" />      
    </i>
    <label>Frameworks</label> <a class="btn btn-sm btn-default"  data-toggle="modal" data-target="#frameworkModal">Add Framework</a>
    <!--<div class="input-group">
        <a class="waves-effect waves-light  form-control form-control--ui" data-toggle="modal" data-target="#frameworkModal">Click Here to Select Framework</a>
        <span class="input-group-addon icon">
            <img src="{{asset('')}}/front/assets/images/icon-search.png" alt="" />
        </span>
    </div>-->
    <span class="dtline list-icon-arrow">
        <ul>
            <?php if(isset($frameworkItems)){
            foreach ($frameworkItems as $key => $frameworkItem) { ?>
                <li><?php echo $frameworkItem->frameworkName ?> > <?php echo $frameworkItem->path ?> > <?php echo $frameworkItem->itemText ?></li>
            <?php }
            }
            ?>
        </ul>
    </span>
    <div class="modal fade " id="frameworkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog w80">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Select Framework
                        <a class="btn btn-sm btn-default ml10" id="includePlg" onclick="includePlg()"><i class="fa fa-check"></i> </i> Include PLG</a>
                        <a style="display:none" class="btn btn-sm btn-danger ml10" onclick="excludePlg()" id="excludePlg" ><i class="fa fa-times"></i> Exclude PLG</a>
                    </h4>

                </div>

                <div class="modal-body" id="frameworks-body" >

                    <div class="form-group treeviewized levelized">

                        <div class="panel-group panel-select-framework" id="accordion">



                            <?php
                            $i = 0;
                            foreach ($frameworks as $framework) {
                                ?>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $i; ?>">
                                                <?php echo $framework['title']; ?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_<?php echo $i; ?>" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul>
                                                <?php echo $framework['body']; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                $i++;
                            }
                            ?>


                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default mr-15" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy'
        });

        $('#learners').select2();
        $('#tags').select2();
        $('#commentsTemps input').click(function () {
            if (this.checked) {
                $target = $('#commentTextarea');
                $li = this.value;
                if (!$target.val().match($(this).val())) {
                    var text = $target.val();
                    $target.val(text + ' ' + $(this).val() + ', ');
                }
                $('#commentTemplateModal').modal('hide');
            }
        });
    });

    $(document).ready(function () {

        $("input:checkbox[name=frameworksItems]:checked").each(function () {
            alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
        });


        $('.checkall').on('change', function () {
            $(".sub_cat_" + this.id).prop('checked', $(this).prop('checked'));
        });
    });

    function deleteImage(id, side) {
        var token = '<?php echo csrf_token(); ?>';

        $.post("<?php echo url("evidence/deleteimage"); ?>", {side: side, id: id, _token: token}, function () {

            window.location.reload();

        });

    }
</script>


<div class="form-group">
    <i class="icon i32x32">
        <img src="{{asset('')}}/front/assets/images/capture_tag@3x.png" alt="" />      
    </i>
    <label>Tags</label>
    <div class="input-group0">
        <select class="form-control form-control-ui select2" name="tags[]"  id="tags"  multiple="multiple">
            <?php foreach ($tags as $tag) { ?>
                <option value="<?php echo $tag->_id; ?>"
                        <?php if (in_array($tag->_id, $selectedTags)) { ?> selected="selected" <?php } ?>
                        ><?php echo $tag->tagName; ?></option>
                    <?php } ?>
        </select>
    </div>
</div>

<script>
    jQuery('[type=file]').change(function () {
        var curElement = jQuery(this).closest(".evibox__inr").find('.upload-preview__img img');
        console.log(curElement);
        var reader = new FileReader();
        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            curElement.attr('src', e.target.result);

        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
        jQuery(this).closest(".evibox__inr").addClass("pre-upload");
    });

    jQuery('.btn-resetupload').click(function () {
        var curElement2 = jQuery(this).closest(".evibox__inr").find('.upload-preview__img img');
        curElement2.attr('src', "");
        var jj = jQuery(this).closest(".evibox__inr").find('[type="file"]').value = "";
        jQuery(this).closest(".evibox__inr").removeClass("pre-upload");
    });




    (function () {
        jQuery(".panel").on("show.bs.collapse hide.bs.collapse", function (e) {
            if (e.type == 'show') {
                jQuery(this).addClass('is-active');
            } else {
                jQuery(this).removeClass('is-active');
            }
        });
    }).call(this);

    jQuery('#accordion').on('shown.bs.collapse', function () {
        console.log("Opened")
        jQuery(this).attr("data-expend", "open");
    });

    jQuery('#accordion').on('hidden.bs.collapse', function () {
        console.log("Closed")
        jQuery(this).attr("data-expend", "close");
    });



</script>

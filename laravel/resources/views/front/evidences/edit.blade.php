<!-- Stored in resources/views/child.blade.php -->
@extends('front.profile')
@section('content')
<?php
$json= json_encode($selectedFrameworkItems);
?>
<div class="content">
    <div class="">
        <div class="page-header-title">
            @include("front.evidences.delete")
            
            
            <a data-id="{{$model->id}}" href="javascript:void(0);"  class="btn btn-danger fr mt15 ml5 view-image" data-toggle="modal" data-target="#confirm-delete">
                <i class="fa fa-trash"></i>
            </a>
            <h4 class="page-title">Edit Evidence</h4>
            
        </div>
    </div>
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12" id="listing">
                                    <div class="inr-box">
                                        {!! Form::model($model, ['class' => 'form','url' => ['evidences/update', $id], 'method' => 'post','files' => true]) !!}
                                        @include('front.evidences.form')
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-flat">SAVE</button>                                  </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    
    $(document).ready(function () {
        var data = <?php echo $json ?>;
         $.each(data, function(key, value){
            $( "#"+value ).prop( "checked", true );
        })
    });
</script>


@endsection

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" aria-hidden="true" class="close">×</a>
                <h4>You are about to delete.</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to proceed?</p>
            </div>
            <div class="modal-footer">
                <a href="#" id="btnYes" class="btn btn-danger">Yes</a>
                <a href="#" data-dismiss="modal" aria-hidden="true" class="btn btn-info">No</a>
            </div>
        </div>
    </div>
</div>
<script>
    var id = "";
    $('.confirm-delete').on('click', function (e) {
        id = $(this).data('id');
        $('#delete-modal').data('id', id).modal('show');
    });

    $('#btnYes').click(function () {
        window.top.location = "<?php echo url("evidences/delete") ?>/" + id;
        $('#myModal').modal('hide');
    });

</script>
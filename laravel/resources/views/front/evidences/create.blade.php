<!-- Stored in resources/views/child.blade.php -->
@extends('front.profile')
@section('content')
<div class="content">
    <div class="">
        <div class="page-header-title">
            <h4 class="page-title">Capture Evidence</h4>
        </div>
    </div>
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12" id="listing">
                                    {!! Form::open(array( 'class' => 'form','url' => 'evidences/save','files' => true)) !!}
                                    @include('front.evidences.form')
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">SAVE</button>                                  </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!-- Stored in resources/views/child.blade.php -->
@extends('front.profile')
@section('content')
<div class="content">
    <div class="">
        <div class="page-header-title">
            <h4 class="page-title">Evidence</h4>
        </div>
    </div>
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    @include("front.learners.filter")
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 nodata">
                    <div class="panel panel-primary mb0">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12" id="listing">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container" id="learners_evidences">

                <!--div class="page-header-title mb0">
                    <h4 class="page-title">Evidence</h4>
                </div-->

                <div class="col-lg-120">
                    <div class="panel-group" id="filters-test-2">
                        <div class="panel panel-info panel-color panel-pink">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#filters-test-2__0d0" href="#filters_evidences" aria-expanded="true" class="collapsed" id="filterEvidences">
                                        Filter Evidence for Selected Learners
                                        <div class="clrlist listdvr dib selected-values __">
                                            <ul id="filterEvidenceDataHtml" title="Please click on 'Show' button ">
                                                <li>Nothing selected</li>
                                            </ul>
                                        </div>
                                    </a>
                                </h4>
                            </div>
                            <div id="filters_evidences" class="panel-collapse collapse out" aria-expanded="true" >

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="row">

                                <!--div class="hed mini col-sm-12">
                                    <h4> <i class="icon"><img src="{{asset('')}}/front/assets/images/icon-photo.png"  alt="" /></i> Photos / Videos</h4>
                                </div-->

                                <div class="clearfix"></div>

                                <div class="evibox-area gridlistview-area" data-view="">
                                    <div class="gridlistview__hed col-sm-12">
                                        <div class=" sortorder-area " onchange="listingEvidences()">
                                            <div class="form-group fr col-sm-3 mr-15">
                                                <div class="input-group">
                                                    <span class="input-group-addon lbl">Sort by:</span>
                                                    <select class="form-control form-control--ui select2" name="esort"  id="esort" >
                                                        <option value="evidenceDate">Evidence Date</option>
                                                        <option value="_created_at">Recent</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="title fl clrhm"><!--h3>List of evidence</h3--></div>
                                        <!--
                                        <div class="form-group fr col-sm-2">
                                            <div class="input-group bg-white">
                                                <span class="input-group-addon lbl">Sort By:</span>
                                                <select id="size" name="size" onchange="listingEvidences();" class="form-control form-control--ui">
                                                    <option value="_created_at">Recent</option>
                                                    <option value="evidenceDate">Evidence Date</option>                 
                                                </select>
                                            </div>
                                        </div>
                                        -->
                                        <div class="gridlistview__actions fr">
                                            <button class="btn-gridview btn btn-sm btn-primary"><i class="glyphicon glyphicon-th-large"></i></button>
                                            <button class="btn-listview btn btn-sm btn-primary"><i class="glyphicon glyphicon-th-list"></i></button>
                                        </div>
                                    </div>

                                    <div class="col-lg-12" id="evidences"></div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="loader p10" id="evidences_loader">
                                    <div class="loaderized"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>


    var learners = {};
    function evidences(p_learners, page) {
        learners = p_learners;

        var form = $("#form-filter-evidences").serialize();
        //var page_size = $("#page_size").val()

        $.ajax({
            url: "<?php echo url("learners/evidences"); ?>?_p_learners=" + p_learners,
            type: 'get',
            dataType: 'html',
            cache: true,
            async: true,
            data: form,
            beforeSend: function () {
                $('#evidences_loader').show();
            },
            complete: function () {

            },
            success: function (response) {

                $('#evidences_loader').hide();
                if (response == "") {
                    return false;
                } else {
                    $('#evidences').html(response);
                    ///evidences(p_learners,page+1)
                }
            }
        });
    }

    function filtersEvidences() {

        $.ajax({
            url: "<?php echo url("evidence/filter"); ?>",
            type: 'get',
            dataType: 'html',
            cache: true,
            async: true,
            data: {},
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {
                $('#filters_evidences').html(response);
            }
        });
    }

    function listingEvidences() {
        var form = $("#form-filter-evidences").serialize();
        $.ajax({
            url: "<?php echo url("learners/evidences"); ?>?_p_learners=" + learners,
            type: 'get',
            dataType: 'html',
            cache: true,
            async: true,
            data: form,
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {
                $('#evidences').html(response);
            }
        });
    }

    $(document).ready(function () {
        filters();
        listing();
        filtersEvidences();

    });


</script>
@endsection

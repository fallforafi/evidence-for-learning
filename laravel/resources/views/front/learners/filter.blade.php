<div class="panel-group mb0" id="filters-test-2">

    
    <div class="panel panel-info panel-color  panel-orange">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#filters-test-2__0" href="#filters" aria-expanded="true" class="collapsed0" id="filterLearners">
                    Filter Learners  <div class="clrlist listdvr dib selected-values">
                        <ul id="filtersData" title="Please click on 'Show' button ">
                            <li>Nothing selected</li>
                        </ul>
                    </div>
                </a>
            </h4>
        </div>


        <div id="filters" class="panel-collapse collapse in" aria-expanded="true" style="">


            <div id="filters_fom"></div>

            <div class=" sortorder-area " onchange="listing()">

                <!--div class="form-group fr col-sm-3">
                    <div class="input-group">
                        <span class="input-group-addon lbl">Sort by:</span>
                        <select class="form-control select2" name="sort"  id="sort" >
                            <option value="_created_at">Recent</option>
                            <option value="firstName">Name</option>
                        </select>
                    </div>
                </div>
            </div-->

                <div class="clearfix"></div>

            </div>
        </div>
    </div>




    <div class="panel panel-info panel-color panel-green">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#filters-test-2__1" href="#filtersTable" aria-expanded="true" class="collapsed0">
                    Selected Filters Records
                </a>
            </h4>
        </div>


        <div  id="filtersTable" class="panel-collapse collapse in" aria-expanded="true" style="">


            <div class="col-xs-12" id="listing"></div>

        </div>
    </div>


</div>
</div>

<script>

    function listing() {
        var form = $("#form-filter").serialize();
        var sort = $("#sort").val();

        $.ajax({
            url: "<?php echo url("learners/listing"); ?>?sort=" + sort,
            type: 'get',
            dataType: 'html',
            cache: true,
            async: true,
            data: form,
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {
                $('#listing').html(response);
            }
        });
    }

    function filters() {

        $.ajax({
            url: "<?php echo url("learners/filter"); ?>",
            type: 'get',
            dataType: 'html',
            cache: true,
            async: true,
            data: {},
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {
                $('#filters_fom').html(response);
            }
        });
    }
</script>

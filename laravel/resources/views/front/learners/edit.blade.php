<!-- Stored in resources/views/child.blade.php -->
@extends('front.profile')
@section('content')
<div class="content">
    <div class="">
        <div class="page-header-title">
            <h4 class="page-title">Add New Learners</h4>
        </div>
    </div>
    <div class="page-content-wrapper ">
        <div class="container0">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12" id="listing">
                                    {!! Form::model($model, ['class' => 'form','url' => ['learners/update', $id], 'method' => 'post']) !!}
                                    @include('front.learners.form')
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>                                  </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
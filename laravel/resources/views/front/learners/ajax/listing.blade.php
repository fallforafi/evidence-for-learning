
<?php
$dateFormat = Config("params.date_format");
?>

<div class="table-area table-responsive">
    <table id="learners_listing_table" class="table table-hover table-striped table-valign table-truncate">
        <thead>
            <tr>
                <th class="hidden"></th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Gender</th>
                <th>D.O.B</th>
                <th>P1</th>
                <th>P2</th>

                <?php foreach ($attributes as $attribute) { ?>
                    <th><?php echo $attribute ?></th>
                <?php } ?>

                <th ></th>
            </tr>
        </thead>
        <tbody>
            @foreach($model as $learner)
            <?php
            // d($learner,1);
            $dob = "";
            if (isset($learner->dateOfBirth)) {
                $datetime = $learner->dateOfBirth->toDateTime();
                $dob = $datetime->format("d-m-Y");
                $mydate = findage($dob);
            } else {
                $mydate = "-";
            }
            ?>
            <tr>

                <td  class="hidden"><input onchange="selectLearners()" type="checkbox" value="{{$learner->_id}}" checked="checked" id="selectedlearners" name="selectedLearners[]"></td>
        <td>
                <span class="lmt">
                        <a href="{{url('learner')}}/{{$learner->_id}}">
                            <?php echo $learner->firstName; ?>
                        </a>
                    </span>
            </td>

            <td>
                <span class="lmt">
                        <a href="{{url('learner')}}/{{$learner->_id}}">
                            <?php echo $learner->lastName; ?>
                        </a>
                    </span>
            </td>

        <td>{{$learner->gender}}</td>
        <td><?php echo $mydate; ?></td>
        <td><?php echo $learner->parent1FirstName . ' ' . $learner->parent1LastName; ?></td>
        <td><?php echo $learner->parent2FirstName . ' ' . $learner->parent2LastName; ?></td>
        <?php foreach ($attributes as $key => $attribute) { ?>
            <td  class="has-icon"><?php
                $attributeName = "attribute" . $key;
                if ($learner->$attributeName == "1")
                    echo '<i class="fa fa-check" aria-hidden="true"></i>';
                ?></td>
        <?php } ?>
        <td width="40"><a   class="btn btn-xs btn-info" href="{{url('evidences/create')}}/{{$learner->_id}}" title=""><i class="fa fa-camera" aria-hidden="true"></i></a></td>
        </tr>
        @endforeach   
        </tbody>
    </table>

</div>

<div class="col-sm-12p0">
    <?php echo $model->appends(Input::query())->render(); ?>
</div>

<script>


    function selectLearners() {

        var values = new Array();
        $.each($("input[name='selectedLearners[]']:checked"), function () {
         //   alert($(this).val());
            values.push("Students$"+$(this).val());
            // or you can do something to the actual checked checkboxes by working directly with  'this'
            // something like $(this).hide() (only something useful, probably) :P
        });
        // var form = $("#form-selected-learners").serialize();
        
        
        
            // JSON.parse() 
        
        evidences(JSON.stringify(values), 15);
    }





    $('.pagination li a').click(function (e) {

        $.ajax({
            url: this.href,
            type: 'get',
            dataType: 'html',
            cache: false,
            async: true,
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (response) {
                $('#listing').html(response);
            }
        });
        return false;
    });


    $(document).ready(function () {
        evidences('<?php echo json_encode($_p_learners) ?>', 1);

        $('#learners_listing_table').dataTable({
            bPaginate: false,
            searching: false,
            destroy: true,

            bInfo: false,

        });
    });



</script>
<script id="ajaxReloadJS">
    $.getScript("{{ asset('front/assets/js/kodeized.js') }}").done(function (script, textStatus) {
        console.log("-> Kodeized  Reloaded : status " + textStatus);
    }).fail(function (jqxhr, settings, exception) {
        console.log("-> Kodeized Reloaded : Failed " + textStatus);
    });
</script>
<?php

function findage($dob) {
    $localtime = getdate();
    $today = $localtime['mday'] . "-" . $localtime['mon'] . "-" . $localtime['year'];
    $dob_a = explode("-", $dob);
    $today_a = explode("-", $today);
    $dob_d = $dob_a[0];
    $dob_m = $dob_a[1];
    $dob_y = $dob_a[2];
    $today_d = $today_a[0];
    $today_m = $today_a[1];
    $today_y = $today_a[2];
    $years = $today_y - $dob_y;
    $months = $today_m - $dob_m;
    if ($today_m . $today_d < $dob_m . $dob_d) {
        $years--;
        $months = 12 + $today_m - $dob_m;
    }

    if ($today_d < $dob_d) {
        $months--;
    }

    $firstMonths = array(1, 3, 5, 7, 8, 10, 12);
    $secondMonths = array(4, 6, 9, 11);
    $thirdMonths = array(2);

    if ($today_m - $dob_m == 1) {
        if (in_array($dob_m, $firstMonths)) {
            array_push($firstMonths, 0);
        } elseif (in_array($dob_m, $secondMonths)) {
            array_push($secondMonths, 0);
        } elseif (in_array($dob_m, $thirdMonths)) {
            array_push($thirdMonths, 0);
        }
    }
    
    $m= $months + ($years*12);
    if($m<60){
        return $m . "m";
    }else{
        return $years . "y " . $months . "m";
    }
    
    
    
}
?>

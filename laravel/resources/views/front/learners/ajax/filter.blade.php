<?php
session_start();
if (!isset($_SESSION['groups'])) {
    $_SESSION['groups'] = array();
}
if (!isset($_SESSION['cohorts'])) {
    $_SESSION['cohorts'] = array();
}
if (!isset($_SESSION['learners'])) {
    $_SESSION['learners'] = array();
}
?>
<div class="panel-body">
    <form id="form-filter" class="form form-inline" name="form-filter" novalidate>


        <div class="form-group col-sm-3 multiselect">
            <label>Groups</label>

            <div class="input-group w100">


                <select class="form-control select2" name="groups[]"  id="groups"  multiple="multiple">
                    <?php foreach ($groups as $group) { ?>
                        <option value="<?php echo $group->_id; ?>" <?php echo (key_exists($group->_id, $_SESSION['groups'])) ? 'selected' : '' ?> ><?php echo $group->groupName; ?></option>
                    <?php } ?>
                </select>

                <span class="input-group-addon icon">
                    <img src="{{asset('')}}/front/assets/images/icon-search.png" alt="" />
                </span>

            </div>
        </div>


        <div class="form-group col-sm-3">

            <label>Cohorts</label>
            <div class="input-group w100">
                <select class="form-control select2" name="cohorts[]"  id="cohorts"  multiple="multiple">
                    <?php foreach ($attributes as $key => $attribute) { ?>
                        <option value="attribute<?php echo $key; ?>" <?php echo (key_exists('attribute' . $key, $_SESSION['cohorts'])) ? 'selected' : '' ?>><?php echo $attribute; ?></option>
                    <?php } ?>
                </select>
                <span class="input-group-addon icon">
                    <img src="{{asset('')}}/front/assets/images/icon-search.png" alt="" />
                </span>

            </div>
        </div>
        <div class="form-group col-sm-3">
            <label>Learners</label>
            <div class="input-group w100">
                <select class="form-control select2" name="learners[]"  id="learners"  multiple="multiple">

                </select>
                <span class="input-group-addon icon">
                    <img src="{{asset('')}}/front/assets/images/icon-search.png" alt="" />
                </span>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                var q = $("#learners").val();

                $('#learners').select2({
                    placeholder: 'Select an learners',
                    closeOnSelect: false,
                    ajax: {
                        url: '<?php echo url("learners/search"); ?>?q=' + q,
                        dataType: 'json',
                        delay: 250,
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    }
                });

            });

        </script>

        <div class="form-group col-sm-3 btns-px-11">
            <label> &nbsp;</label> <br/>
            <button type="button" class="btn btn-primary waves-effect waves-light w35" onclick="listing();">SEARCH</button>

            <button type="button" class="btn btn-default waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-md" >SAVE AS</button>
            <button type="button" class="btn btn-warning waves-effect waves-light " data-toggle="modal" onclick="loadFilters();" data-target=".bs-example-modal-load" >PRESETS</button>

        </div>        
    </form>
</div>
<div class="modal fade bs-example-modal-load" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">USE THE FOLLOWING PRESET FILTER</h4>
            </div>
            <div class="modal-body" >
                <div class="row">
                    <div id="load-filters"></div>

                    <div class="note col-sm-12">You can manage filters including set a default in 'My Profile'.</div>
                </div>
            </div>
            <div class="modal-footer pr0"> 

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button> 
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="setFilters();" >Select</button>
            </div>
        </div>
    </div>  
</div>

<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="saveFilterModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="mySmallModalLabel">SAVE AS A PRESET FILTER</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12" id="filter-errors"></div>

                <div class="form-group"> 
                    <Label>Name:</Label>
                    <div class="input-group">
                        <input type="text" name="name" id="name" class="form-control" placeholder="">
                    </div>
                </div>

                <br clear="all"/>
            </div>
            <div class="modal-footer pr0"> 
                <div id="loading" style="display: none">
                    <center><img src="{{ asset('front/assets/images/loader.gif') }}"></center>
                    <center><h4>Loading..</h4></center>
                </div>
                <button type="button" id="saveFilterCancel" class="btn btn-default waves-effect" data-dismiss="modal">CANCEL</button> 
                <button type="button" id="saveFilterSubmit" class="btn btn-primary waves-effect waves-light" onclick="saveFilter();" >SAVE</button>
            </div>
        </div>
    </div>  
</div>

@include('front.common.errors')
<script>
    $(document).ready(function () {

        $('#groups').select2({closeOnSelect: false});
        $('#cohorts').select2({closeOnSelect: false});
    });

    function setFilters() {
        $('.select2').val(null).trigger('change');
        var filters = $("#userfilters").val();
        filters = JSON.parse(filters);

        if (filters.learners) {
            saveLearnerFilter(filters.learners)
        }

        $.each(filters, function (key, value) {
            $('#' + key).val(value).trigger('change');
        });
        $('.bs-example-modal-load').modal('hide');
        listing();
    }

    function saveLearnerFilter(learners) {

        $.ajax({
            url: "<?php echo url("learners/get"); ?>?learners=" + learners,
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $('#save-loader').show();
            },
            complete: function (response) {
                $('#save-loader').hide();
            },
            success: function (response) {
                var html = "";
                $.each(response, function (key, value) {
                    html += "<option value='" + key + "' selected='selected'>" + value + "</option>";
                    //$('#' + key).val(value).trigger('change');
                });
                $("#learners").html(html);
                listing();
            }
        });
    }

    function loadFilters() {
        $.ajax({
            url: "<?php echo url("learners/loadfilters"); ?>",
            type: 'get',
            dataType: 'html',
            beforeSend: function () {
                $('#load-loader').show();
            },
            complete: function (response) {
                $('#load-loader').hide();
            },
            success: function (response) {
                $("#load-filters").html(response);
            }
        });
    }

    function saveFilter() {
        var name = $("#name").val();
        var filter = {filters: $("#form-filter").serialize(), name: name};

        $.ajax({
            url: "<?php echo url("learners/savefilter"); ?>",
            type: 'get',
            dataType: 'json',
            data: filter,
            beforeSend: function () {
                $('#loading').css('display', 'block');
                $('#saveFilterCancel').hide();
                $('#saveFilterSubmit').hide();
                $('#save-loader').show();
            },
            complete: function (response) {
                $('#loading').css('display', 'none');
                $('#saveFilterCancel').show();
                $('#saveFilterSubmit').show();
                $('#save-loader').hide();
            },
            success: function (response) {
                console.log(response);
                if (response.error === 0) {
//                    var errorsHtml = '<div class="alert alert-success">' +
//                            '<h4><i class="icon fa fa-check"></i> Alert!</h4>' +
//                            '<strong>Successfully Submitted</strong>' +
//                            '</div>';
//                    $('#filter-errors').html(errorsHtml).show();
//                    setTimeout(function () {
//                        window.location.href = '<?php //echo url('userfilters'); ?>';
//                    }, 1500);
                    $('#saveFilterModal').modal('hide');

                    //  $("#message_summary").html(response.message)
                } else {
                    showErrors('filter-errors', response);
                }
            }
        });
    }


    setTimeout(function () {

        $("#cohorts, #tags, #groups").select2({
            placeholder: "Type or select...",

        });

    }, 2000);




</script>
<script type="text/javascript" src="{{ asset('front/assets/js/jquery.cookie.js') }}"></script>
<script type="text/javascript">


    $(document).ready(function () {
        $("#filterLearners").click(function () {
            saveSessionLearner();
            var filterDataHtml = '';
            var groups = $("select[name='groups[]']").serializeArray();
            var cohorts = $("select[name='cohorts[]']").serializeArray();
            var learners = $("select[name='learners[]']").serializeArray();
            //alert('amoos');
            if (groups.length !== 0) {
                filterDataHtml += "<li><b>Groups: </b>" + $("select[name='groups[]'] option:selected").text() + "</li>";
            }
            if (cohorts.length !== 0) {
//                var amooos = $("select[name='cohorts[]']").val();
//                run(amooos);
                filterDataHtml += "<li> <b>Cohorts: </b>" + $("select[name='cohorts[]'] option:selected").text() + "</li>";
            }
            if (learners.length !== 0) {
                filterDataHtml += "<li> <b>Learners: </b>" + $("select[name='learners[]'] option:selected").text() + " </li>";
            }

            $('#filtersData').html(filterDataHtml);
        });
    });

    function saveSessionLearner() {
        var url = '<?php echo url('save-session-learner'); ?>';
        var filters = $("#form-filter").serialize();
        $.ajax({
            type: "GET",
            url: url,
            data: filters
        }).done(function (msg) {
            //alert(msg);
        });
    }

</script>
<style>
    .select2-container {
        width: auto !important;
        display: block;
    }
</style>

<?php
$dateFormat = Config("params.date_format");
?>


<script src="http://afarkas.github.io/lazysizes/lazysizes.min.js" async=""></script>

<div class="evibox-area bg-greeny">
    <div class="row">

        <div class="clearfix"></div>

        @foreach($model as $key=>$evidence)

        <?php
        $datetime = $evidence->evidenceDate->toDateTime();
        $date = $datetime->format($dateFormat);
        $createdDate = date_create($date);

        if (is_object($createdDate)) {
            $mydate = $createdDate;
            $mydate = date_format($createdDate, 'D jS M Y');
        } else {
            $mydate = "-";
        }
        ?>

        <div class="evibox cols-5 fl">
            <div class="evibox__inr" onclick="window.location.href ='{{url('evidence/')}}/{{$evidence->_id}}'">
                <div class="evibox__cont">

                    <div class="actions text-right">

                        <!--
                        <a href="#"><i class="icon icon-circle blue"></i></a>
                        <a href="#"><i class="icon icon-circle cloud">
                                <img src="{{asset('')}}/front/assets/images/icon-cloud.png" alt="" />
                            </i></a>-->

                        <?php if ($evidence->status == "1") { ?>
                            <a href="#"><i class="icon icon-circle green"></i></a>
                        <?php } if ($evidence->status == "0") { ?>
                            <a href="#"><i class="icon icon-circle bg-orange"></i></a>
                        <?php } ?>
                        <?php /* if ($evidence->parentView == "1") { ?>
                          <a href="#"><i class="icon icon-circle blue"></i></a>
                          <?php } */ ?>
                        <?php /* if ($evidence->parentCapture == "1") { ?>
                          <a href="#"><i class="icon icon-circle yellow"></i></a>
                          <?php } */ ?>
                        <?php if ($evidence->parentView == "1") { ?>
                            <a href="#">
                                <i class="icon icon-img blue">
                                    <img src="{{asset('')}}/front/assets/images/icon-parents.png" alt="" />    
                                </i>
                            </a>
                        <?php } ?>
                    </div>

                    <big>
                        <a href="{{url('evidence/')}}/{{$evidence->_id}}">
                            <?php
                            if (isset($learners[$evidence->learnerCloudId])) {
                                echo $learners[$evidence->learnerCloudId];
                            }
                            ?>
                        </a>
                    </big>

                    <time><?php echo $mydate; ?></time>
                    <a href="{{url('evidences/edit/')}}/{{$evidence->_id}}" class="btn-xs btn-success">Edit</a>
                    <?php if ($evidence->parentCapture == "1") { ?>
                        <div class="yellowbar">
                            From Parent/Carer
                        </div>
                    <?php } ?>

                    <div class="for-listview-only">
                        <div class="evibox__comment">
                            <?php echo $evidence->comment; ?>
                        </div>

                        <div class="evibox__comment">
                            <?php echo implode(", ",$evidence->tags)?>
                        </div>

                        <!-- <div class="evibox__comment">
                            <big>Framework:</big>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, harum.
                        </div>
                        -->
                    </div>
                </div>
                <div class="evibox__img">
                    <a href="{{url('evidence/')}}/{{$evidence->_id}}">
                        <?php
                        if ($evidence->image1Tn != "") {
                            $image1Tn = env("S3_BUCKET_URI") . $evidence->image1Tn;
                            ?>
                            <div class="__imgholder col-sm-2  __img1 has-image">
                                <div class="__img">
                                    <img class="lazyload"  src="<?php echo data_uri($image1Tn, "image/jpeg") ?>"   />
                                </div>
                            </div>
                        <?php } elseif ($evidence->image1Tn == "") { ?>
                            <div class="no-image"><?php echo $evidence->comment; ?></div>
                        <?php } ?>

                        <?php
                        if ($evidence->image2Tn != "") {
                            $image2Tn = env("S3_BUCKET_URI") . $evidence->image2Tn;
                            ?>
                            <div class="__imgholder col-sm-2  __img2 has-image">
                                <div class="__img">
                                    <img class="lazyload"  src="<?php echo data_uri($image2Tn, "image/jpeg") ?>"   />
                                </div>
                            </div>
                        <?php } elseif ($evidence->image2Tn == "") { ?>
                            <div class="no-image"><?php echo $evidence->comment; ?></div>
                        <?php } ?>

                        <?php
                        if ($evidence->image3Tn != "") {
                            $image3Tn = env("S3_BUCKET_URI") . $evidence->image3Tn;
                            ?>
                            <div class="__imgholder col-sm-2  __img2 has-image">
                                <div class="__img">
                                    <img class="lazyload"  src="<?php echo data_uri($image3Tn, "image/jpeg") ?>"   />
                                </div>
                            </div>
                        <?php } elseif ($evidence->image3Tn == "") { ?>
                            <div class="no-image"><?php echo $evidence->comment; ?></div>
                        <?php } ?>
                    </a>
                </div>
            </div>
        </div>
        @endforeach   
    </div>
</div>
<div class="col-sm-12p0">
    <?php echo $model->appends(Input::query())->render(); ?>
</div>
@include('front.common.js')
<script>

    $('.col-sm-12p0 li a').click(function (e) {


        $.ajax({
            url: this.href,
            type: 'get',
            dataType: 'html',
            cache: true,
            async: true,
            beforeSend: function () {
                $("#evidences_loader").show();
            },
            complete: function () {

            },
            success: function (response) {
                $("#evidences_loader").hide();
                $('#evidences').html(response);
            }
        });
        return false;
    });

</script>




<script>

    var cookie = readCookie('listgridview');
    if (cookie !== null) {
        jQuery(".gridlistview-area").attr("data-view", cookie);

    } else {
        jQuery(".gridlistview-area").attr("data-view", "grid");
    }

    jQuery(".btn-gridview").on("click", function () {
        createCookie('listgridview', 'grid', 5);
        jQuery(".gridlistview-area").attr("data-view", "grid");
    });
    jQuery(".btn-listview").on("click", function () {
        createCookie('listgridview', 'list', 5);
        jQuery(".gridlistview-area").attr("data-view", "list");
    });

    (function () {
        jQuery(".panel").on("show.bs.collapse hide.bs.collapse", function (e) {
            if (e.type == 'show') {
                jQuery(this).addClass('is-active');
            } else {
                jQuery(this).removeClass('is-active');
            }
        });
    }).call(this);

    jQuery('#accordion').on('shown.bs.collapse', function () {
        console.log("Opened")
        jQuery(this).attr("data-expend", "open");
    });

    jQuery('#accordion').on('hidden.bs.collapse', function () {
        console.log("Closed")
        jQuery(this).attr("data-expend", "close");
    });


jQuery(".daterangepicker .ranges ul li").on("click", function(){
	var dateRangeVal = jQuery(this).text();
	//console.log(dateRangeVal);
	jQuery("#range").attr("data-selected", dateRangeVal);
});

</script>

<?php
$required = "";
?>
@include('admin/commons/errors')

<div class="clearfix"></div>

<div class="form-group col-sm-4">
    {!! Form::label('First Name') !!}
    {!! Form::text('firstName', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-sm-4">
    {!! Form::label('Last Name') !!}
    {!! Form::text('lastName', null , array('class' => 'form-control',$required) ) !!} 
</div>
<div class="form-group col-sm-4">
    {!! Form::label('Email') !!}
    {!! Form::text('email', null , array('class' => 'form-control',$required) ) !!} 
</div>


<div class="form-group col-sm-3">
    {!! Form::label('External ID') !!}
    {!! Form::text('uniqueRef', null , array('class' => 'form-control',$required) ) !!} 
</div>

 
<div class="form-group col-sm-2">
    {!! Form::label('Gender') !!}
    {!! Form::select('gender', ['Male'=>'Male','Female'=>'Female'],'',['class' => 'form-control','name'=>'gender']) !!}
</div>


<div class="form-group col-sm-2">
    {!! Form::label('Date of Birth') !!}
    {!! Form::text('dateOfBirth', null , array('class' => 'form-control datepicker') ) !!} 
</div>
<div class="col-sm-12">
    <h4>
        Parent / Carer 1
    </h4>
</div>
<div class="form-group  col-sm-4">
    {!! Form::label('Parent 1 First Name') !!}
    {!! Form::text('parent1FirstName', null , array('class' => 'form-control') ) !!} 
</div>
<div class="form-group col-sm-4">
    {!! Form::label('Parent 1 Last Name') !!}
    {!! Form::text('parent1LastName', null , array('class' => 'form-control') ) !!} 
</div>
<div class="form-group col-sm-4">
    {!! Form::label('Parent 1 Email') !!}
    {!! Form::text('parent1Email', null , array('class' => 'form-control') ) !!} 
</div>
<div class="col-sm-12">
    <h4>
        Parent / Carer 2
    </h4>
</div>
<div class="form-group col-sm-4">
    {!! Form::label('Parent 2 First Name') !!}
    {!! Form::text('parent2FirstName', null , array('class' => 'form-control') ) !!} 
</div>
<div class="form-group col-sm-4">
    {!! Form::label('Parent 2 Last Name') !!}
    {!! Form::text('parent2LastName', null , array('class' => 'form-control') ) !!} 
</div>
<div class="form-group col-sm-4">
    {!! Form::label('Parent 2 Email') !!}
    {!! Form::text('parent2Email', null , array('class' => 'form-control') ) !!} 
</div>
<div class="col-sm-12">
    <h4>
        Attributes
    </h4>
</div>
<?php foreach ($attributes as $key => $attribute) { ?>
    <div class="form-group col-sm-4">
        <?php $attributeName = "attribute" . $key; ?>
        <span class="fnc__checkbox checker-area sm">
        {!! Form::checkbox($attributeName, 1, null, ['id' => $attributeName]) !!}
        </span>
        {!! Form::label($attributeName, $attribute, array('class' => '')) !!}
    </div>
<?php } ?>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker();
    });
</script>
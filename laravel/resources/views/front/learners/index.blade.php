<!-- Stored in resources/views/child.blade.php -->
@extends('front.profile')
@section('content')
<div class="content">
    <div class="">
        <div class="page-header-title">
            <h4 class="page-title">Learners 

            </h4>
        </div>
    </div>
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12"><b>Note:</b> New filters can be created when filtering the learners.  </div>
                <div class="col-lg-12">
                    @include("front.learners.filter")
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="row">
                                @include("front.common.errors")
                                <div class="col-xs-12" id="listing">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        filters();
        listing();
        
        
    });
</script>



@endsection
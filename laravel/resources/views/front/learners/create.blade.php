<!-- Stored in resources/views/child.blade.php -->
@extends('front.profile')
@section('content')
<div class="content">
    <div class="">
        <div class="page-header-title">
            <h4 class="page-title">Add New Learners</h4>
        </div>
    </div>
    <div class="page-content-wrapper ">
        <div class="container">
            <div class="inr-box w100" id="listing">
                {!! Form::open(array( 'class' => 'form','url' => 'learners/save','files' => true)) !!}
                @include('front.learners.form')
                <div class="form-group col-sm-12">
                    <button type="submit" class="btn btn-primary btn-flat">ADD NEW LEARNER</button>
                </div>
                {!! Form::close() !!}
            </div>


        </div>
    </div>
</div>
@endsection
<!-- Stored in resources/views/child.blade.php -->
@extends('front.layout')

<!--
  @section('title', 'Page Title')
@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection
-->
@section('content')
<div class="accountbg"></div>

<div class="wrapper-page">

    <div class="panel panel-color panel-primary panel-pages login-box">
        <div class="panel-body">

            <a href="{{url('')}}" class="login__logo mb20 text-center db">
                <img src="{{asset('front/assets/images/logo.png')}}">
            </a>

            <div class="form-horizontal00 m-t-20">
                @include('front.users.login')

                <div class="form-group0  m-b-0 row">
                    <div class="col-sm-7"> <a href="{{ url('forgot-password') }}" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a></div>
                </div>
            </div>

        </div>
    </div>



    <div class="cr-area text-center">
        <p>©2018 Copyrights theTeacherCloud Ltd . All Rights Reserved.</p>



    </div>


</div>
@endsection

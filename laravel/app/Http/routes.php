<?php

Route::filter('force.ssl', function() {
    if (!Request::secure()) {
        return Redirect::secure(Request::path());
    }
});

Route::get('/', ['uses' => 'HomeController@index', 'https' => true]);
Route::get('home', ['uses' => 'HomeController@index', 'https' => true]);
Route::get('login', 'SignupController@index');
Route::get('signup', 'SignupController@index');
Route::get('register', 'SignupController@register');
Route::post('signUpPost', 'SignupController@store');
Route::post('postLogin', 'SignupController@postLogin');

Route::get('register/verify/{confirmation_code}', 'SignupController@confirmEmail');
Route::get('register/success/{id}', 'SignupController@success');
Route::post('postsetpassword', 'SignupController@setPassword');


Route::get('dashboard', 'DashboardController@index');
Route::get('dashboard/listing', 'DashboardController@listing');

Route::get('evidence/filter', 'EvidenceController@filter');
Route::get('evidence/{evidenceId}', 'EvidenceController@view');
Route::get('evidences/saved', 'EvidenceController@saved');
Route::get('evidences/updated', 'EvidenceController@updated');
Route::get('evidences/file', 'EvidenceController@file');

Route::get('evidences/create/{learner_id}', 'EvidenceController@create');
Route::post('evidences/save', 'EvidenceController@save');
Route::get('evidences/edit/{id}', 'EvidenceController@edit');
Route::post('evidences/update/{id}', 'EvidenceController@update');
Route::get('evidences/delete/{id}', 'EvidenceController@delete');

Route::post('evidence/deleteimage', 'EvidenceController@deleteimage');

Route::get('learners', 'LearnersController@index');
Route::get('learners/filter', 'LearnersController@filter');
Route::get('learners/listing', 'LearnersController@listing');
Route::get('learners/savefilter', 'LearnersController@savefilter');
Route::get('learners/loadfilters', 'LearnersController@loadfilters');
Route::get('learner/{id}', 'LearnersController@view');
Route::get('learners/evidences', 'LearnersController@evidences');
Route::get('learners/create', 'LearnersController@create');
Route::post('learners/save', 'LearnersController@save');
Route::get('learners/edit/{id}', 'LearnersController@edit');
Route::post('learners/update/{id}', 'LearnersController@update');
Route::get('learners/delete/{id}', 'LearnersController@delete');
Route::get('learners/search', 'LearnersController@search');
Route::get('learners/get', 'LearnersController@get');

Route::get('save-session-learner', 'LearnersController@session');

Route::get('tags', 'TagsController@index');
Route::get('tags/create', 'TagsController@create');
Route::post('tags/save', 'TagsController@save');
Route::get('tags/edit/{id}', 'TagsController@edit');
Route::post('tags/update/{id}', 'TagsController@update');
Route::get('tags/delete/{id}', 'TagsController@delete');

Route::get('school/login', 'LoginController@school');
Route::get('school/force/login/{id}', 'LoginController@forcelogin');
Route::post('school/login', 'LoginController@schoollogin');
Route::get('getschoollogo', 'CommonController@getschoollogo');

Route::get('logout', 'UsersController@logout');
Route::get('users', 'UsersController@index');
Route::get('users/create', 'UsersController@create');
Route::post('users/save', 'UsersController@save');
Route::get('users/edit/{id}', 'UsersController@edit');
Route::post('users/update/{id}', 'UsersController@update');
Route::get('users/delete/{id}', 'UsersController@delete');
Route::get('reset/user/password/{id}', 'UsersController@resetUserPassword');

Route::get('users/export', 'UsersController@export');
Route::get('users/import', 'UsersController@import');
Route::post('users/import', 'UsersController@importcsv');

Route::get('userfilters', 'UserFiltersController@index');
Route::get('userfilters/delete/{id}', 'UserFiltersController@delete');

Route::get('frameworks/get/{id}', 'FrameworksController@get');

Route::get('change-password', 'UsersController@changepassword');
Route::post('postchangepassword', 'UsersController@postchangepassword');
Route::get('profile', 'UsersController@profile');
Route::post('updateprofile', 'UsersController@updateprofile');

Route::get('forgot-password', 'ForgotController@index');
Route::post('postforgotpassword', 'ForgotController@reset_password');
<?php

namespace App\Http\Controllers;

use App\User;
use Session;

class HomeController extends Controller {


    public function __construct() {

    }

    public function index() {
        
        $session = Session::all();
        
        
        
        if (isset($session['schoolId'])) {
            header("Location:" . url("dashboard") . "");
            die;
        }
            return view('front.index');
    }
}

<?php

namespace App\Http\Controllers;

use App\SubscriptionQuota;
use App\UserFilters;
use App\Learners;
use App\Group;
use App\LearnersGroups;
use Session;
use Illuminate\Http\Request;
use Validator,
    Input,
    Redirect;
use App\Evidence;
use App\Documents;
use MongoDB;
use DB;
use App\Log;
use App\Functions\Functions;
use App\Tags;
# use MongoDB\Client as Mongo;

class LearnersController extends Controller {

    private $session;

    public function __construct() {
        $this->session = Session::all();

        if (!isset($this->session['schoolId'])) {
            header("Location:" . url("/") . "");
            die;
        }
    }

    public function session(Request $request) {

        // d($request->all(), 1);
        session_start();
        unset($_SESSION['groups']);
        unset($_SESSION['cohorts']);
        unset($_SESSION['learners']);
        if (isset($request->groups)) {

            foreach ($request->groups as $value) {
                $_SESSION['groups'][$value] = $value;
            }
        }

        if (isset($request->cohorts)) {

            foreach ($request->cohorts as $value) {
                $_SESSION['cohorts'][$value] = $value;
            }
        }
        if (isset($request->learners)) {

            foreach ($request->learners as $value) {
                $_SESSION['learners'][$value] = $value;
            }
        }
    }

    public function index() {

        $schoolId = $this->session['schoolId'];
        $school = SubscriptionQuota::where('schoolId', $schoolId)->first();
        $data['school'] = $school;

        return view('front.learners.index', $data);
    }

    public function get(Request $request) {

        $l = explode(",", $request->learners);
        $learners = Learners::whereRaw(['_id' => ['$in' => $l]])->get();
        $json = array();
        foreach ($learners as $learner) {
            $json[$learner->_id] = $learner->firstName . " " . $learner->lastName;
        }
        echo json_encode($json);
    }

    public function search(Request $request) {

        $learners = Learners::where('firstName', 'like', '%' . $request->q . '%')->orWhere('lastName', 'like', '%' . $request->q . '%')->limit(20)->get();
        $json = array();
        foreach ($learners as $learner) {
            $json[] = ['id' => $learner->_id, 'text' => $learner->firstName . " " . $learner->lastName];
        }
        echo json_encode($json);
        die;
    }

    public function create() {

        $schoolId = $this->session['schoolId'];
        $school = SubscriptionQuota::where('schoolId', $schoolId)->first();
        $data['school'] = $school;

        $model = new Learners();
        $attributes = $this->session['attributes'];
        $data['model'] = $model;
        $data['attributes'] = $attributes;
        return view('front.learners.create', $data);
    }

    public function save(Request $request) {

        $input = $request->all();
        $validation = array(
            'firstName' => 'required',
            'lastName' => 'required',
            'dateOfBirth' => "required",
            'uniqueRef' => 'required|unique:Students',
        );

        $validator = Validator::make($input, $validation);
        $response['error'] = 0;

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $schoolId = $this->session['schoolId'];
            unset($input['_token']);
            $attributes = $this->session['attributes'];
//$id = new MongoDB\BSON\ObjectID;
            $id = Functions::generateRandomString(20);
            $input['_id'] = (string) $id;

            foreach ($attributes as $key => $attribute) {
                $attributeName = "attribute" . $key;
                if (!key_exists($attributeName, $input)) {
                    $input[$attributeName] = "0";
                }
            }
            $input['schoolId'] = $schoolId;
            list($month, $date, $year) = explode('/', $input['dateOfBirth']);
            $input['dateOfBirth'] = $year . '-' . $month . '-' . $date;
//$input['dateOfBirth']=date("Y-m-d H:i:s",strtotime($input['dateOfBirth']));
            $dateOfBirth = new \DateTime($input['dateOfBirth']);
            $input['dateOfBirth'] = new \MongoDB\BSON\UTCDateTime($dateOfBirth->getTimestamp() * 1000);
            $created = new \DateTime();
//$input['_created_at'] = new \MongoDB\BSON\UTCDateTime($created->getTimestamp() * 1000);
//$input['_updated_at'] = new \MongoDB\BSON\UTCDateTime($created->getTimestamp() * 1000);
            $input['is_deleted'] = "0";
            $input['acheived'] = "0";
            $acl['role:' . $schoolId . '_AppUsers']['r'] = true;
            $acl['role:' . $schoolId . '_AppUsers']['w'] = true;
            $input['_acl'] = $acl;
            $input['_rperm'][0] = 'role:' . $schoolId . '_AppUsers';
            $input['_wperm'][0] = 'role:' . $schoolId . '_AppUsers';
            $input['cloudId'] = Functions::getCloudId();
            $input['_created_at'] = date('Y-m-d H:i:s');
            $input['_updated_at'] = date('Y-m-d H:i:s');
            $learner = Learners::create($input);
            $input['eventType'] = "learners.add";
            $input['eventDetail'] = "learner" . ":" . $input['cloudId'] . " was added.";
            Log::createLog($input);

            $input['cloudId'] = Functions::getCloudId();
        }
        \Session::flash('success', 'Learner created.');
        return redirect("learners");
    }

    public function edit($id) {

        $schoolId = $this->session['schoolId'];
        $model = Learners::whereRaw(['_id' => $id])->first();

        $datetime = $model->dateOfBirth->toDateTime();
        $model->dateOfBirth = $datetime->format('m/d/Y');
        $attributes = $this->session['attributes'];
        $data['model'] = $model;
        $data['attributes'] = $attributes;
        $data['id'] = $id;
        return view('front.learners.edit', $data);
    }

    public function update(Request $request, $id) {

        $input = $request->all();
        $validation = array(
            'firstName' => 'required',
            'lastName' => 'required',
        );

        $validator = Validator::make($input, $validation);
        $response['error'] = 0;

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $schoolId = $this->session['schoolId'];
            unset($input['_token']);
            $attributes = $this->session['attributes'];
//$id = new MongoDB\BSON\ObjectID;
//$input['_id'] = (string) $id;

            foreach ($attributes as $key => $attribute) {
                $attributeName = "attribute" . $key;
                if (!key_exists($attributeName, $input)) {
                    $input[$attributeName] = "0";
                }
            }
//d($input,1);
            $input['schoolId'] = $schoolId;
            list($month, $date, $year) = explode('/', $input['dateOfBirth']);
            $input['dateOfBirth'] = $year . '-' . $month . '-' . $date;
            $dateOfBirth = new \DateTime($input['dateOfBirth']);
            $input['dateOfBirth'] = new \MongoDB\BSON\UTCDateTime($dateOfBirth->getTimestamp() * 1000);
            $created = new \DateTime();
            $input['_updated_at'] = new \MongoDB\BSON\UTCDateTime($created->getTimestamp() * 1000);
            Learners::whereRaw(['_id' => $id])->update($input);
        }
        \Session::flash('success', 'Learner updated.');
        return redirect("learners");
    }

    public function listing(Request $request) {

        $input = $request->all();

        $schoolId = $this->session['schoolId'];
        $model = Learners::where('schoolId', $schoolId);

        if (!empty($input['learners'])) {
            $model = $model->whereRaw(['_id' => ['$in' => $input['learners']]]);
        }

        if (!empty($input['groups'])) {
            $learnerGroups = LearnersGroups::whereRaw(['owningId' => ['$in' => $input['groups']]])->get()->toArray();
            $inputGroups = array();

            foreach ($learnerGroups as $inputGroup) {
                $inputGroups[] = $inputGroup['relatedId'];
            }
//->sortBy($input["sort"]);

            $model = $model->whereRaw(['_id' => ['$in' => $inputGroups]]);
        }

        if (!empty($input['cohorts'])) {

            foreach ($input['cohorts'] as $cohort) {
                $model = $model->where($cohort, '1');
            }
        }


        $order = 'desc';
        if (!isset($input['sort'])) {
            $input["sort"] = '_created_at';
        } elseif ($input['sort'] == 'firstName') {
            $order = 'asc';
        }

        $pageSize = 30;
        if (isset($input['page_size'])) {
            $pageSize = (int) $input['page_size'];
        }

        $model = $model->orderBy($input["sort"], $order)->paginate($pageSize);
        //d($model,1);
        $_p_learners = array();
        foreach ($model as $learner) {
            $_p_learners[] = "Students$" . $learner['_id'];
        }
//echo json_encode($_p_learners);
        $attributes = $this->session['attributes'];
        $data['attributes'] = $attributes;
        $data['model'] = $model;

        $data['_p_learners'] = $_p_learners;

        return view('front.learners.ajax.listing', $data);
    }

    public static function showDate($daterange) {
        $string = explode('-', $daterange);
        $date1 = explode('/', trim($string[0]));
        $date2 = explode('/', trim($string[1]));

        $countryCode = Session::get("countryCode");
        if($countryCode=="UK"){
            $date['start'] = $date1[2] . '-' . $date1[1] . '-' . $date1[0];
            $date['end'] = $date2[2] . '-' . $date2[1] . '-' . $date2[0];
        }else{
            $date['start'] = $date1[2] . '-' . $date1[0] . '-' . $date1[1];
            $date['end'] = $date2[2] . '-' . $date2[0] . '-' . $date2[1];
        }
        
        return $date;
    }

    public function evidences(Request $request) {
        $input = $request->all();

        $schoolId = $this->session['schoolId'];
        $_p_learners = json_decode($input['_p_learners']);
        $model = Evidence::where('schoolId', $schoolId)->whereIn('_p_learner', $_p_learners);

//{'tagsPointerArray.objectId':"fkC67WQDH1",schoolId:"963"}
        if (!isset($input['status']) || $input['status'] == "all") {
            
        } elseif ($input['status'] == 0) {
            $model = $model->whereRaw(['is_deleted' => ['$exists' => false]]);
        } elseif ($input['status'] == 1) {
            $model = $model->where("status", "1");
        }

        if (isset($input['frameworksItems'])) {
            $model = $model->whereIn('frameworkItemsPointerArray.objectId', $input['frameworksItems']);
        }

        if (isset($input['tags'])) {
            $model = $model->whereIn('tagsPointerArray.objectId', $input['tags']);
        }

        if (isset($input["daterange"]) && $input["daterange"] != "") {
            $date = self::showDate($input["daterange"]);
            $model = $model->where('_created_at', '>=', new \DateTime($date['start']));
            $model = $model->where('_created_at', '<=', new \DateTime($date['end']));
        }

        if (!isset($input['parentCapture']) || $input['parentCapture'] == "all") {
            
        } else {
            $model = $model->where("parentCapture", '"'.$input['parentCapture'].'"');
        }

        if (!isset($input['parentView']) || $input['parentView'] == "all") {
            
        } else {
            $model = $model->where("parentView", '"'.$input['parentView'].'"');
        }
        


        $order = 'desc';
        if (!isset($input['esort'])) {
            $input["esort"] = 'evidenceDate';
        }

        $pageSize = 15;
        if (isset($input['size'])) {
            $pageSize = (int) $input['size'];
        }
       
        $model = $model->where("is_deleted","0");
        $paginationData = $model->orderBy($input["esort"], $order)->paginate($pageSize);
        if (count($paginationData) == 0) {
            die("No record found.");
        }
        
        $model = $paginationData;

        $evidences = array();
        $learnerCloudIds = array();
        
        
        foreach ($model as $evidence) {
            $evidences[$evidence->_id] = $evidence;
            $learnerCloudIds[] = $evidence->learnerCloudId;
            
            if(isset($evidence->tagsPointerArray)){
                $tagsIds=array();
                foreach($evidence->tagsPointerArray as $tagsPointerArray){
                    $tagsIds[]=$tagsPointerArray['objectId'];
                }
                $modelTags = Tags::where('schoolId', $schoolId)->whereIn('_id', $tagsIds)->get();
                $tags=array();
                
                if(count($modelTags)>0){
                    foreach($modelTags as $tag){
                        $tags[$tag->_id]=$tag->tagName;
                    }
                }
                $evidences[$evidence->_id]['tags']=$tags;
            }else{
                $evidences[$evidence->_id]['tags']=array();
            }
        }

        $modelLearner = Learners::whereIn('cloudId', $learnerCloudIds)->get()->toArray();
        $learners = array();

        foreach ($modelLearner as $learner) {
            $learners[$learner['cloudId']] = $learner['firstName'] . " " . $learner['lastName'];
        }

        $data['learners'] = $learners;
        $data['evidences'] = $evidences;
        $data['model'] = $model;
        return view('front.learners.ajax.evidences', $data);
    }

    public function filter() {

        $schoolId = $this->session['schoolId'];
        $learners = array();
// $learners = Learners::whereRaw(['schoolId' => $schoolId, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['firstName' => 1, 'lastName' => 1]])->get();
        $groups = Group::whereRaw(['schoolId' => $schoolId, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['groupName' => 1]])->get();
        $attributes = $this->session['attributes'];
        $data['learners'] = $learners;
        $data['groups'] = $groups;
        $data['attributes'] = $attributes;
        return view('front.learners.ajax.filter', $data);
    }

    public function loadfilters(Request $request) {
        $schoolId = $this->session['schoolId'];
        $user_id = $this->session['_id'];
      
        
        //$model = UserFilters::;
        
        if ($this->session['loginType'] == 'user') {
            $school = SubscriptionQuota::Where('schoolId', $schoolId)->first()->toArray();
           // d($school,1);
            $model = UserFilters::where("user_id", $user_id)->orWhere("user_id",$school['_id']);
        }else{
            $model = UserFilters::where("user_id",$user_id)->where("schoolId", $schoolId);;
        }


        $model = $model->get()->toArray();
        //d($model,1);
        $i = 0;
        foreach ($model as $row) {

            $data['model'][$i] = $row;

            if (empty($row['learners'])) {
                $row['learners'] = array();
            }
            if (empty($row['cohorts'])) {
                $row['cohorts'] = array();
            }
            if (empty($row['groups'])) {
                $row['groups'] = array();
            }


            $data['model'][$i]['filters']['learners'] = $row['learners'];

            $data['model'][$i]['filters']['groups'] = $row['groups'];
            $data['model'][$i]['filters']['cohorts'] = $row['cohorts'];

            $i++;
        }
        return view('front.learners.ajax.loadfilter', $data);
    }

    public function savefilter(Request $request) {
        $input = $request->all();
        $validation = array(
            'name' => 'required',
            'filters' => 'required',
        );

        $messages = array(
            'name.required' => 'Filter name is required',
            'filters.required' => 'Choose atleast Groups, Learners or Cohorts.',
        );

        $validator = Validator::make($input, $validation, $messages);
        $response['error'] = 0;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $schoolId = $this->session['schoolId'];
            $user_id = $this->session['_id'];
            parse_str($input['filters'], $filter);

            $input["learners"] = array();
            $input["groups"] = array();
            $input["cohorts"] = array();

            if (isset($filter['groups'])) {
                $input["groups"] = $filter['groups'];
            }

            if (isset($filter['cohorts'])) {
                $input["cohorts"] = $filter['cohorts'];
            }

            if (isset($filter['learners'])) {
                $input["learners"] = $filter['learners'];
            }

            $created = new \DateTime();

            unset($input['filters']);

            $id = Functions::generateRandomString(20);
            $input['_id'] = (string) $id;
            $input['user_id'] = $user_id;
            $input['schoolId'] = $schoolId;
            $input['is_deleted'] = 0;
            $input['cloudId'] = Functions::getCloudId();
            $input['_created_at'] = date('Y-m-d H:i:s');
            $input['_updated_at'] = date('Y-m-d H:i:s');
            $model = UserFilters::create($input);
            $input['eventType'] = "userfilters.add";
            $input['eventDetail'] = "userfilters" . ":" . $input['cloudId'] . " was added";
            Log::createLog($input);
            //d($model, 1);
        }
        return json_encode($response);
    }

    public function view($id) {
        $model = Learners::whereRaw(['_id' => $id, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['firstName' => 1, 'lastName' => 1]])->first();


        $attributes = $this->session['attributes'];
        $data['attributes'] = $attributes;
        $evidences = Evidence::whereRaw(['learnerCloudId' => $model->cloudId, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['firstName' => 1, 'lastName' => 1]])->get();

        $documents = Documents::whereRaw(['studentCloudId' => $model->cloudId, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['dateCreated' => 1, 'docTitle' => 1]])->get();

// d($documents,1);

        $data['model'] = $model;
        $data['evidences'] = $evidences;
        $data['documents'] = $documents;
        return view('front.learners.view', $data);
    }

}

<?php

namespace App\Http\Controllers;

use Session;

class CommonController extends Controller {

    private $session;

    public function __construct() {
        $this->session = Session::all();
    }

    public function getschoollogo() {
        $src = "https://www.theteachercloud.net/schools/identity/" . $this->session['schoolId'] . "_logo_title.png";
        echo json_encode(array('src' => $src));
    }

}

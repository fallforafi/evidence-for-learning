<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\UserFilters;
use Session;
use App\Log;

class UserFiltersController extends Controller {

    public function __construct() {

        $this->session = Session::all();

        if (!isset($this->session['schoolId'])) {
            header("Location:" . url("/") . "");
            die;
        }
    }

    public function index() {

        $user_id = $this->session['_id'];
        $model = UserFilters::where('user_id', $user_id)->paginate(20);
        $data['model'] = $model;
        return view('front.userfilters.index', $data);
    }

    public function delete($id) {
        $user = UserFilters::find($id);
        $input['schoolId'] = $user->schoolId;
        $input['eventType'] = "userfilters.delete";
        $input['eventDetail'] = "userfilters" . ":" . $user->cloudId . " was deleted";
        Log::createLog($input);
        $user->delete();

        \Session::flash('success', 'Successfully deleted');
        return redirect("userfilters");
    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use DB;
use App\User;
use App\Orders;
use App\Products;
use Auth;
use Session;
use Illuminate\Http\Request;

class HomeController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        
        $users=array();
        $totalUsers=0;
        
        $orders=array();
        $totalSuccessSales=0;
        
        $ordersPending=array();
        $totalPendingSales=0;
        $ordersTotal=array();
        $totalSales=0;
        $recentOrders=array();
        $products=array();
       

        return view('admin.home', [
            'totalUsers' => $totalUsers,
            'totalSuccessSales' => $totalSuccessSales,
            'totalPendingSales' => $totalPendingSales,
            'totalSales' => $totalSales,
            'recentOrders' => $recentOrders,
            'products' => $products,
        ]);
    }

}

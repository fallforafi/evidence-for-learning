<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use App\Subscriptions;
use Illuminate\Http\Request;

class SubscriptionsController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $subscriptions = Subscriptions::orderBy('title', 'asc')->get();
        return view('admin.subscriptions.index', [
            'subscriptions' => $subscriptions,
        ]);
    }

    public function create() {
        return view('admin.subscriptions.create');
    }

    public function insert(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title' => 'required|max:255|unique:subscriptions',
                    'price' => 'required|numeric',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $model = new Subscriptions;

        $model->title = $request->title;
        $model->description = $request->description;
        $model->initialPrice = $request->initialPrice;
        $model->price = $request->price;
        $model->save();
        return redirect('admin/subscriptions');
    }

    public function edit($id) {
        $subscriptions = Subscriptions::findOrFail($id);
        return view('admin.subscriptions.edit', compact('subscriptions'))->with('id', $id);
    }

    public function update($id, Request $request) {
        $id = $request->id;

        $input = $request->all();
        unset($input['_token']);
        Subscriptions::where('id', '=', $id)->update($input);

        \Session::flash('message', 'Updated Successfully!');
        return redirect('admin/subscriptions');
    }

    public function delete($id) {
        $row = Subscriptions::where('id', '=', $id)->delete();
        return redirect('admin/subscriptions');
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,
    Input,
    Redirect;
use App\User;
use Illuminate\Http\Request;
use App\Functions\Functions;

class ForgotController extends Controller {

    public function __construct() {
    }

    public function index() {
        return view('front.users.forgot');
    }

    public function reset_password(Request $request) {
        $validation = array(
            'email' => 'required|email',
        );

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return redirect('forgot-password')->withErrors($validator->errors());
        }
        
        $user = User::where("email",$request->email)->first();
        if (count($user) == 1) {
            $password = substr(md5(microtime()), rand(0, 26), 7);
            $replaces['name'] = $user->firstName . ' ' . $user->lastName;
            $replaces['password'] = $password;
            
            $input=array();
            //$password="aaaa";
            $pass = $password . $user->salt;
            $input['password'] = hash('SHA512', $pass);
            $affectedRows = User::where('_id','=',$user->id)->update($input);
            
            $body=view("emails.password_email",$replaces);
            $mail = Functions::sendEmail($user->email, "Your new password.", $body);
            \Session::flash('success', 'Your new password has been emailed.');
        } else {
            \Session::flash('danger', 'Email not found.');
        }

        return redirect('forgot-password');
    }
}

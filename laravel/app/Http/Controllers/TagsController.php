<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Validator,
    Input,
    Redirect;
use App\Tags;
use MongoDB;
use DB;
use App\Functions\Functions;

class TagsController extends Controller {

    private $session;

    public function __construct() {
        $this->session = Session::all();

        if (!isset($this->session['schoolId'])) {
            header("Location:" . url("/") . "");
            die;
        }
    }

    public function index() {

        $schoolId = $this->session['schoolId'];
        $model = Tags::where('schoolId', $schoolId)->whereRaw(['is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['tagName' => 1]])->paginate(10);
        $data['model'] = $model;
        return view('front.tags.index', $data);
    }

    public function create() {

        $schoolId = $this->session['schoolId'];
        $model = new Tags();
        $data['model'] = $model;
        return view('front.tags.create', $data);
    }

    public function save(Request $request) {
        
        $input = $request->all();
        
        $validation = array('tagName' => 'required');

        $validator = Validator::make($input, $validation);
        $response['error'] = 0;

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            
            $schoolId = $this->session['schoolId'];
            unset($input['_token']);
            $id = new MongoDB\BSON\ObjectID;
            //$input['_id'] = (string) "asd8855dasdda".rand(0,999);
            $input['schoolId'] = $schoolId;
            
            if (isset($input['internalUse'])) {
                $input['internalUse'] = 1;
            } else {
                $input['internalUse'] = 0;
            }
            
            $input['cloudId'] = Functions::getCloudId();
            $input['is_deleted']="0";
            $input['acheived']="0";
            $acl['role:' . $schoolId . '_AppUsers']['r'] = true;
            $acl['role:' . $schoolId . '_AppUsers']['w'] = true;
            $input['_acl'] = $acl;
            $input['_rperm'][0] = 'role:' . $schoolId . '_AppUsers';
            $input['_wperm'][0] = 'role:' . $schoolId . '_AppUsers';
            $model = Tags::create($input);
        }
        \Session::flash('success', 'New Tag Created.');
        return redirect("tags");
    }
    
    public function edit($id) {

        $schoolId = $this->session['schoolId'];
        $model = Tags::whereRaw(['_id'=>$id])->first();
        $data['model'] = $model;
        $data['id'] = $id;
        return view('front.tags.edit', $data);
    }
    
    
    public function update(Request $request,$id) {
        
        $input = $request->all();
        
        $validation = array(
            'tagName' => 'required',
        );
        $validator = Validator::make($input, $validation);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            
            $schoolId = $this->session['schoolId'];
            unset($input['_token']);
            $input['schoolId'] = $schoolId;
            
            if (isset($input['internalUse'])) {
                $input['internalUse'] = 1;
            } else {
                $input['internalUse'] = 0;
            }
            
            // $model = Tags::create($input);
            Tags::whereRaw(['_id'=>$id])->update($input);
        }
        \Session::flash('success', 'Tag updated.');
        return redirect("tags");
    }

    public function listing(Request $request) {

        $input = $request->all();

        $schoolId = $this->session['schoolId'];
        $model = Learners::where('schoolId', $schoolId)->whereRaw(['is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['firstName' => 1, 'lastName' => 1]]);

        if (!empty($input['learners'])) {
            $model = $model->whereRaw(['_id' => ['$in' => $input['learners']]]);
        }

        if (!empty($input['groups'])) {
            $learnerGroups = LearnersGroups::whereRaw(['owningId' => ['$in' => $input['groups']]])->get()->toArray();
            $inputGroups = array();

            foreach ($learnerGroups as $inputGroup) {
                $inputGroups[] = $inputGroup['relatedId'];
            }

            $model = $model->whereRaw(['_id' => ['$in' => $inputGroups]]);
        }

        if (!empty($input['cohorts'])) {

            foreach ($input['cohorts'] as $cohort) {
                $model = $model->where($cohort, '1');
            }
        }

        $model = $model->paginate(10);

        $_p_learners = array();
        foreach ($model as $learner) {
            $_p_learners[] = "Students$" . $learner['_id'];
        }
        //echo json_encode($_p_learners);
        $attributes = $this->session['attributes'];
        $data['attributes'] = $attributes;
        $data['model'] = $model;
        
        $data['_p_learners'] = $_p_learners;

        return view('front.learners.ajax.listing', $data);
    }

    public static function showDate($daterange) {
        $string = explode('-', $daterange);
        $date1 = explode('/', trim($string[0]));
        $date2 = explode('/', trim($string[1]));

        $date['start'] = $date1[2] . '-' . $date1[0] . '-' . $date1[1];
        $date['end'] = $date2[2] . '-' . $date2[0] . '-' . $date2[1];
        return $date;
    }

    public function evidences(Request $request) {
        $input = $request->all();
        $schoolId = $this->session['schoolId'];
        $_p_learners = json_decode($input['_p_learners']);
        $model = Evidence::where('schoolId', $schoolId)->whereIn('_p_learner', $_p_learners);

        if (!isset($input['status']) || $input['status'] == "all") {
            
        } elseif ($input['status'] == 0) {
            $model = $model->whereRaw(['is_deleted' => ['$exists' => false]]);
        } elseif ($input['status'] == 1) {
            $model = $model->where("status", "1");
        }

        if (isset($input["daterange"])) {
            $date = self::showDate($input["daterange"]);
            $model = $model->where('_created_at', '>=', new \DateTime($date['start']));
            $model = $model->where('_created_at', '<=', new \DateTime($date['end']));
        }

        if (isset($input['parentCapture']) && $input['parentCapture'] == "1") {
            $model = $model->where("parentCapture", "1");
        }

        if (isset($input['parentView']) && $input['parentView'] == "1") {
            $model = $model->where("parentView", "1");
        }

        $model = $model->get();

        $evidences = array();
        $learnerCloudIds = array();

        foreach ($model as $evidence) {
            $evidences[$evidence->_id] = $evidence;
            $learnerCloudIds[] = $evidence->learnerCloudId;
        }

        $modelLearner = Learners::whereIn('cloudId', $learnerCloudIds)->get()->toArray();
        $learners = array();

        foreach ($modelLearner as $learner) {
            $learners[$learner['cloudId']] = $learner['firstName'] . " " . $learner['lastName'];
        }

        $data['learners'] = $learners;
        $data['evidences'] = $evidences;
        $data['model'] = $model;
        return view('front.learners.ajax.evidences', $data);
    }

    public function filter() {

        $schoolId = $this->session['schoolId'];
        $learners = Learners::whereRaw(['schoolId' => $schoolId, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['firstName' => 1, 'lastName' => 1]])->get();
        $groups = Group::whereRaw(['schoolId' => $schoolId, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['groupName' => 1]])->get();
        $attributes = $this->session['attributes'];
        $data['learners'] = $learners;
        $data['groups'] = $groups;
        $data['attributes'] = $attributes;
        return view('front.learners.ajax.filter', $data);
    }

    public function loadfilters(Request $request) {
        $schoolId = $this->session['schoolId'];
        $user_id = $this->session['_id'];
        $model = UserFilters::where("user_id", $user_id)->where("schoolId", $schoolId)->get()->toArray();
        $data['model'] = $model;
        return view('front.learners.ajax.loadfilter', $data);
    }

    public function savefilter(Request $request) {
        $input = $request->all();
        $validation = array(
            'name' => 'required',
            'filters' => 'required',
        );

        $messages = array(
            'name.required' => 'Filter name is required',
            'filters.required' => 'Choose atleast Groups, Learners or cohorts.',
        );

        $validator = Validator::make($input, $validation, $messages);
        $response['error'] = 0;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $schoolId = $this->session['schoolId'];
            $user_id = $this->session['_id'];
            parse_str($input['filters'], $filters);
            $model = UserFilters::create(['name' => $input['name'], "user_id" => $user_id, "schoolId" => $schoolId, 'filters' => $filters]);
        }
        return json_encode($response);
    }

    public function view($id) {
        $model = Learners::whereRaw(['_id' => $id, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['firstName' => 1, 'lastName' => 1]])->first();


        $attributes = $this->session['attributes'];
        $data['attributes'] = $attributes;
        $evidences = Evidence::whereRaw(['learnerCloudId' => $model->cloudId, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['firstName' => 1, 'lastName' => 1]])->get();

        $documents = Documents::whereRaw(['studentCloudId' => $model->cloudId, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['dateCreated' => 1, 'docTitle' => 1]])->get();

        // d($documents,1);

        $data['model'] = $model;
        $data['evidences'] = $evidences;
        $data['documents'] = $documents;
        return view('front.learners.view', $data);
    }

}

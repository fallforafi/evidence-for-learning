<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,
    Input,
    Redirect;
use App\User;
use App\SubscriptionQuota;
use App\SubscriptionLicence;
use Session;
use Illuminate\Http\Request;
use App\Functions\Functions;

class LoginController extends Controller {

    //protected $loginPath = 'signup';

    public function __construct() {
        //session_start();
        //$this->registrar = $registrar;
        //$this->middleware('guest', ['except' => 'getLogout']);
    }

    public function school() {
        // echo hash('ripemd160', 'N1BY3BnKzbOuTxA4c5CXTFT216Jw9u6C4+afJs2CAdilPiKNuyUXkxOQqTUZVKJGeBxHqSIdkpsYIfqM+6Aqwg==');
        return view('front.login.school');
    }

    public function forcelogin($id) {

        $users = SubscriptionQuota::where('schoolId', $id)->first();

        $result = self::login($users->dashboardLogin, $users->cloudAdminPassword);

        if ($result == 1) {
            return redirect('dashboard');
        } else {
            $validator = Validator::make(array(), array());
            $validator->errors()->add('error', $result['message']);
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }

    public static function login($dashboardLogin, $cloudAdminPassword) {
        session_start();
        $users = SubscriptionQuota::where('dashboardLogin', $dashboardLogin)->where('cloudAdminPassword', $cloudAdminPassword)->where('status', '1')->first();

        $attributes = array();
        if (isset($users->exists) && $users->exists == 1) {
            $subscriptionLicence = SubscriptionLicence::where('schoolId', $users->schoolId)->first();
            $endDate = $subscriptionLicence->endDate->toDateTime();
            $endDate = $endDate->format("Y-m-d H:i:s");
            $today = date("Y-m-d H:i:s");



            if (strtotime($endDate) <= strtotime($today)) {

                $result['message'] = "subscription time ends.";
                $result['error'] = true;
                return $result;
                //return redirect()->back()->withErrors($validator->errors())->withInput();
                die;
            }



            Session::put('_id', $users->_id);
            Session::put('name', $users->subscriptionName);
            Session::put('email', $users->email);
            Session::put('username', $users->dashboardLogin);
            Session::put('schoolId', $users->schoolId);
            Session::put('status', $users->status);
            Session::put('colour', $users->colour);
            Session::put('schoolLogin', 1);
            Session::put('loginType', "school");
            Session::put('quota', $users->quota);

            $getData = Functions::getGeoData();
            $getData['geoplugin_countryCode'] = 'UK';
            
            
            if ($getData['geoplugin_countryCode'] == 'UK') {
                $dateFormat = "d/m/Y";
                $datepickerFormat = "dd/mm/YY";
            } else {
                $dateFormat = "m/d/Y";
                $datepickerFormat = "mm/dd/YY";
            }
            
            
            $datetime = $users->_created_at->toDateTime();
            $mydate = $datetime->format($dateFormat);
            Session::put('subscriptionDate', $mydate);
            
            Session::put('countryCode', $getData['geoplugin_countryCode']);
            Session::put('subscriptionLicenceId', $subscriptionLicence->_id);
            Session::put('domain', $subscriptionLicence->domain);
            Session::put('plan', $subscriptionLicence->plan);
            Session::put('users', $subscriptionLicence->users);
            Session::put('dateFormat', $dateFormat);
            Session::put('datepickerFormat', $datepickerFormat);
            
            $datetime = $subscriptionLicence->endDate->toDateTime();
            $mydate = $datetime->format($dateFormat);
            Session::put('endDate', $mydate);




            $school = $users->toArray();

            for ($i = 1; $i <= 100; $i++) {
                $attributeName = "attribute" . $i . "Name";
                if (key_exists($attributeName, $school)) {
                    $attributes[$i] = $school[$attributeName];
                } else {
                    break;
                }
            }

            Session::put('attributes', $attributes);
            return 1;
            //return redirect('dashboard');
        } else {
            $result['message'] = "Invalid login.";
            $result['error'] = true;
            return $result;
        }
        return 0;
    }

    public function schoollogin(Request $request) {

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);


        $result = self::login($request->email, $request->password);

        if ($result == 1) {
            return redirect('dashboard');
        } else {
            $validator = Validator::make(array(), array());
            $validator->errors()->add('error', $result['message']);
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        return redirect('/')
                        ->withInput($request->only('email', 'remember'))
                        ->withErrors([
                            'email' => 'Invalid login.',
        ]);
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\SubscriptionQuota;
use Session;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Validator,
    Input,
    Redirect;
use App\SubscriptionLicence;
use App\Log;

class UsersController extends Controller {

    public function __construct() {

        $this->session = Session::all();

        if (!isset($this->session['schoolId'])) {
            header("Location:" . url("/") . "");
            die;
        }
    }

    public function index() {

        $schoolId = $this->session['schoolId'];
        $school = SubscriptionQuota::where('schoolId', $schoolId)->first();

        $model = User::where('schoolId', $schoolId)->where('is_deleted', "0")->paginate(20);
        $data['model'] = $model;
        return view('front.users.index', $data);
    }

    public function export() {

        $schoolId = $this->session['schoolId'];
        $users = User::where('schoolId', '=', $schoolId)->get();

        $header[] = "firstName";
        $header[] = "lastName";
        $header[] = "username";
        $header[] = "email";
        $header[] = "password";
        $header[] = "salt";
        $header[] = "externalId";

        //$header = implode(',', $header) . '\t';


        $users = User::where('schoolId', '=', $schoolId)->get();
        $fp = fopen('php://output', 'w');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="users' . $schoolId . '.csv"');
        header('Pragma: no-cache');
        header('Expires: 0');
        fputcsv($fp, $header);

        foreach ($users as $user) {

            $data['firstName'] = $user->firstName;
            $data['lastName'] = $user->lastName;
            $data['username'] = $user->username;
            $data['email'] = $user->email;
            $data['password'] = $user->password;
            $data['salt'] = $user->salt;
            $data['externalId'] = $user->externalId;
            fputcsv($fp, $data);
        }
        die;
        //  return view('front.users.import', $data);
    }

    public function import() {

        $schoolId = $this->session['schoolId'];
        $data = array();
        return view('front.users.import', $data);
    }

    public function importcsv(Request $request) {

        $schoolId = $this->session['schoolId'];
        $validation = array('file' => 'required',);

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $file = $request->all();

        $extension = $file['file']->getClientOriginalExtension();
        $realPath = $file['file']->getRealPath();


        $lines = array();
        $fh = fopen($realPath, 'r+');

        $file = file($realPath);
        $count = count($file) - 1;

        $subscriptionLicence = SubscriptionLicence::where('schoolId', $schoolId)->first();
        $countUsers = User::where('schoolId', $schoolId)->count();
        //echo $subscriptionLicence->users;
        //echo " ";
        $total = $count + $countUsers;
        //die;
        if ($total > $subscriptionLicence->users) {

            $validator->errors()->add('error', 'subscription limit exceeds.');
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }


        $i = 0;
        $emailDuplicate = 0;
        $usernameDuplicate = 0;
        $externalIdDuplicate = 0;
        $duplicate = 0;

        $totalInsert = 0;
        while (($row = fgetcsv($fh, 8192)) !== FALSE) {


            if ($i == 0) {
                $i++;
                continue;
            }

            //echo $row;
            $input = array();

            $input['firstName'] = $row[0];
            $input['lastName'] = $row[1];
            $input['email'] = $row[2];
            $input['password'] = $row[3];
            $input['username'] = $row[4];
            $input['externalId'] = $row[5];
            $input['manager'] = $row[6];
            $error = 0;
            $success = true;
            $message = "";
            $data = $input;
            $user = User::where('email', '=', $input['email'])->first();

            if (count($user) > 0) {

                $data['message'] = 'Email already exist.';
                $data['success'] = false;
                $emailDuplicate++;
                $duplicate++;
                $error = 1;
            }

            $user = User::where('username', '=', $input['username'])->first();

            if (count($user) > 0) {

                $data['message'] = 'Username already exist.';
                $data['success'] = false;
                $usernameDuplicate++;
                $duplicate++;
                $error = 1;
            }

            $user = User::where('externalId', '=', $input['externalId'])->first();

            if (count($user) > 0) {

                $data['message'] = 'External Id already exist.';
                $data['success'] = false;
                $externalIdDuplicate++;
                $duplicate++;
                $error = 1;
            }


            if ($error == 0) {
                $schoolId = $this->session['schoolId'];
                $totalInsert++;
                $input['userTitle'] = "";
                $input['role'] = "";
                //$input['externalId'] = "";
                $id = Functions::generateRandomString(20);
                $input['_id'] = (string) $id;
                $input['schoolId'] = $schoolId;
                $input['cloudId'] = Functions::getCloudId();
                $acl['role:' . $schoolId . '_AppUsers']['r'] = true;
                $acl['role:' . $schoolId . '_AppUsers']['w'] = true;
                $input['_acl'] = $acl;
                $input['_rperm'][0] = 'role:' . $schoolId . '_AppUsers';
                $input['_wperm'][0] = 'role:' . $schoolId . '_AppUsers';
                $input['_created_at'] = date('Y-m-d H:i:s');
                $input['_updated_at'] = date('Y-m-d H:i:s');
                $salt = "salt" . time();
                $input['salt'] = $salt;
                $pass = $input['password'] . $salt;
                $input['password'] = hash('SHA512', $pass);
            }

            $response[$i] = $data;

            $response[$i]['success'] = $success;
            $response[$i]['message'] = $message;
            $model = User::create($input);
            $i++;
        }
        $response['externalIdDuplicate'] = $externalIdDuplicate;
        $response['usernameDuplicate'] = $usernameDuplicate;
        $response['emailDuplicate'] = $emailDuplicate;
        $response['totalInsert'] = $totalInsert;
        $response['duplicate'] = $duplicate;



        \Session::flash('success', 'import success.');
        return redirect()->back();
        //return view('front.users.import', $data);
    }

    public function changepassword() {

        $id = $this->session['_id'];
        $model = User::find($id);
        $data['model'] = $model;
        return view('front.users.change_password', $data);
    }

    public function postchangepassword(Request $request) {

        $validation = array(
            'password' => 'required|confirmed|min:5|max:100',
        );

        $input = $request->all();
        $id = $this->session['_id'];
        $model = User::find($id);
        $oldPassword = $input['old_password'] . $model->salt;
        $oldPassword = hash('SHA512', $oldPassword);
        $validator = Validator::make($input, $validation);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }



        if ($model->password != $oldPassword) {
            $error = 1;
            $validator->getMessageBag()->add('error', 'Incorrect wrong password.');
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            unset($input['_token']);
            unset($input['password_confirmation']);
            unset($input['old_password']);
            unset($input['submit']);

            $pass = $input['password'] . $model->salt;
            $input['password'] = hash('SHA512', $pass);
            User::whereRaw(['_id' => $id])->update($input);
        }

        \Session::flash('success', 'password changed.');
        return redirect()->back();
    }

    public function create() {

        $schoolId = $this->session['schoolId'];



        $subscriptionLicence = SubscriptionLicence::where('schoolId', $schoolId)->first();
        $users = User::where('schoolId', $schoolId)->count();
        $school = SubscriptionQuota::where('schoolId', $schoolId)->first();
        $data['school'] = $school;
        $model = new User();
        $data['model'] = $model;
        $data['message'] = '';
        if ($subscriptionLicence->users == "-1") {
            $view = "create";
        } elseif ($users >= $subscriptionLicence->users) {
            $data['message'] = 'You have reached you users limit.';
            $view = "limit";
        } else {
            $view = "create";
        }

        return view('front.users.' . $view, $data);
    }

    public function edit($id) {

        $schoolId = $this->session['schoolId'];
        $model = User::find($id);
        $data['model'] = $model;
        $data['id'] = $id;
        return view('front.users.edit', $data);
    }

    public function save(Request $request) {

        $input = $request->all();
        // d($input, 1);
        $validation = array(
            'firstName' => 'required',
            'lastName' => 'required',
            'username' => 'required|max:50|unique:TTC_Users',
            'email' => 'required|email|max:50|unique:TTC_Users',
                // 'password' => 'required|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/|confirmed|min:6',
                //'externalId' => 'unique:TTC_Users|min:5|max:50',
        );
        //'password' => 'required|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/|confirmed|min:6',
        //'email' => 'required|min:6|email|unique:users,email,' . $user->id,
        $validator = Validator::make($input, $validation);
        $response['error'] = 0;
        $schoolId = $this->session['schoolId'];

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            unset($input['_token']);
            //unset($input['password_confirmation']);
            $input['schoolId'] = $schoolId;

            if (isset($input['manager'])) {
                $input['manager'] = 1;
            } else {
                $input['manager'] = 0;
            }
            // if (isset($input['status'])) {
            $input['status'] = "1";
            $input['confirmed'] = 0;
            $input['is_deleted'] = "0";
            //  } 
//            else {
//                $input['status'] = 0;
//            }

            $input['userTitle'] = "";
            $input['role'] = "";
            //$input['externalId'] = "";
            $id = Functions::generateRandomString(20);
            $input['_id'] = (string) $id;

            $input['cloudId'] = Functions::getCloudId();
            $acl['role:' . $schoolId . '_AppUsers']['r'] = true;
            $acl['role:' . $schoolId . '_AppUsers']['w'] = true;
            $input['_acl'] = $acl;
            $input['_rperm'][0] = 'role:' . $schoolId . '_AppUsers';
            $input['_wperm'][0] = 'role:' . $schoolId . '_AppUsers';

            $input['_created_at'] = date('Y-m-d H:i:s');
            $input['_updated_at'] = date('Y-m-d H:i:s');

//            $salt = "salt" . time();
//            $input['salt'] = $salt;
//            $pass = $input['password'] . $salt;
//            $input['password'] = hash('SHA512', $pass);
            $model = User::create($input);

            $input['eventType'] = "users.add";
            $input['eventDetail'] = "user" . ":" . $input['cloudId'] . " was added";
            Log::createLog($input);

            $confirmation_code = $input['_id'];
            $subject = view('emails.confirm_email.subject');
            $body = view('emails.confirm_email.body', compact('confirmation_code'));
            Functions::sendEmail($request->email, $subject, $body);

            \Session::flash('success', 'Successfully Added');
            return redirect("users");
        }
    }

    public function update(Request $request, $id) {

        $input = $request->all();
        $model = User::where('_id', $id)->first();
        // d($input, 1);
        $error = 0;
        $validation = array(
            'firstName' => 'required',
            'lastName' => 'required',
            'username' => 'required|max:50',
            'email' => 'required|email|max:50',
                // 'externalId' => 'required|max:50'
        );
        //'email' => 'required|min:6|email|unique:users,email,' . $user->id,
//        if (!empty(trim($input['password']))) {
//            $validation['password'] = 'required|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/|confirmed|min:6';
//        } else {
//            unset($input['password']);
//        }
        $validator = Validator::make($input, $validation);

        $user = User::where('_id', '<>', $id)->where('email', '=', $request->email)->get();

        if (count($user) > 0) {
            $error = 1;
            $validator->getMessageBag()->add('email', 'Email already exist.');
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $user = User::where('_id', '<>', $id)->where('username', '=', $request->username)->get();

        if (count($user) > 0) {
            $error = 1;
            $validator->getMessageBag()->add('email', 'Username already exist.');
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }


//        $user = User::where('_id', '<>', $id)->where('externalId', '=', $request->externalId)->get();
//
//        if (count($user) > 0) {
//            $error = 1;
//            $validator->getMessageBag()->add('email', 'External Id already exist.');
//            return redirect()->back()->withErrors($validator->errors())->withInput();
//        }


        if ($validator->fails() || $error == 1) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {


            unset($input['_token']);
            // unset($input['password_confirmation']);

            if (isset($input['manager'])) {
                $input['manager'] = 1;
            } else {
                $input['manager'] = 0;
            }
            if (isset($input['status'])) {
                $input['status'] = "1";
            } else {
                $input['status'] = "0";
            }

//            if (isset($input['password'])) {
//                $pass = $input['password'] . $model->salt;
//                $input['password'] = hash('SHA512', $pass);
//            }
            $input['_updated_at'] = date('Y-m-d H:i:s');
            // $model = Tags::create($input);
            User::whereRaw(['_id' => $id])->update($input);
        }

        \Session::flash('success', 'User updated.');
        // return redirect("users");
        return redirect()->back();
    }

    public function profile() {

        $id = $this->session['_id'];
        $model = User::find($id);

        $data['model'] = $model;
        $data['id'] = $id;
        return view('front.users.profile', $data);
    }

    public function updateprofile(Request $request) {

        $id = $this->session['_id'];

        $validation = array(
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required|email|max:50',
        );

        $validator = Validator::make($request->all(), $validation);

        $user = User::where('_id', '<>', $id)->where('email', '=', $request->email)->get();

        if (count($user) > 0) {
            $error = 1;
            $validator->getMessageBag()->add('email', 'Email already exist.');
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $input = $request->all();
        unset($input['_token']);
        \Session::flash('success', 'profile updated.');
        User::whereRaw(['_id' => $id])->update($input);
        return redirect()->back();
    }

    public function delete($id) {

        $user = User::find($id);
        if ($user->status == "0") {
//            $user->delete();
            $input['is_deleted'] = "1";
            User::whereRaw(['_id' => $id])->update($input);
            \Session::flash('success', 'Deleted Successfully');
        } else {
            \Session::flash('danger', "Active user can't be deleted");
        }

        return redirect("users");
    }

    public function logout() {
        session_start();
        $user = Session::all();
        Session::flush();
        session_destroy();
        if (isset($user['schoolLogin']) && $user['schoolLogin'] == 1) {
            return redirect('school/login');
            die;
        }
        return redirect('/');
    }

    public function resetUserPassword($id) {

        //$user = User::where("_id", $id)->where("confirmed", 1)->where("is_deleted", 0)->where("status", 1)->first();
        $user = User::where("_id", $id)->first();
        if (count($user) == 1) {
            //$password = substr(md5(microtime()), rand(0, 26), 7);
//            $replaces['name'] = $user->firstName . ' ' . $user->lastName;
//            $replaces['password'] = $password;
            $password = Functions::generateRandomString(8);
            $pass = $password . $user->salt;
            $input['password'] = hash('SHA512', $pass);

//            $salt = "salt" . time();
//            $input['salt'] = $salt;
//            $pass = $input['password'] . $salt;
//            $input['password'] = hash('SHA512', $pass);
            User::where('_id', '=', $user->id)->update($input);

            $subject = view('emails.reset_password_email.subject');
            $body = view('emails.reset_password_email.body', compact('password'));
            Functions::sendEmail($user->email, $subject, $body);

            \Session::flash('success', 'Password reset & sent successfully');
            return redirect()->back();
        } else {
            \Session::flash('danger', 'Invalid User');
            return redirect()->back();
        }
    }

}

<?php

namespace App\Http\Controllers;

use App\Frameworks;
use App\FrameworkItems;
use Session;
use Illuminate\Http\Request;
use Validator,
    Input,
    Redirect;

class FrameworksController extends Controller {


    public function __construct() {

    }

    public function get($id) {
        //echo $id;
        $_p_frameworkPointer="Frameworks$".$id;
        $frameworkItemsModels = FrameworkItems::where('_p_frameworkPointer' , $_p_frameworkPointer)->where('canSelect',1)->get();
        
        $data=array();
        foreach($frameworkItemsModels as $framework){
         $data[$framework->path]=$framework->itemText;   
        }
        d($data);
    }
}

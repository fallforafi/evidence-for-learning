<?php

namespace App\Http\Controllers;

use App\Evidence;
use App\Learners;
use App\Group;
use App\Frameworks;
use App\Tags;
use App\FrameworkItems;
use Session;
use Illuminate\Http\Request;
use Validator;
use App\Aws\S3;
use App\Functions\Functions;
use App\Log;
use App\CommentTemplates;
use App\DateRanges;

class EvidenceController extends Controller {

    private $session;

    public function __construct() {
        $this->session = Session::all();
        if (!isset($this->session['schoolId'])) {
            header("Location:" . url("/") . "");
            die;
        }
    }

    public function getFrameworks() {

        $schoolId = $this->session['schoolId'];
        $model = Frameworks::whereRaw(['schoolId' => $schoolId, 'is_deleted' => ['$ne' => '1']], ['sort' => ['_created_at' => 1]])->get();
        $frameworks = array();
        foreach ($model as $val) {
            $frameworks[$val->_id]['title'] = $val->frameworkName;
        }


        foreach ($frameworks as $key => $framework) {
            $model = FrameworkItems::whereRaw(['_p_frameworkPointer' => "Frameworks$" . $key, 'is_deleted' => ['$ne' => '1']], ['sort' => ['_created_at' => 1]])->get()->toArray();

            $f = 0;
            $i = 0;
            $body = "";
            foreach ($model as $row) {

                if (trim($row['itemText']) == "") {
                    continue;
                }
                if ($f == 0) {
                    $level = $row['indentLevel'];
                    $f++;
                }
                $diff = 0;
                if ($level != $row['indentLevel']) {

                    $diff = $row['indentLevel'] - $level;
                    $diff = abs($diff) * 2;
                    $class = "";
                    if (isset($row['plg']) && $row['plg'] == 1) {
                        $class = "plg";
                    }

                    if ($row['canSelect'] == 0) {

                        $body .= '<li class="' . $level . '"  data-dd=' . $diff . ' style="margin-left:' . $diff . '0px;,width:100%;"><b>' . $row['itemText'] . "</b></li>";
                    } else {
                        $body .= '<li class="' . $level . '" data-dd=' . $diff . ' style="margin-left:' . $diff . '0px;,width:100%;"><input data-path="' . $row['frameworkName'] . ' > ' . $row['path'] . ' > ' . $row['itemText'] . '" class="sub_cat_1 ' . $class . '" name="frameworksItems[]" type="checkbox" id="' . $row['_id'] . '" value="' . $row['_id'] . '">' . $row['itemText'] . "</li>";
                    }
                } else {
                    $diff = $row['indentLevel'] - $level;
                    $diff = abs($diff) * 2;

                    if ($row['canSelect'] == 0) {
                        $body .= '<li class="' . $level . '" data-dd=' . $diff . ' style="width:100%;"><b>' . $row['itemText'] . "</b></li>";
                    } else {

                        $body .= '<li class="' . $level . '" data-dd="' . $diff . '" style="margin-left:60px;,width:100%;"><input data-path="' . $row['frameworkName'] . ' > ' . $row['path'] . ' > ' . $row['itemText'] . '" class="sub_cat_1 ' . $class . '" name="frameworksItems[]" type="checkbox" value="' . $row['_id'] . '">' . $row['itemText'] . "</li>";
                    }
                }
            }
            $frameworks[$key]['body'] = $body;
        }
        return $frameworks;
    }

    public function create($learner_id) {

        $schoolId = $this->session['schoolId'];
        $learner = Learners::whereRaw(['_id' => $learner_id, 'schoolId' => $schoolId, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['firstName' => 1, 'lastName' => 1]])->first();

        $tags = Tags::whereRaw(['schoolId' => $schoolId, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['tagName' => 1]])->get();

        $model = new Evidence();

        $frameworks = $this->getFrameworks();
        $data['frameworks'] = $frameworks;


        $selectedFrameworkItems = array();
        $selectedTags = array();

        $selectedFrameworkItems = array();
        $selectedTags = array();

        if (!empty($model->frameworkItemsPointerArray)) {
            foreach ($model->frameworkItemsPointerArray as $fpa) {
                $selectedFrameworkItems[] = $fpa['objectId'];
            }
        }

        if (!empty($model->tagsPointerArray)) {
            foreach ($model->tagsPointerArray as $tpa) {
                $selectedTags[] = $tpa['objectId'];
            }
        }
        $frameworkItemsIds = array();
        if (!empty($model->frameworkItemsPointerArray)) {
            foreach ($model->frameworkItemsPointerArray as $frameworkItemsPointerArray) {
                $frameworkItemsIds[] = $frameworkItemsPointerArray['objectId'];
            }
        }

        $frameworkItemsModels = FrameworkItems::whereRaw(['_id' => ['$in' => $frameworkItemsIds], 'is_deleted' => ['$ne' => '1']], ['sort' => ['sortOrder' => 1]])->get();

        $data['frameworkItems'] = $frameworkItemsModels;


        $data['selectedFrameworkItems'] = $selectedFrameworkItems;
        $data['selectedTags'] = $selectedTags;
        $data['learner'] = $learner;
        $data['tags'] = $tags;
        $data['selectedTags'] = array();
        $data['model'] = $model;
        $data['commentTemplates'] = CommentTemplates::whereRaw(['schoolId' => $schoolId, 'is_deleted' => ['$ne' => '1']])->get();


        return view('front.evidences.create', $data);
    }

    public static function evidenceFile($file, $i) {
        $fileName = Functions::generateRandomString(15) . '-' . rand(111, 999) . time();
        return $result = S3::storeImage($fileName, $file, 1, 1, 120, 120, $i);
    }

    public function edit($id) {

        $schoolId = $this->session['schoolId'];
        $model = Evidence::whereRaw(['_id' => $id])->first();

        $datetime = $model->evidenceDate->toDateTime();
        $learners = array();
        $dateFormat = Session::get("dateFormat");
        $model->evidenceDate = $datetime->format($dateFormat);
        $tags = Tags::whereRaw(['schoolId' => $schoolId, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['tagName' => 1]])->get();
        // echo $schoolId;
        //echo $schoolId;
        //$learners = Learners::where("schoolId",$schoolId)->get();
        //d($learners,1);
        $learner = Learners::whereRaw(['cloudId' => $model->learnerCloudId])->first();
        $frameworks = $this->getFrameworks();
        $selectedFrameworkItems = array();
        $selectedTags = array();

        if (!empty($model->frameworkItemsPointerArray)) {
            foreach ($model->frameworkItemsPointerArray as $fpa) {
                $selectedFrameworkItems[] = $fpa['objectId'];
            }
        }

        if (!empty($model->tagsPointerArray)) {
            foreach ($model->tagsPointerArray as $tpa) {
                $selectedTags[] = $tpa['objectId'];
            }
        }
        $frameworkItemsIds = array();
        if (!empty($model->frameworkItemsPointerArray)) {
            foreach ($model->frameworkItemsPointerArray as $frameworkItemsPointerArray) {
                $frameworkItemsIds[] = $frameworkItemsPointerArray['objectId'];
            }
        }

        $frameworkItemsModels = FrameworkItems::whereRaw(['_id' => ['$in' => $frameworkItemsIds], 'is_deleted' => ['$ne' => '1']], ['sort' => ['sortOrder' => 1]])->get();

        $data['frameworkItems'] = $frameworkItemsModels;
        $data['learners'] = $learners;
        $data['learner'] = $learner;
        $data['frameworks'] = $frameworks;
        $data['model'] = $model;
        $data['id'] = $id;
        $data['selectedFrameworkItems'] = $selectedFrameworkItems;
        $data['tags'] = $tags;
        $data['selectedTags'] = $selectedTags;
        $data['commentTemplates'] = CommentTemplates::whereRaw(['schoolId' => $schoolId, 'is_deleted' => ['$ne' => '1']])->get();

        return view('front.evidences.edit', $data);
    }

    public function update(Request $request, $id) {
        $countryCode = Session::get("countryCode");
        $schoolId = $this->session['schoolId'];
        $input = $request->all();
        $validation = array(
            'evidenceDate' => 'required',
            'file1' => 'mimes:jpeg,jpg,mp4,mov',
            'file2' => 'mimes:jpeg,jpg,mp4,mov',
            'file3' => 'mimes:jpeg,jpg,mp4,mov',
        );

        $validator = Validator::make($input, $validation);
        $response['error'] = 0;

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $images = array();
            for ($i = 1; $i <= 3; $i++) {

                if (isset($input['file' . $i]) && !empty($input['file' . $i])) {
                    $result = self::evidenceFile($input['file' . $i], $i);
                    $images = array_merge($images, $result);

                    unset($input['file' . $i]);
                }
            }

            $input['frameworkItemsPointerArray'] = array();
            $input['frameworkItems'] = array();

            if (!empty($input['frameworksItems'])) {
                $result = self::getInputFrameworkItems($input['frameworksItems']);
                $input['frameworkItemsPointerArray'] = $result['frameworkItemsPointerArray'];
                $input['frameworkItems'] = $result['frameworkItems'];
            }

            $input['tagsPointerArray'] = array();
            $input['tagCloudIds'] = array();
            if (!empty($input['tags'])) {
                $tags = self::getInputTags($input['tags']);
                $input['tagsPointerArray'] = $tags['tagsPointerArray'];
                $input['tagCloudIds'] = $tags['tagCloudIds'];
            }

            unset($input['_token']);
            unset($input['frameworksItems']);
            unset($input['tags']);

            if ($countryCode == 'UK') {
                list($date, $month, $year) = explode('/', $input['evidenceDate']);
                //$evidenceDate = $evidenceDate[1] . '/' . $evidenceDate[0] . '/' . $evidenceDate[2];
            } else {
                list($month, $date, $year) = explode('/', $input['evidenceDate']);
            }

            $input['evidenceDate'] = $year . '-' . $month . '-' . $date;
            $evidenceDate = new \DateTime($input['evidenceDate']);
            $input['evidenceDate'] = new \MongoDB\BSON\UTCDateTime($evidenceDate->getTimestamp() * 1000);
            $input['schoolId'] = $schoolId;
            $created = new \DateTime();
            $input['_updated_at'] = new \MongoDB\BSON\UTCDateTime($created->getTimestamp() * 1000);


            if (!empty($images)) {
                $input = array_merge($input, $images);
            }

            if (isset($input['status'])) {
                $input['status'] = "1";
            } else {
                $input['status'] = "0";
            }

            Evidence::whereRaw(['_id' => $id])->update($input);
            $getEvidence = Evidence::whereRaw(['_id' => $id])->first();
            $input['eventType'] = "evidences.edit";
            $input['eventDetail'] = "evidence" . ":" . $getEvidence->cloudId . " was edited";
            Log::createLog($input);
            \Session::flash('success', 'Evidence updated.');
            return redirect()->back();
            //return redirect("evidences/updated");
        }
    }

    public function delete($id) {
        Evidence::whereRaw(['_id' => $id])->update(array("is_deleted" => "1"));
        return view('front.evidences.deleted');
    }

    public function getInputFrameworkItems($frameworksItems) {

        $frameworksItemsModel = FrameworkItems::whereRaw(['_id' => ['$in' => $frameworksItems]])->get();
        $frameworkItemsPointerArray = array();
        $frameworkItems = array();
        $i = 0;
        foreach ($frameworksItemsModel as $item) {
            $res = new \stdClass();
            $res->__type = "Pointer";
            $res->className = "FrameworkItems";
            $res->objectId = $item->_id;
            $frameworkItemsPointerArray[$i] = $res;
            $frameworkItems[$i] = $item->cloudId;
            $i++;
        }
        $result['frameworkItemsPointerArray'] = $frameworkItemsPointerArray;
        $result['frameworkItems'] = $frameworkItems;
        return $result;
    }

    public function getInputTags($tags) {
        $tagsModel = Tags::whereIn("_id", $tags)->get();
        $tagsPointerArray = array();
        $tagCloudIds = array();
        $i = 0;

        foreach ($tagsModel as $item) {
            $res = new \stdClass();
            $res->__type = "Pointer";
            $res->className = "Tags";
            $res->objectId = $item->_id;
            $d[$i]['name'] = $item->tagName;
            $d[$i]['_id'] = $item->_id;
            $tagsPointerArray[$i] = $res;
            $tagCloudIds[$i] = $item->cloudId;
            $i++;
        }

        $result['tagsPointerArray'] = $tagsPointerArray;
        $result['tagCloudIds'] = $tagCloudIds;
        return $result;
    }

    public function save(Request $request) {
        $countryCode = Session::get("countryCode");
        $schoolId = $this->session['schoolId'];
        $input = $request->all();
        $validation = array(
            'learners' => 'required',
            'evidenceDate' => 'required',
            'file1' => 'mimes:jpeg,jpg,mp4,mov',
            'file2' => 'mimes:jpeg,jpg,mp4,mov',
            'file3' => 'mimes:jpeg,jpg,mp4,mov',
        );

        $validator = Validator::make($input, $validation);
        $response['error'] = 0;

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $images = array();
            for ($i = 1; $i <= 3; $i++) {

                if (isset($input['file' . $i])) {
                    $result = self::evidenceFile($input['file' . $i], $i);
                    $images = array_merge($images, $result);

                    unset($input['file' . $i]);
                }
            }

            if (!empty($input['frameworksItems'])) {
                $result = self::getInputFrameworkItems($input['frameworksItems']);
                $input['frameworkItemsPointerArray'] = $result['frameworkItemsPointerArray'];
                $input['frameworkItems'] = $result['frameworkItems'];
            }

            if (!empty($input['tags'])) {
                $tags = self::getInputTags($input['tags']);
                $input['tagsPointerArray'] = $tags['tagsPointerArray'];
                $input['tagCloudIds'] = $tags['tagCloudIds'];
            }
            $learner = Learners::whereRaw(['_id' => $input['learner_id']])->first();
            unset($input['learners']);
            unset($input['_token']);
            unset($input['frameworksItems']);
            unset($input['tags']);
            unset($input['learner_id']);

            if ($countryCode == 'UK') {
                list($date, $month, $year) = explode('/', $input['evidenceDate']);
                //$evidenceDate = $evidenceDate[1] . '/' . $evidenceDate[0] . '/' . $evidenceDate[2];
            } else {
                list($month, $date, $year) = explode('/', $input['evidenceDate']);
            }

            $input['evidenceDate'] = $year . '-' . $month . '-' . $date;
            $evidenceDate = new \DateTime($input['evidenceDate']);
            $input['evidenceDate'] = new \MongoDB\BSON\UTCDateTime($evidenceDate->getTimestamp() * 1000);
            $input['schoolId'] = $schoolId;

            if (isset($input['status'])) {
                $input['status'] = "1";
            } else {
                $input['status'] = "0";
            }

            $created = new \DateTime();
            $input['is_deleted'] = "0";

            $id = Functions::generateRandomString(20);
            $input['_id'] = (string) $id;
            $input["_acl"] = $learner->_acl;
            $input["_rperm"] = $learner->_rperm;
            $input["_wperm"] = $learner->_wperm;
            $input["_p_learner"] = "Students$" . $learner->_id;
            $input["learnerCloudId"] = $learner->cloudId;

            $input['cloudId'] = Functions::generateRandomString(8) . '-' . Functions::generateRandomString(4) . '-' . Functions::generateRandomString(4) . '-' . Functions::generateRandomString(4) . '-' . Functions::generateRandomString(4) . '-' . Functions::generateRandomString(12);
            if (!empty($images)) {
                $input = array_merge($input, $images);
            }
            $input['_created_at'] = date('Y-m-d H:i:s');
            $input['_updated_at'] = date('Y-m-d H:i:s');
            $evidence = Evidence::create($input);
            $input['eventType'] = "evidences.add";
            $input['eventDetail'] = "evidence" . ":" . $input['cloudId'] . " was added";
            Log::createLog($input);
            \Session::flash('success', 'Evidence created.');
            return redirect("evidences/saved");
        }
    }

    public function filter() {

        $schoolId = $this->session['schoolId'];

        $tags = Tags::where('schoolId', $schoolId)->get();

        $groups = Group::whereRaw(['schoolId' => $schoolId, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['groupName' => 1]])->get();

        $frameworks = $this->getFrameworks();

        $frameworkItemsIds = array();
        if (!empty($model->frameworkItemsPointerArray)) {
            foreach ($model->frameworkItemsPointerArray as $frameworkItemsPointerArray) {
                $frameworkItemsIds[] = $frameworkItemsPointerArray['objectId'];
            }
        }

        $dateRangersModel = DateRanges::whereRaw(['schoolId' => $schoolId, 'is_deleted' => ['$ne' => '1']], ['sort' => ['_created_at' => 1]])->get();

        $dateRanges = array();
        $dateFormat = Session::get("dateFormat");
        foreach ($dateRangersModel as $range) {

            $datetime = $range->from->toDateTime();
            $from = $datetime->format($dateFormat);

            $datetime = $range->to->toDateTime();
            $to = $datetime->format($dateFormat);
            $dateRanges[] = "'" . $range->rangeName . "':['" . $from . "','" . $to . "']";
        }



        $frameworkItemsModels = FrameworkItems::whereRaw(['_id' => ['$in' => $frameworkItemsIds], 'is_deleted' => ['$ne' => '1']], ['sort' => ['sortOrder' => 1]])->get();

        $data['frameworkItems'] = $frameworkItemsModels;



        $learners = array();
        $data['dateRanges'] = implode(",", $dateRanges);
        $data['frameworks'] = $frameworks;
        $data['learners'] = $learners;
        $data['tags'] = $tags;
        $data['groups'] = $groups;
        $data['frameworks'] = $frameworks;
        return view('front.evidences.ajax.filter', $data);
    }

    public function view($evidenceId) {

        $schoolId = $this->session['schoolId'];
        $evidence = Evidence::whereRaw(['_id' => $evidenceId])->first();
        $learnerCloudId = $evidence->learnerCloudId;
        $model = Learners::where('cloudId', $learnerCloudId)->first();

        $attributes = $this->session['attributes'];
        $data['attributes'] = $attributes;

        $frameworkItemsIds = array();
        if (!empty($evidence->frameworkItemsPointerArray)) {
            foreach ($evidence->frameworkItemsPointerArray as $frameworkItemsPointerArray) {
                $frameworkItemsIds[] = $frameworkItemsPointerArray['objectId'];
            }
        }

        $frameworkItemsModels = FrameworkItems::whereRaw(['_id' => ['$in' => $frameworkItemsIds], 'is_deleted' => ['$ne' => '1']], ['sort' => ['sortOrder' => 1]])->get();


        $tagsPointerArrayIds = array();
        if (!empty($evidence->tagsPointerArray)) {
            foreach ($evidence->tagsPointerArray as $tagsPointerArray) {
                $tagsPointerArrayIds[] = $tagsPointerArray['objectId'];
            }
        }

        $tagsModels = Tags::whereRaw(['_id' => ['$in' => $tagsPointerArrayIds], 'is_deleted' => ['$ne' => '1']])->get();


        $data['tags'] = $tagsModels;
        $data['frameworkItems'] = $frameworkItemsModels;
        $data['evidence'] = $evidence;
        $data['model'] = $model;
        return view('front.evidences.view', $data);
    }

    public function file(Request $request) {
        $id = $request['id'];
        $arr = explode('.', $id);
        $ext = end($arr);
        $data['id'] = $id;
        $data['ext'] = $ext;
        return view('front.evidences.file', $data);
    }

    public function saved() {
        $data = array();
        $data['title'] = "Evidence Saved!";
        $data['message'] = "New evidence for learner has been saved successfully.";
        return view('front.evidences.saved', $data);
    }

    public function updated() {
        $data = array();
        $data['title'] = "Evidence Updated!";
        $data['message'] = "Evidence for learner has been updated successfully.";
        return view('front.evidences.saved', $data);
    }

    public function deleteimage(Request $request) {
        $input = $request->all();
        $id = $input['id'];
        unset($input['id']);
        unset($input['_token']);

        $evidence = Evidence::find($id);
        $input['schoolId'] = $evidence->schoolId;
        $input['eventType'] = "evidences" . $input['side'] . ".delete";
        $input['eventDetail'] = "evidences" . $input['side'] . ":" . $evidence->cloudId . " was deleted";

        Evidence::where('_id', '=', $id)->update(array(
            $input['side'] => "",
            $input['side'] . 'Filename' => "",
            $input['side'] . 'Tn' => "",
            $input['side'] . 'TnFilename' => "",
        ));

//        S3::deleteImage($input['side'], env("S3_BUCKET_URI"), 1);
//        S3::deleteImage($input['side'] . 'Filename', env("S3_BUCKET_URI"), 1);
//        S3::deleteImage($input['side'] . 'Tn', env("S3_BUCKET_URI"), 1);
//        S3::deleteImage($input['side'] . 'TnFilename', env("S3_BUCKET_URI"), 1);

        \Session::flash('success', 'Successfully Deleted');
        return redirect()->back();
    }

}

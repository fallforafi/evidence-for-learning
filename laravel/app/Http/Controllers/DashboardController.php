<?php

namespace App\Http\Controllers;

use App\SubscriptionQuota;
use App\UserFilters;
use App\Learners;
use App\Group;
use App\LearnersGroups;
use Session;
use Illuminate\Http\Request;
use Validator,
    Input,
    Redirect;
use App\Evidence;
use App\Documents;

# use MongoDB\Client as Mongo;

class DashboardController extends Controller {

    private $session;

    public function __construct() {
        $this->session = Session::all();

        if (!isset($this->session['schoolId'])) {
            header("Location:" . url("/") . "");
            die;
        }
    }

    public function index() {


        /*
          $s='md2,md4,md5,sha1,sha224,sha256,sha384,sha512,ripemd128,ripemd160,ripemd256,ripemd320,whirlpool';

          $arr=explode(",",$s);


          foreach($arr as $val){
          $pass = "qwerty";
          $salt="salt1242992451";
          $pass=$salt.$pass.$salt;
          echo hash(''.$val.'',$pass);
          echo "<br/>";
          }


          die;
         */


        $schoolId = $this->session['schoolId'];
        $school = SubscriptionQuota::where('schoolId', $schoolId)->first();
        $data['school'] = $school;

        return view('front.dashboard.index', $data);
    }

    public function listing(Request $request) {

        $input = $request->all();

        $schoolId = $this->session['schoolId'];
        $model = Learners::where('schoolId', $schoolId)->whereRaw(['is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['firstName' => 1, 'lastName' => 1]]);

        if (!empty($input['learners'])) {
            $model = $model->whereRaw(['_id' => ['$in' => $input['learners']]]);
        }

        if (!empty($input['groups'])) {
            $learnerGroups = LearnersGroups::whereRaw(['owningId' => ['$in' => $input['groups']]])->get()->toArray();
            $inputGroups = array();

            foreach ($learnerGroups as $inputGroup) {
                $inputGroups[] = $inputGroup['relatedId'];
            }

            $model = $model->whereRaw(['_id' => ['$in' => $inputGroups]]);
        }

        if (!empty($input['cohorts'])) {

            foreach ($input['cohorts'] as $cohort) {
                $model = $model->where($cohort, '1');
            }
        }

        $model = $model->paginate(10);

        $_p_learners = array();
        foreach ($model as $learner) {
            $_p_learners[] = "Students$" . $learner['_id'];
        }
        
        $attributes = $this->session['attributes'];
        $data['attributes'] = $attributes;
        $data['model'] = $model;
        $data['_p_learners'] = $_p_learners;

        return view('front.learners.ajax.listing', $data);
    }

    public function loadfilters(Request $request) {
        $schoolId = $this->session['schoolId'];
        $user_id = $this->session['_id'];
        $model = UserFilters::where("user_id", $user_id)->where("schoolId", $schoolId)->get()->toArray();
        $data['model'] = $model;
        return view('front.learners.ajax.loadfilter', $data);
    }

    public function savefilter(Request $request) {
        $input = $request->all();
        $validation = array(
            'name' => 'required',
            'filters' => 'required',
        );

        $messages = array(
            'name.required' => 'Filter name is required',
            'filters.required' => 'Choose atleast Groups, Learners or cohorts.',
        );

        $validator = Validator::make($input, $validation, $messages);
        $response['error'] = 0;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $schoolId = $this->session['schoolId'];
            $user_id = $this->session['_id'];
            parse_str($input['filters'], $filters);
            $model = UserFilters::create(['name' => $input['name'], "user_id" => $user_id, "schoolId" => $schoolId, 'filters' => $filters]);
        }
        return json_encode($response);
    }

    public function view($id) {
        $model = Learners::whereRaw(['_id' => $id, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['firstName' => 1, 'lastName' => 1]])->first();


        $attributes = $this->session['attributes'];
        $data['attributes'] = $attributes;
        $evidences = Evidence::whereRaw(['learnerCloudId' => $model->cloudId, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['firstName' => 1, 'lastName' => 1]])->get();

        $documents = Documents::whereRaw(['studentCloudId' => $model->cloudId, 'is_deleted' => ['$ne' => '1'], 'archived' => ['$ne' => '1']], ['sort' => ['dateCreated' => 1, 'docTitle' => 1]])->get();

        // d($documents,1);

        $data['model'] = $model;
        $data['evidences'] = $evidences;
        $data['documents'] = $documents;
        return view('front.learners.view', $data);
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,
    Input,
    Redirect;
use App\User;
use App\SubscriptionQuota;
use Session;
use Illuminate\Http\Request;
use App\Functions\Functions;

class SignupController extends Controller {

    protected $loginPath = 'signup';

    public function __construct() {
//session_start();
//$this->registrar = $registrar;
//$this->middleware('guest', ['except' => 'getLogout']);
    }

    public function index() {
        return view('front.users.login_customer');
    }

    public function register() {
        $result = States::get();
        return view('front.users.register')->with('states', $result);
    }

    public function forgot_password() {
        return view('front.users.forgot');
    }

    public function reset_password(Request $request) {
        $validation = array(
            'email' => 'required|email',
        );

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

            return redirect('forgot')->withErrors($validator->errors());
        }

        $user = User::where("email", "=", $request->email)->get();
        if (count($user) == 1) {
            $password = substr(md5(microtime()), rand(0, 26), 7);
            $user = $user[0];
            $replaces['NAME'] = $user->firstName . ' ' . $user->lastName;
            $replaces['PASSWORD'] = $password;
            $affectedRows = User::where('id', '=', $user->id)->update(array('password' => bcrypt($password)));
            $content = Content::where('code', '=', 'forgot_password')->get();
            $template = Functions::setEmailTemplate($content, $replaces);
            $mail = Functions::sendEmail($request->email, $template['subject'], $template['body']);
            \Session::flash('success', 'Your new password has been emailed.');
        } else {
            \Session::flash('success', 'Email not found.');
        }

        return redirect('forgot');
    }

    public function postLogin(Request $request) {

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $users = User::where('username', $request->email)->first();
//where('password',$request->password)->first();
//d($users,1);

        if (isset($users->exists) && $users->exists == 1) {

            if (isset($users->confirmed) && $users->confirmed == 1) {
                if (isset($users->status) && $users->status == "1") {
                    $pass = $request->password . $users->salt;
                    $pass = hash('SHA512', $pass);
                    if ($pass == $users->password) {

                        Session::put('_id', $users->_id);
                        Session::put('lastName', $users->lastName);
                        Session::put('firstName', $users->firstName);
                        Session::put('email', $users->email);
                        Session::put('username', $users->username);
                        Session::put('schoolId', $users->schoolId);
                        Session::put('status', $users->status);
                        Session::put('schoolLogin', 0);
                        Session::put('loginType', "user");


                        $school = SubscriptionQuota::where('schoolId', $users->schoolId)->first()->toArray();

                        Session::put('colour', $school['colour']);

                        Session::put('quota', $school['quota']);

                        $getData = Functions::getGeoData();
                        $getData['geoplugin_countryCode'] = 'UK';
                        if ($getData['geoplugin_countryCode'] == 'UK') {
                            $dateFormat = "d/m/Y";
                            $datepickerFormat = "dd/mm/YY";
                        } else {
                            $dateFormat = "m/d/Y";
                            $datepickerFormat = "mm/dd/YY";
                        }
                        
                        Session::put('countryCode', $getData['geoplugin_countryCode']);
                        
                        Session::put('dateFormat', $dateFormat);
                        Session::put('datepickerFormat', $datepickerFormat);

                        $datetime = $school['_created_at']->toDateTime();
                        $mydate = $datetime->format($dateFormat);
                        Session::put('subscriptionDate', $mydate);


                        for ($i = 1; $i <= 100; $i++) {
                            $attributeName = "attribute" . $i . "Name";
                            if (key_exists($attributeName, $school)) {
                                $attributes[$i] = $school[$attributeName];
                            } else {
                                break;
                            }
                        }
                        Session::put('attributes', $attributes);
                        return redirect('dashboard');
                    }
                } else {
                    \Session::flash('danger', "Your account has been suspended, please contact your administrator");
                    return redirect()->back();
                }
            } else {
                \Session::flash('danger', "You haven't activated your account, check & verify your email");
                return redirect()->back();
            }



//            die;
        }
        return redirect('/')
                        ->withInput($request->only('email', 'remember'))
                        ->withErrors([
                            'email' => 'Invalid login.',
        ]);
    }

    public function store(Request $request) {

        $validation = array(
            'firstName' => 'required|max:20',
            'lastName' => 'required|max:20',
            'email' => 'required|email|max:50|unique:users',
            'password' => 'required|confirmed|min:6',
            'gender' => 'required',
            'state' => 'required',
            'date' => 'required|numeric',
            'month' => 'required|numeric',
            'year' => 'required|numeric',
            'city' => 'required|max:25',
            'zip' => 'required|max:15',
            'address' => 'required|max:200',
            'phone' => 'required|max:16',
        );

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors(), 'register');
        }

//d($request->all());

        $user = new User;
        $user->firstName = $request->firstName;
        $user->lastName = $request->lastName;
        $user->state = $request->state;
        $user->gender = $request->gender;
        $user->dob = $request->year . "-" . $request->month . "-" . $request->date;
        $user->email = $request->email;
        $user->role_id = $request->role_id;
        $user->password = bcrypt($request->password);
        $user->token = bcrypt($request->email . $request->password);
        $user->save();

        $address = new Address;
        $address->user_id = $user->id;
        $address->phone = $request->phone;
        $address->country = 230;
        $address->state = $request->state;
        $address->city = $request->city;
        $address->address = $request->address;
        $address->zip = $request->zip;
        $address->save();
        $credentials = $request->only('email', 'password');

        if ($user->id) {
            $content = Content::where('code', '=', 'registration')->get();
            $replaces['NAME'] = $user->firstName . ' ' . $user->lastName;
            $replaces['LINK'] = url('verification?token=' . $user->token);
            $template = Functions::setEmailTemplate($content, $replaces);
            $mail = Functions::sendEmail($request->email, $template['subject'], $template['body']);
        }



        if (Auth::attempt($credentials, 1)) {

            session_start();
            return redirect('subscribe');
        }
    }

    public function confirmEmail($confirmation_code) {
        if (!$confirmation_code) {
            return 'Error! Confirmation Key missing.';
        }
        $user = User::where('_id', $confirmation_code)->first();
        if (!$user) {
            return 'Error! Confirmation Key missing.';
        }
        $user->confirmed = 1;
        $user->save();

//        //When new user confirmed registeration
//        $subjectRegister = view('emails.user_register.subject');
//        $bodyRegister = view('emails.user_register.body', compact('categories'));
//        Functions::sendEmail($user->email, $subjectRegister, $bodyRegister);

        return redirect('register/success/' . $user->id);
    }

    public function success($id) {
        $this->middleware('auth');
        $user = User::findOrFail($id);
        $data['user'] = $user;

        // d($data,1);
        return view('front.users.set_password', $data);
    }

//    public function success($id) {
//        return view('front.success');
//    }
    public function setPassword(Request $request) {
        $input = $request->all();
        $validation = array(
            'password' => 'required|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/|confirmed|min:8|max:100',
        );

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $user = User::where("_id", $request->_id)->where("confirmed", 1)->where("is_deleted", "0")->where("status", "1")->first();
        if (count($user) == 1) {
            // $password = substr(md5(microtime()), rand(0, 26), 7);
//            $replaces['name'] = $user->firstName . ' ' . $user->lastName;
//            $replaces['password'] = $password;
            //$password="aaaa";
            // $pass = $password . $user->salt;
            // $input['password'] = hash('SHA512', $pass);
            unset($input['_token']);
            unset($input['password_confirmation']);

            $salt = "salt" . time();
            $input['salt'] = $salt;
            $pass = $input['password'] . $salt;
            $input['password'] = hash('SHA512', $pass);
            User::where('_id', '=', $user->id)->update($input);

//            $body = view("emails.password_email", $replaces);
//            $mail = Functions::sendEmail($user->email, "Your new password.", $body);
            \Session::flash('success', 'Your password added successfully');
            return redirect('/');
        } else {
            \Session::flash('danger', 'Invalid User');
            return redirect()->back();
        }
    }

    public function destroy($id) {
        $row = Categories::where('id', '=', $id)->delete();
        return redirect('categories');
    }

}

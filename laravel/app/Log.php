<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Functions\Functions;

class Log extends Eloquent {

    protected $collection = 'Log';
    protected $fillable = ['_id', 'schoolId', 'cloudId', 'eventType', 'eventDetail', 'login', '_created_at', '_updated_at'];
    public $timestamps = false;

    public static function createLog($input) {
        // d($input,1);
        $data = array();
        $data['_id'] = Functions::generateRandomString(20);
        $data['schoolId'] = $input['schoolId'];
        $data['cloudId'] = Functions::getCloudId();
        $data['eventType'] = $input['eventType'];
        $data['eventDetail'] = $input['eventDetail'];
        $data['login'] = "admin";
        $data['_created_at'] = date('Y-m-d H:i:s');
        $data['_updated_at'] = date('Y-m-d H:i:s');

        //d($data,1);
        self::create($data);
    }

}

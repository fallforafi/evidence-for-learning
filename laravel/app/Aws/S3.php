<?php

namespace App\Aws;

use Illuminate\Support\Facades\Storage;
//use Intervention\Image\Facades\Image as Image;
use Intervention\Image\ImageManagerStatic as Image;

class S3 {

    public static function storeImage($fileName, $file, $destination, $destinationThumb = 1,$width = 420, $height = 420,$i) {
        
        
        $extension = $file->getClientOriginalExtension();
        $realPath = $file->getRealPath();
        
        //$fileName = $fileName . '.' . $extension;
        $imageFilename=$fileName.".".$extension;
        $image="mfp_".rand(0,15698)."_".$fileName.".".$extension;
        
        $imageTnFilename=rand(0,156908)."_".$fileName."_tn.".$extension;
        $imageTn="mfp_".rand(0,156908)."_".$fileName."_tn.".$extension;
        
        $r=Storage::put($image, file_get_contents($realPath), 'public');
        if(strtolower($extension) != "mp4"){
            if ($r==1 && $destinationThumb == 1) {
                $imageThumb = Image::make($realPath);
                $upload = $imageThumb->resize($width, $height)->encode($extension);
                Storage::put($imageTn, (string) $upload, 'public');
            }
        }
        $result['image'.$i.'Filename']=$imageFilename;
        $result['image'.$i.'Tn']=$imageTn;
        $result['image'.$i]=$image;
        $result['image'.$i.'TnFilename']=$imageTnFilename;
        return $result;
    }

    public static function deleteImage($fileName, $destination, $destinationThumb = "") {

        if ($fileName != "") {
            
            if (Storage::has($destination . $fileName)) {
                Storage::delete($destination . $fileName);
            }

            if ($destinationThumb != "") {
                if (Storage::has($destinationThumb . $fileName)) {
                    Storage::delete($destinationThumb . $fileName);
                }
            }
        }
    }
    
    public static function copy($from, $to) {
        if (Storage::has($from))
        {
            Storage::copy($from,$to);
        }
    }
}
<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SubscriptionQuota extends Eloquent {
    protected $collection = 'SubscriptionQuota';
}

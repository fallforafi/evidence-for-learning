<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Evidence extends Eloquent {

    protected $collection = 'Evidence';
    protected $dates = ['_created_at'];
     public $timestamps = false;
    protected $fillable = ['_id','is_deleted', 'schoolId','status', '_rperm', '_wperm', '_acl', '_created_at', '_updated_at', 'frameworkItemsPointerArray', 'evidenceDate', 'comment', 'frameworkItems','frameworkItemsPointerArray','tagCloudIds','tagsPointerArray','_p_learner','learnerCloudId','evidenceSize','cloudId','subscriptionDeviceID','deviceUDID','image1','image1Filename','image1Tn','image1TnFilename','parentView','parentCapture'];
    //  image3TnFilename image1
    //protected $guarded = [];
    public function frameworkItems() {
        return $this->hasMany('FrameworkItems');
    }

}

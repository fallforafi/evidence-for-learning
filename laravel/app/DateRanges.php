<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class DateRanges extends Eloquent {
    protected $collection = 'DateRanges';
    public $timestamps = false;
    protected $guarded = [];
    protected $fillable = ['_id','rangeName','from','to','schoolId','cloudId', '_rperm', '_wperm', '_acl', '_created_at', '_updated_at','is_deleted'];
   
}
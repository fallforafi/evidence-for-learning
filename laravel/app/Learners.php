<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Learners extends Eloquent {
    protected $collection = 'Students';
    protected $guarded = [];
    public $timestamps = false;
    protected $fillable = ['_id','uniqueRef','firstName', 'lastName', 'email', 'gender','schoolId','dateOfBirth','parent1FirstName','parent1LastName','parent1Email','parent2FirstName','cloudId','parent2LastName','parent2Email','status', '_rperm', '_wperm', '_acl', '_created_at', '_updated_at'];
   
}
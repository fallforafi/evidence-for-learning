<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SubscriptionLicence extends Eloquent {
    protected $collection = 'SubscriptionLicence';
    protected $guarded = [];
    protected $fillable = ['_id','schoolId','users','endDate', 'domain','_created_at', '_updated_at'];
   
}
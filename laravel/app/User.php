<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class User extends Eloquent {

    protected $collection = 'TTC_Users';
    public $timestamps = false;
    protected $fillable = ['_id', 'username', 'email', 'firstName', 'lastName', 'externalId', 'manager', 'role', 'password', 'userTitle', 'salt', '_created_at', '_updated_at', '_wperm', '_rperm', '_acl', 'cloudId', 'schoolId','status','confirmed','is_deleted'];

}

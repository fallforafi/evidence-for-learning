<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ParentLog extends Eloquent {
    protected $collection = 'ParentLog';
    public $timestamps = false;
}

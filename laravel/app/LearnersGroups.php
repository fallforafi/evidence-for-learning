<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class LearnersGroups  extends Eloquent {
    protected $collection = '_Join:learners:Group';
}

<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Documents extends Eloquent {
    protected $collection = 'LearnerProfileDocuments';
}

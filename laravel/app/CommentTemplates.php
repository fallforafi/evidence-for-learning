<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CommentTemplates extends Eloquent {
    protected $collection = 'CommentTemplates';
    public $timestamps = false;
    protected $guarded = [];
    protected $fillable = ['_id','commentTemplate','schoolId','is_deleted','cloudId', '_rperm', '_wperm', '_acl', '_created_at', '_updated_at'];
   
}
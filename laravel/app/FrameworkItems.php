<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FrameworkItems extends Eloquent {
    
    protected $collection = 'FrameworkItems';
    public $timestamps = false;
    
    public function evidence()
    {
        return $this->belongsTo('Evidence');
    }
}
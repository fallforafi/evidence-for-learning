<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserFilters extends Eloquent {

    protected $collection = 'UserFilters';
    public $timestamps = false;
    protected $fillable = ['_id', 'name', 'user_id', 'schoolId', 'cloudId', 'filters', '_created_at', '_updated_at', 'learners', 'groups', 'cohorts'];

}

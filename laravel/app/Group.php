<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Group extends Eloquent {
    protected $collection = 'Group';
}
